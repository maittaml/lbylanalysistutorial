# LbyLpackage-Rel21


## First time setup (on lxplus)
 git clone ssh://git@gitlab.cern.ch:7999/maittaml/lbylanalysistutorial.git  \
 cd lbylanalysistutorial\
 source runlbyl.sh
## For the next time  you log in 
source  everylogin.sh
## Add your input files
Path to data:/afs/cern.ch/user/m/maittaml/ForTuto/mc
## Run a test
cd .. \
cd run \
python localtest.py \
-You can use **run/runlbyl.py**  to run  the code with diffrenent  options:   \
       -For example: **submit a job to  the grid* , you can use the folloing command:  **python  runlbyl.py  - g  "Name of Rucio dataset" **  \
       - if you have a .txt file with the name containing input file paths, you can use the following command:  **python  runlbyl.py  - t  mc.txt   **
## output files
The job output is stored in the directory 'lbyl_timestr'. The directory name can be changed in the 
job steering macro "localtest.py" \
Output histograms are stored in files called hist-*.root inside the directory 'mini_test_timestr'
