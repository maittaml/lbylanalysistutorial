/**
 *  @file  LapxAODEvent.h
 *  @brief  tp gather-up all information and all corrections related to one xAOD event for the WZ analysis
 *
 *
 *  @author  E Sauvan
 *
 *  @date    2015-11-07
 *
 *  @internal
 *     Created : 
 * Last update : 
 *          by : 
 *
 * =====================================================================================
 */
 
#ifndef EXCLRUNII_LAPXAODEVENT_H
#define EXCLRUNII_LAPXAODEVENT_H


//--- RootCore / xAOD includes
//#include <EventLoop/Algorithm.h>
#include <AnaAlgorithm/AnaAlgorithm.h>


#include <xAODEventInfo/EventInfo.h>

#include <AsgTools/MessageCheck.h>


// GRL
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <AsgTools/AnaToolHandle.h>

//egamma calibration
#include <ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h>
#include "ElectronPhotonShowerShapeFudgeTool/ElectronPhotonShowerShapeFudgeTool.h"


// include files for using the trigger tools
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"


//---- muons
#include <xAODMuon/MuonContainer.h>


//---- tracking
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

//#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"


//---- systematics
#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicVariation.h" 

#include "xAODTrigMinBias/TrigSpacePointCountsContainer.h"
#include "xAODTrigMinBias/TrigSpacePointCounts.h"
#include "xAODTrigMinBias/versions/TrigHisto2D_v1.h"



//--- ZDC
//#include "ZdcAnalysis/ZdcAnalysisTool.h"



#include <xAODCore/ShallowCopy.h>
#include <xAODCore/ShallowAuxContainer.h>


#include "xAODBase/IParticleHelpers.h"


#include "xAODTrigger/EnergySumRoI.h"
//#include "xAODTrigger/EnergySumRoIAuxInfo.h"
#include "xAODTrigger/MuonRoI.h"
#include "xAODTrigger/EmTauRoI.h"

//---- electrons
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"


// photons
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/PhotonAuxContainer.h"
#include "xAODEgamma/EgammaDefs.h"


//--- truth
#include "xAODTruth/TruthEventContainer.h"

#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"


//---- isolation



//--- ROOT includes
#include <TH1.h>
#include <TTree.h>
#include <TSystem.h>



//--- EXCLRunII includes
#include "EXCLRunII/KerasModel.h"
#include "EXCLRunII/ExclVeto.h"
#include "EXCLRunII/Pair.h"
#include "EXCLRunII/EXCLcandidate.h"


//--- not used any longer
typedef std::vector<xAOD::Electron*> VectorOfElectrons;
typedef std::vector<xAOD::Muon*>     VectorOfMuons;



//--- STL includes



class LapxAODEvent : public EL::AnaAlgorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:



  xAOD::TEvent* m_event; //!
  const xAOD::EventInfo* m_eventInfo; //!


  int m_eventCounter; //!

	    
  //--- timing variables 
  Long_t m_tStartLoop;
  Long_t m_tEndLoop;
  Long_t m_tStartInitialise;	  
  Long_t m_tEndInitialise;



  double weight; //!
  int m_sum_pix; //!
  float sum_EtTot;

  //---- EventInfos
  int EventNumber; //!  
  int RunNumber; //!    
  float ActualInteractionsPerCrossing; //! 
  float AverageInteractionsPerCrossing; //!    
  int LumiBlock; //!
  short Level1TriggerType;//
  int pixelFlags;//
  int sctFlags;//
  int trtFlags;//
  int larFlags;//
  int tileFlags;//  
  int muonFlags;//

  bool use_nn_pid; //!
  // Use the model only taking three variables into account
  bool use_nn_only_3; //!
  KerasModel nn_photon_models[4]; //!
  float nn_photon_thresholds[4]; //!

  const xAOD::Vertex *pv; //!
  const xAOD::Vertex *lepv; //!
  
   double t_pv_z;//!

//---- for lumi calculation 
  bool m_isMC; //!
  int m_mcID; //!
  double m_genLumiWeight; //!
  double m_sumOfGenWeights; //!
  double m_pileupWeight; //!
  double m_genWeight; //!
  
  double vertexWeight(double z);

  
//---- electron tools  

  asg::AnaToolHandle<CP::IEgammaCalibrationAndSmearingTool> m_electronCalibrationTool;
  
  asg::AnaToolHandle<ElectronPhotonShowerShapeFudgeTool> m_fudgeMCTool;
 
   

//---- muon tools  

  

   
  
  //--- tracking tools

  
  //---ZDC tools
  //ZDC::ZdcAnalysisTool* m_zdcAnalysisTool;//!
  
 
  bool DoSyst = true; //!

//---- List of systematics
  std::vector<CP::SystematicSet> m_sysList; //!






  // this is a standard constructor
  LapxAODEvent (const std::string& name, ISvcLocator* pSvcLocator);
    
  // these are the functions inherited from Algorithm
  //virtual StatusCode setupJob (Job& job);
  virtual StatusCode fileExecute ();
  virtual StatusCode histInitialize ();
  virtual StatusCode changeInput (bool firstFile);
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode postExecute ();
  virtual StatusCode finalize () override;
  virtual StatusCode histFinalize ();
  

  
  StatusCode fillElectron();
  StatusCode fillPhoton();
  StatusCode fillTrack();
  StatusCode fillPixelTrack();
  StatusCode fillCluster();
  
  StatusCode fillEvent();
  float transformNNVariable(float x, float mean, float std);
  bool passesPhotonSelection(xAOD::Photon *photon);
  
  void clear();
  
  
  //--- for systematics
  bool configureToolsForSystematics(CP::SystematicSet sysUsed);


  bool m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 = false;
  bool m_eventFiredHLT_mb_sptrk_ion_L1ZDC_A_C_VTE50 = false; 
  bool m_eventFiredHLT_mb_sptrk_vetombts2in_L1ZDC_A_C_VTE50 = false;
  bool m_eventFiredHLT_hi_loose_upc_L1ZDC_A_C_VTE50 = false;
  bool m_eventFiredHLT_mb_sptrk_vetombts2in_L1MU0_VTE50 = false;
  bool m_eventFiredHLT_TE5= false;
  
  bool m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_noiseSup_L1TEX_VTEY= false;
  bool m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ= false;
  bool m_eventFired_2018_exclusivelooseFgapTriggers= false;
  bool m_eventFired_2018_exclusivelooseNoFgapTriggers= false;
  bool m_eventFired_2018_EMPTY= false;
  
    

//---- remaining histos, to be removed  
  TH1 *h_InitialNumberOfEvents; //!    
  TH1 *h_InitialSumOfWeights; //!  
  TH1 *h_InitialSumOfWeightsSquared; //!    
  
  TH1 *h_NumOfEvtsAfterDerivation; //! 
  TH1 *h_NumOfEvtsAfterGRL; //! 
  TH1 *h_NumOfEvtsAfterPV; //! 
  TH1 *h_NumOfEvtsAfterSingleMuTrig; //!

  TH1 *h_NumOfEvtsAfter2Mu; //! 
  TH1 *h_NumOfEvtsAfter2El; //! 
  TH1 *h_NumOfEvtsAfterEMu; //! 
        
  TH1 *h_PtMinBothLeptons;   //!
  TH1 *h_EtaMaxBothLeptonZ;   //!
  TH1 *h_MuonQualityAcceptedCut;   //!
  TH1 *h_MuonIsolation;   //!
  TH1 *h_isCompatible;   //!
  TH1 *h_TwoMuons;   //!
  
  TH1 *h_NumOfLeptons; //!
 
   // defining the output file name and tree that we will put in the output ntuple, also the one branch that will be in that tree 
  std::string outputName;
  TTree *tree; //!
  
  
  //---- Container
  std::pair<xAOD::ElectronContainer*, xAOD::ShallowAuxContainer*> m_electrons_shallowCopy;
  std::pair<xAOD::PhotonContainer*, xAOD::ShallowAuxContainer*> m_photons_shallowCopy;
  std::pair<xAOD::CaloClusterContainer*, xAOD::ShallowAuxContainer*> m_clusters_shallowCopy;
  std::pair<xAOD::TrackParticleContainer*, xAOD::ShallowAuxContainer*> m_trks_shallowCopy;
  std::pair<xAOD::TrackParticleContainer*, xAOD::ShallowAuxContainer*> m_pixtrks_shallowCopy;
  
  //---- list of selected particles
  std::vector<xAOD::Electron*> m_goodElectrons; //!
  std::vector<xAOD::Electron*> m_goodElectrons_noID; //!  

  std::vector<xAOD::Muon*> m_goodMuons;   //!
  std::vector<xAOD::Muon*> m_goodMuons_LooseTrkIso; //!

  
  std::vector<xAOD::TrackParticle*> m_goodTracks;   //!
  std::vector<xAOD::TrackParticle*> m_goodPixTracks;   //!
  std::vector<xAOD::TrackParticle*> m_goodTracksTight;   //!
  
  std::vector<xAOD::TrackParticle*> m_goodTracksPriVtx;   //!  
  std::vector<const xAOD::Vertex*> m_goodVertices;   //!

  std::vector<xAOD::TruthParticle*> m_TruthTracksPri;   //! 
  std::vector<xAOD::TrackParticle*> m_TracksPriMatched;   //! 
  
  std::vector<xAOD::Photon*> m_goodPhotons; //!
  std::vector<xAOD::Photon*> m_goodPhotonsNoPID; //!
  std::vector<xAOD::Photon*> m_goodPhotonsF1Inv; //!
  
  std::vector<xAOD::CaloCluster*> m_goodClusters; //!
  
  
  
   
  // this is needed to distribute the algorithm to the workers
  //ClassDef(LapxAODEvent, 1);
  
  private: 
  
  
  protected:  
  
  std::vector<std::string> m_listOfMuonTriggers; //!  
  std::vector<std::string> m_listOfDiMuonTriggers; //!    
  std::vector<std::string> m_listOfTriMuonTriggers; //!    
  std::vector<std::string> m_listOfElectronTriggers; //!  
  std::vector<std::string> m_listOfDiElectronTriggers; //!
  std::vector<std::string> m_listOfElectronMuonTriggers; //! 
  

  
};

#endif
