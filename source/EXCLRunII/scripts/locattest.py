#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Import os to be able to read environment variables
import os

# Set up the SampleHandler object to handle the input files
sh = ROOT.SH.SampleHandler()

# Set the name of the tree in our files in the xAOD the TTree
# containing the EDM containers is "CollectionTree"
sh.setMetaString( 'nc_tree', 'CollectionTree' )

# Use SampleHandler to get the sample from the defined location
inputFilePath  = '/afs/cern.ch/user/m/maittaml/ForTuto/mc'
ROOT.SH.ScanDir().filePattern('*AOD.17615400._000001.pool.root.1*').scan( sh, inputFilePath )


# Print information about the sample
sh.Print()

# Set data type
dataType = "mc" # "mc" or "data"

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, 1000 )

# Later we'll add additional analysis algorithms here

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'EXCLRecoAnalysis', 'AnalysisAlg' )

# Later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the job
job.algsAdd( alg )

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )

# Do not add anything here!!!

