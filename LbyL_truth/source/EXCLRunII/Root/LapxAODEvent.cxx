

#define LAPPEVENTDEBUG 0

//---- to use fixedCut Loose ISo for Z-leptons (lower pT leptons)
#define USEFIXISO 0


#include <AsgTools/MessageCheck.h>

//--- RootCore / xAOD includes
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>


#include <EXCLRunII/LapxAODEvent.h>

/*
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
// EDM includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODRootAccess/tools/Message.h"

#include "PATInterfaces/CorrectionCode.h" 
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODCore/ShallowCopy.h"
*/

//--- ROOT includes
#include <TSystem.h>
#include <TFile.h>


//--- EXCLRunII includes
#include "EXCLRunII/LapPhysicsConstants.h"



//--- STL includes
#include <ctime>
#include <iostream>




// this is needed to distribute the algorithm to the workers
//ClassImp(LapxAODEvent)



/*****************************************************************/
LapxAODEvent :: LapxAODEvent (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator){
/*****************************************************************/
//---- constructor


 
//--- set it's object name
 //SetName("LapxAODEvent_CLASS");

}





/*

StatusCode LapxAODEvent :: setupJob (Job& job) {

  
  job.useXAOD ();

  ANA_CHECK( "setupJob()", xAOD::Init() ); // call before opening first file
  
  return StatusCode::SUCCESS;
}
*/


/*****************************************************************/
StatusCode LapxAODEvent :: histInitialize (){
/*****************************************************************/
//--- Initialise histograms
  
  std::cout<<"LapxAODEvent: Initializing histograms..."<<std::endl;
   
 
  book (TH1F ("h_jetPt", "h_jetPt", 100, 0, 500)); // jet pt [GeV]
  

  h_InitialNumberOfEvents = new TH1F("MetaData/InitialNumberOfEvents", "InitialNumberOfEvents", 1, 0, 1); // 
  wk()->addOutput (h_InitialNumberOfEvents);    
  
  h_InitialSumOfWeights = new TH1F("MetaData/InitialSumOfWeights", "InitialSumOfWeights", 1, 0, 1); // 
  wk()->addOutput (h_InitialSumOfWeights);   
    
  h_InitialSumOfWeightsSquared = new TH1F("MetaData/InitialSumOfWeightsSquared", "InitialSumOfWeightsSquared", 1, 0, 1); // 
  wk()->addOutput (h_InitialSumOfWeightsSquared);     


  h_NumOfEvtsAfterDerivation = new TH1F("MetaData/NumOfEvtsAfterDerivation", "NumOfEvtsAfterDerivation", 1, 0, 1); // 
  wk()->addOutput (h_NumOfEvtsAfterDerivation);   

  h_NumOfEvtsAfterGRL = new TH1F("MetaData/NumOfEvtsAfterGRL", "NumOfEvtsAfterGRL", 1, 0, 1); // 
  wk()->addOutput (h_NumOfEvtsAfterGRL);  
  
  h_NumOfEvtsAfterPV = new TH1F("MetaData/NumOfEvtsAfterPV", "NumOfEvtsAfterPV", 1, 0, 1); // 
  wk()->addOutput (h_NumOfEvtsAfterPV);    

  h_NumOfEvtsAfterSingleMuTrig = new TH1F("MetaData/NumOfEvtsAfterSingleMuTrig", "NumOfEvtsAfterSingleMuTrig", 1, 0, 1); // 
  wk()->addOutput (h_NumOfEvtsAfterSingleMuTrig);   

  h_NumOfEvtsAfter2Mu = new TH1F("MetaData/NumOfEvtsAfter2Mu", "NumOfEvtsAfter2Mu", 1, 0, 1); // 
  wk()->addOutput (h_NumOfEvtsAfter2Mu); 
  h_NumOfEvtsAfter2El = new TH1F("MetaData/NumOfEvtsAfter2El", "NumOfEvtsAfter2El", 1, 0, 1); // 
  wk()->addOutput (h_NumOfEvtsAfter2El);   
  h_NumOfEvtsAfterEMu = new TH1F("MetaData/NumOfEvtsAfterEMu", "NumOfEvtsAfterEMu", 1, 0, 1); // 
  wk()->addOutput (h_NumOfEvtsAfterEMu); 
    
  h_NumOfLeptons = new TH1F("MetaData/NumOfLeptons", "NumOfLeptons", 5, -0.5, 4.5); // 
  wk()->addOutput (h_NumOfLeptons);   


  // --  to check the cutflow wrt Zmumu analysis
  h_PtMinBothLeptons = new TH1F("MetaData/NumPtMinBothLeptons", "NumPtMinBothLeptons", 1, 0, 1); // 
  wk()->addOutput (h_PtMinBothLeptons);   
  h_EtaMaxBothLeptonZ = new TH1F("MetaData/NumEtaMaxBothLeptonZ", "NumEtaMaxBothLeptonZ", 1, 0, 1); // 
  wk()->addOutput (h_EtaMaxBothLeptonZ); 
  h_MuonQualityAcceptedCut = new TH1F("MetaData/NumMuonQualityAcceptedCut", "NumMuonQualityAcceptedCut", 1, 0, 1); // 
  wk()->addOutput (h_MuonQualityAcceptedCut); 
  h_MuonIsolation = new TH1F("MetaData/NumMuonIsolation", "NumMuonMuonIsolation", 1, 0, 1); // 
  wk()->addOutput (h_MuonIsolation); 
  h_isCompatible = new TH1F("MetaData/NumisCompatible", "NumisCompatible", 1, 0, 1); // 
  wk()->addOutput (h_isCompatible); 
  h_TwoMuons = new TH1F("MetaData/NumTwoMuons", "NumTwoMuons", 1, 0, 1); // 
  wk()->addOutput (h_TwoMuons); 


  
  std::cout<<"... LapxAODEvent histograms initialized"<<std::endl;
  
  return StatusCode::SUCCESS;
}



/*****************************************************************/
StatusCode LapxAODEvent :: fileExecute ()
/*****************************************************************/
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return StatusCode::SUCCESS;
}



/*****************************************************************/
StatusCode LapxAODEvent :: changeInput (bool firstFile)
/*****************************************************************/
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  
  if(firstFile){};//--- just to avoid a warning
  
  printf("****** Change input file \n");
   
   //m_event = wk()->xaodEvent();

   //-------------------------------------------------------------------------// 
  // get the MetaData tree once a new file is opened, with
/*  TTree *MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
  if (!MetaData) {
    Error("fileExecute()", "MetaData not found! Exiting.");
    return StatusCode::FAILURE;
  }
  MetaData->LoadTree(0);
  Info("initialize()", "Load MetaData information" );   

  //check if file is from a DxAOD
  bool m_isDerivation = !MetaData->GetBranch("StreamAOD");

  if(m_isDerivation ){
  
    // check for corruption
    const xAOD::CutBookkeeperContainer* incompleteCBC = nullptr;
    if(!m_event->retrieveMetaInput(incompleteCBC, "IncompleteCutBookkeepers").isSuccess()){
      Error("initializeEvent()","Failed to retrieve IncompleteCutBookkeepers from MetaData! Exiting.");
      return StatusCode::FAILURE;
    }

    if ( incompleteCBC->size() != 0 ) {
      Info("initialize()", "incompleteCBC=%i, check further", (int)incompleteCBC->size());	   

      for(auto cbk : *incompleteCBC) {
        if (cbk->inputStream() != "unknownStream"){
          Error("fileExecute()","Found incomplete Bookkeepers in data! Check file for corruption. Exiting!");
          return StatusCode::FAILURE;
        }
        else{
          Error("fileExecute()","Found incomplete Bookkeepers in data! Check file for corruption. NOT exiting coming from RAW->ESD step...");
        }
      }
    }
    const xAOD::CutBookkeeperContainer* completeCBC = 0;
    if(!m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess()){
      Error("initializeEvent()","Failed to retrieve CutBookkeepers from MetaData! Exiting.");
      return StatusCode::FAILURE;
    }

    const xAOD::CutBookkeeper* allEventsCBK = 0;
    int maxCycle = -1;
    for (const auto& cbk: *completeCBC) {
      if (cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD") {
 	allEventsCBK = cbk;
 	maxCycle = cbk->cycle();
      }
    }

    uint64_t nEventsProcessed  = allEventsCBK->nAcceptedEvents();
    double sumOfWeights        = allEventsCBK->sumOfEventWeights();
    double sumOfWeightsSquared = allEventsCBK->sumOfEventWeightsSquared();


    const xAOD::EventInfo* eventInfo = 0;
    ANA_CHECK(m_event->retrieve( eventInfo, "EventInfo"));  
  
    if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) )
    		std::cout << "***New File*** "<<"mcID = "<< eventInfo-> mcChannelNumber() <<" nEventsProcessed = " << nEventsProcessed << " sumOfWeights=" << sumOfWeights << " sumOfWeightsSquared=" << sumOfWeightsSquared << std::endl;
  
    //---- store them as member variables
    m_sumOfGenWeights  = sumOfWeights;
    
  }//--- end if derivation
 */
 
  return StatusCode::SUCCESS;
}



/*****************************************************************/
StatusCode LapxAODEvent :: initialize ()
/*****************************************************************/
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  
  
  printf("******* Initialise LapxAODEvent ******* \n");
  
  m_tStartInitialise=gSystem->Now();
  
  histInitialize ();
  
  
  //book (TH1F ("h_jetPt", "h_jetPt", 100, 0, 500)); // jet pt [GeV]

  //m_event = wk()->xaodEvent();

  //std::cout<<"initialize() Number of events = "<< m_event->getEntries() <<std::endl; 
 
 
  //------ set the Event Information --------------------
/*  m_eventInfo = 0;
  ANA_CHECK(m_event->retrieve( m_eventInfo, "EventInfo"));  
  //----- check if the event is data or MC
  m_isMC = false;
  weight = 1;  
  if(m_eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ) m_isMC = true;   
*/

/*
  //-------------------------------------------------------------------------// 
  // get the MetaData tree once a new file is opened, with
  TTree *MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
  if (!MetaData) {
    Error("fileExecute()", "MetaData not found! Exiting.");
    return StatusCode::FAILURE;
  }
  MetaData->LoadTree(0);
  Info("initialize()", "Load MetaData information" );   

  //check if file is from a DxAOD
  bool m_isDerivation = !MetaData->GetBranch("StreamAOD");
  
    

  if(m_isDerivation ){
  
    // check for corruption
    const xAOD::CutBookkeeperContainer* incompleteCBC = nullptr;
    if(!m_event->retrieveMetaInput(incompleteCBC, "IncompleteCutBookkeepers").isSuccess()){
      Error("initializeEvent()","Failed to retrieve IncompleteCutBookkeepers from MetaData! Exiting.");
      return StatusCode::FAILURE;
    }

    if ( incompleteCBC->size() != 0 ) {
      Info("initialize()", "incompleteCBC=%i, check further", (int)incompleteCBC->size());	   

      for(auto cbk : *incompleteCBC) {
        if (cbk->inputStream() != "unknownStream"){
          Error("fileExecute()","Found incomplete Bookkeepers in data! Check file for corruption. Exiting!");
          return StatusCode::FAILURE;
        }
        else{
          Error("fileExecute()","Found incomplete Bookkeepers in data! Check file for corruption. NOT exiting coming from RAW->ESD step...");
        }
      }
    }
    

    
    const xAOD::CutBookkeeperContainer* completeCBC = 0;
    if(!m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess()){
      Error("initializeEvent()","Failed to retrieve CutBookkeepers from MetaData! Exiting.");
      return StatusCode::FAILURE;
    }

    const xAOD::CutBookkeeper* allEventsCBK = 0;
    int maxCycle = -1;
    for (const auto& cbk: *completeCBC) {
      if (cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD") {
 	allEventsCBK = cbk;
 	maxCycle = cbk->cycle();
      }
    }


    uint64_t nEventsProcessed  = allEventsCBK->nAcceptedEvents();
    double sumOfWeights        = allEventsCBK->sumOfEventWeights();
    double sumOfWeightsSquared = allEventsCBK->sumOfEventWeightsSquared();
  
    std::cout << "nEventsProcessed = " << nEventsProcessed << " sumOfWeights=" << sumOfWeights << " sumOfWeightsSquared=" << sumOfWeightsSquared << std::endl;
  
    //---- store them as member variables
    m_sumOfGenWeights  = sumOfWeights;
    
    h_InitialNumberOfEvents->Fill(0.5,nEventsProcessed);
    h_InitialSumOfWeights->Fill(0.5,sumOfWeights);
    h_InitialSumOfWeightsSquared->Fill(0.5,sumOfWeightsSquared);

  }//--- end if derivation

  */


  


//-------------------------------------------------------------------------

  
//---- count number of events
  m_eventCounter = 0;  



  

   

  
//-------------------------------------------------------------------------//     


  
//--------------------- Initialize Muon Selection Tool --------------------//
  //-------------------------------------------------------------------------// 
  // Muon twiki https://twiki.cern.ch/twiki/bin/view/Atlas/MuonSelectionTool //
  //-------------------------------------------------------------------------//   
/*  m_muonSelection = new CP::MuonSelectionTool("muonSelection");
  m_muonSelection->msg().setLevel( MSG::INFO );
//  ANA_CHECK("initialize()", m_muonSelection->setProperty( "MaxEta", 2.4 )); 
  ANA_CHECK("initialize()", m_muonSelection->setProperty( "MuQuality", 1)); //0 = Tight, 1 = Medium, 2 = Loose, 3 = VeryLoose 
  ANA_CHECK("initialize()", m_muonSelection->initialize());  
  
  m_muonSelection_loose = new CP::MuonSelectionTool("muonSelection_loose");
  m_muonSelection_loose->msg().setLevel( MSG::INFO );
  //ANA_CHECK("initialize()", m_muonSelection_loose->setProperty( "MaxEta", 2.5 )); //---- why not ?? crazy ....
  ANA_CHECK("initialize()", m_muonSelection_loose->setProperty( "MuQuality", 3)); //0 = Tight, 1 = Medium, 2 = Loose, 3 = VeryLoose 
  ANA_CHECK("initialize()", m_muonSelection_loose->initialize());    
 */ 
  //-------------------------------------------------------------------------// 

  


 
//------- Initialise electrons ---------------------------------------------  
  // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaIdentificationRun2
  //---------------------------- Electron LH ID -----------------------------//   
/*  m_LHToolTight2015    = new AsgElectronLikelihoodTool ("m_LHToolTight2015");
  m_LHToolMedium2015   = new AsgElectronLikelihoodTool ("m_LHToolMedium2015"); 
  m_LHToolLoose2015    = new AsgElectronLikelihoodTool ("m_LHToolLoose2015");
  
 // initialize the primary vertex container for the tool to have access to the number of vertices used to adapt cuts based on the pileup
  ANA_CHECK("initialize()", m_LHToolTight2015  -> setProperty("primaryVertexContainer","PrimaryVertices"));
  ANA_CHECK("initialize()", m_LHToolMedium2015 -> setProperty("primaryVertexContainer","PrimaryVertices"));
  ANA_CHECK("initialize()", m_LHToolLoose2015  -> setProperty("primaryVertexContainer","PrimaryVertices"));

  // define the config files
  std::string confDir = "ElectronPhotonSelectorTools/offline/mc15_20150712/";
  ANA_CHECK("initialize()", m_LHToolTight2015  -> setProperty("ConfigFile",confDir+"ElectronLikelihoodTightOfflineConfig2015.conf"));
  ANA_CHECK("initialize()", m_LHToolMedium2015 -> setProperty("ConfigFile",confDir+"ElectronLikelihoodMediumOfflineConfig2015.conf"));
  ANA_CHECK("initialize()", m_LHToolLoose2015  -> setProperty("ConfigFile",confDir+"ElectronLikelihoodLooseOfflineConfig2015.conf"));
  //--- switch to BLayer
//  ANA_CHECK("initialize()", m_LHToolLoose2015  -> setProperty("ConfigFile",confDir+"ElectronLikelihoodVeryLooseOfflineConfig2015.conf"));

  ANA_CHECK("initialize()", m_LHToolTight2015  -> initialize());
  ANA_CHECK("initialize()", m_LHToolMedium2015 -> initialize());
  ANA_CHECK("initialize()", m_LHToolLoose2015  -> initialize()); 


/////////////////////////
  m_EMToolLoose2015 = new AsgElectronIsEMSelector("m_LooseRun2"); // create the selector
  // set the file that contains the cuts on the shower shapes
  ANA_CHECK("initialize()", m_EMToolLoose2015->setProperty("ConfigFile",confDir+"ElectronIsEMLooseSelectorCutDefs.conf")); 
  ANA_CHECK("initialize()", m_EMToolLoose2015->initialize());
*/
/////////////////////////





  



//--------------------- Initialize Track Selection Tool --------------------//  
/*  m_trkSelTool = new InDet::InDetTrackSelectionTool("trkSel_loose");  
  //ANA_CHECK("initialize()", m_trkSelTool->setProperty( "CutLevel", "Tight" ) ); 
  ANA_CHECK("initialize()", m_trkSelTool->setProperty( "CutLevel", "LooseElectron" ) ); // CHANGED!!!!!!!  //Tight 
  //ANA_CHECK("initialize()", m_trkSelTool->setProperty( "minPt", 500.));   
  //ANA_CHECK("initialize()", m_trkSelTool->setProperty("minNSiHits", 5) );
  ANA_CHECK("initialize()", m_trkSelTool->initialize());  
*/




//---ZDC---
//  m_zdcAnalysisTool = new ZDC::ZdcAnalysisTool("ZdcAnalysisTool");
//  ANA_CHECK("initialize()",m_zdcAnalysisTool->setProperty("FlipEMDelay", true)); //(flipDelay is true for 20.1.9.X and false for 20.7.X)
//  ANA_CHECK("initialize()",m_zdcAnalysisTool->initialize());



//////////////////////// PHOTONS ////////////////////////////////////

  //---- init electron/photon calibration
/*   m_electronCalibrationTool = new CP::EgammaCalibrationAndSmearingTool("m_electronCalibrationTool"); 
   ANA_CHECK("initialize()",m_electronCalibrationTool->setProperty("ESModel", "es2015PRE")); 
   ANA_CHECK("initialize()",m_electronCalibrationTool->setProperty("ResolutionType", "SigmaEff90"));  
   ANA_CHECK("initialize()",m_electronCalibrationTool->setProperty("decorrelationModel", "FULL_v1"));
   ANA_CHECK("initialize()",m_electronCalibrationTool->initialize());




  m_photonLooseIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonLooseIsEMSelector" );
  m_photonLooseIsEMSelector->setProperty("isEMMask",egammaPID::PhotonLoose);
  m_photonLooseIsEMSelector->setProperty("ConfigFile","ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMLooseSelectorCutDefs.conf");
  ANA_CHECK("initialize()", m_photonLooseIsEMSelector->initialize());

  m_fudgeMCTool = new ElectronPhotonShowerShapeFudgeTool ( "FudgeMCTool" );  
  int FFset = 16; // for MC15 samples, which are based on a geometry derived from GEO-21
  ANA_CHECK("initialize()", m_fudgeMCTool->setProperty("Preselection",FFset));
  ANA_CHECK("initialize()", m_fudgeMCTool->initialize());

*/




  
  
  
//--- not needed here now, could be removed ? 
//--------------------- Loop over systematics registry --------------------//
/*  const CP::SystematicRegistry& registry = CP::SystematicRegistry::getInstance();
  const CP::SystematicSet& recommendedSystematics = registry.recommendedSystematics(); // get list of recommended systematics

  // this is the nominal set, no systematic
  m_sysList.push_back(CP::SystematicSet());
  
  // loop over recommended systematics
  for(CP::SystematicSet::const_iterator sysItr = recommendedSystematics.begin(); sysItr != recommendedSystematics.end(); ++sysItr){
    m_sysList.push_back( CP::SystematicSet() );
    m_sysList.back().insert( *sysItr );
  }
*/
    
// //--- alternative method ??
//   // using PAT method to derive +- 1sigma systematics
//   sysList = CP::make_systematics_vector(recommendedSystematics);

// //---- List systematics used 
//   printf("**** List of systematics used [%d] : \n",m_sysList.size()); 
//   std::vector<CP::SystematicSet>::const_iterator sysListItr;  
//   for (sysListItr = m_sysList.begin(); sysListItr != m_sysList.end(); ++sysListItr){  
//     	printf("Sys : %s \n",((*sysListItr).name()).c_str());
//   } 


//--------------------------------------------------

//-------------------------------------------------------------------------//     


//   //----------------- Book histograms for systematic shifts -----------------//
//   std::vector<CP::SystematicSet>::const_iterator sysListItr;  
//   for (sysListItr = m_sysList.begin(); sysListItr != m_sysList.end(); ++sysListItr){  
//     if ((*sysListItr).name()!="") this->bookHistos((*sysListItr).name());
//   } 
//   //-------------------------------------------------------------------------// 
 
//   TSystem sys;
   m_tEndInitialise=gSystem->Now();
   m_tStartLoop=gSystem->Now();

  return StatusCode::SUCCESS;
}



/*****************************************************************/
StatusCode LapxAODEvent :: execute (){
/*****************************************************************/
//---- for each event in the loop


  //m_event = wk()->xaodEvent();  //--- needed also here if set in the initialise ??

  time_t t = time(0);   // get time now
  struct tm * now = localtime( & t );

  if( (m_eventCounter % 1000) ==1 ) Info("execute()", "Event number = %i", m_eventCounter );
  if( (m_eventCounter % 1000) ==1 ) std::cout << now->tm_hour << ":" << now->tm_min << ":" << now->tm_sec << std::endl; 



 


  //testing new histogramming scheme 
  hist("h_jetPt")->Fill (30.); // GeV
  

    
      // get muon container of interest
  //const xAOD::MuonContainer* muons = 0;
  //ANA_CHECK (evtStore()->retrieve (muons, "Muons"));
  
  //fillElectron();


/*
//---- retrieve primary vertex
//  const xAOD::Vertex *pv(0);
  pv = 0;
  lepv = 0; //--- lepton vertex re-reconstructed only from the lepton tracks later...
  const xAOD::VertexContainer *vertexContainer(0);
  ANA_CHECK(m_event->retrieve( vertexContainer, "PrimaryVertices" ));
  for ( const auto* const vtx_itr : *vertexContainer ) {
    if (vtx_itr->vertexType() != xAOD::VxType::VertexType::PriVtx) continue;
    else { pv = vtx_itr; lepv = vtx_itr; break;}
  }
  

//---- retrieve truth primary vertex
//  const xAOD::TruthVertex *t_pv(0);
  t_pv_z = 0;
  
  if (m_isMC){
    const xAOD::TruthVertexContainer *t_vertexContainer(0);
    ANA_CHECK(m_event->retrieve( t_vertexContainer, "TruthVertices" ));
    for ( const auto* const t_vtx_itr : *t_vertexContainer ) {
      t_pv_z = t_vtx_itr->z(); break; // first item is always truth PV
      //if (t_vtx_itr->barcode() != 0 ) continue; //???
      //else { t_pv = t_vtx_itr; break;}
    }
  }


 
  //----- calculate MC event weights
  if (m_isMC){
       const std::vector< float > weights = m_eventInfo->mcEventWeights();
       if( weights.size() > 0 ) weight = weights[0];
       m_genWeight = weight;


       
       if( (m_eventCounter % 10000) ==1 ) Info("execute()", "MC ID is = %i", m_mcID);

       //---- get pile-up weight
       m_pileupWeight = 1.;
       //-----
       //m_pileupWeight = m_PileupReweightingTool->getCombinedWeight( *m_eventInfo );
       
       //weight = getLbLcosThetaW();
       
       //std::cout<<"truth weight = "<<weight<<std::endl;

       //---- set MC event weight
       //weight = m_genWeight * m_pileupWeight * m_genLumiWeight * vertexWeight(t_pv_z);    
  }    

*/
  
  m_eventCounter++;


  return StatusCode::SUCCESS;
}




/*****************************************************************/
StatusCode LapxAODEvent ::fillEvent()
/*****************************************************************/
{

  //--- clear member variables and lists from one event to the other
  clear();
  
    //------ Event Information --------------------
  m_eventInfo = 0;
  ANA_CHECK(evtStore()->retrieve( m_eventInfo, "EventInfo"));  


  //----- check if the event is data or MC
  m_isMC = false;
  weight = 1.;  
  if(m_eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ) m_isMC = true;  


 // TE5 trigger efficiency
        const xAOD::PhotonContainer* photons = 0;
        ANA_CHECK(evtStore()->retrieve( photons, "Photons" )); 
       // create a shallow copy of the photon container
       std::pair< xAOD::PhotonContainer*, xAOD::ShallowAuxContainer* > m_photons_shallowCopy = xAOD::shallowCopyContainer( *photons );
       // loop over the electrons in the container
       xAOD::PhotonContainer::iterator ph_itr = (m_photons_shallowCopy.first)->begin();
       xAOD::PhotonContainer::iterator ph_end = (m_photons_shallowCopy.first)->end();
        float sum_Et = 0;
	for( ; ph_itr != ph_end; ++ph_itr ) 
        {
//                if( (*clTS_itr)->pt()/1e3 > 1.5 && fabs((*clTS_itr)->etaBE(2)) < 2.47
//                        &&  (fabs( (*clTS_itr)->etaBE(2) ) < 1.37 ||  fabs( (*clTS_itr)->etaBE(2) ) > 1.52) ) {

                   sum_Et += (*ph_itr)->pt()/1e3;

//                 }
        }

	
	// trigger weight for MC
	//if(m_isMC) weight *= (0.5*(TMath::Erf((sum_Et-5.93593)/1.42807)+1.));


	// Pixel cluster sum	
	std::string METTag="HLT_xAOD__TrigSpacePointCountsContainer_spacepoints";	
	const xAOD::TrigSpacePointCountsContainer* SpacePointCountsCont=0;
	if(evtStore()->retrieve( SpacePointCountsCont, METTag ).isSuccess()){

	for(uint i = 0; i < SpacePointCountsCont->size(); i++) {	
	     m_sum_pix += SpacePointCountsCont->at(i)->pixelClusBarrelSumEntries(20., 0., xAOD::TrigHistoCutType::ABOVE_X_ABOVE_Y);
	     m_sum_pix += SpacePointCountsCont->at(i)->pixelClusEndcapASumEntries(20., 0., xAOD::TrigHistoCutType::ABOVE_X_ABOVE_Y);
	     m_sum_pix += SpacePointCountsCont->at(i)->pixelClusEndcapCSumEntries(20., 0., xAOD::TrigHistoCutType::ABOVE_X_ABOVE_Y);
	   }
    	 }
  	
	// part of the trigger for MC simulation
	//if(m_isMC && m_sum_pix>15) return StatusCode::SUCCESS;


  //--- PV requirement
//  if (!pv) return StatusCode::SUCCESS;
  //h_NumOfEvtsAfterPV->Fill(0.5, weight);
  
  fillElectron();  
  fillPhoton();
  fillTrack();
  fillPixelTrack();
  fillCluster();

/*
  fillVertices();

  //---- fill list of good leptons
  fillMuons();
  fillElectrons();

  
  
  
  //m_zdcAnalysisTool->reprocessZdc();
  
  //--- photon number requirement here (should speed up the running?)
//  if ( m_goodPhotons.size() < 2) {
//     return StatusCode::SUCCESS; // go to the next event
//  }


  fillTruthTracks();
  fillTracks();
  
 */ 



  return StatusCode::SUCCESS;

}

/*****************************************************************/
void LapxAODEvent::clear()
/*****************************************************************/
{
//--- clear member variables and lists from one event to the other

  m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 = false;
  m_eventFiredHLT_mb_sptrk_ion_L1ZDC_A_C_VTE50 = false; 
  m_eventFiredHLT_mb_sptrk_vetombts2in_L1ZDC_A_C_VTE50 = false;
  m_eventFiredHLT_hi_loose_upc_L1ZDC_A_C_VTE50 = false;
  m_eventFiredHLT_mb_sptrk_vetombts2in_L1MU0_VTE50 = false; 
  m_eventFiredHLT_TE5 = false; 
  
  m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_noiseSup_L1TEX_VTEY= false;
  m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ= false;
  m_eventFired_2018_exclusivelooseFgapTriggers= false;
  m_eventFired_2018_exclusivelooseNoFgapTriggers= false;
  m_eventFired_2018_EMPTY= false;
  
  m_sum_pix = 0;
  
  //---- delete shallo copies  

  //if(m_electrons_shallowCopy.first){ delete m_electrons_shallowCopy.first; m_electrons_shallowCopy.first=0; }
  //if(m_electrons_shallowCopy.second){ delete m_electrons_shallowCopy.second; m_electrons_shallowCopy.second=0; }

  // if(m_jets_shallowCopy.first){ delete m_jets_shallowCopy.first; m_jets_shallowCopy.first=0;}
  // if(m_jets_shallowCopy.second){ delete m_jets_shallowCopy.second; m_jets_shallowCopy.second=0;}   

/*  if(m_trks_shallowCopy.first){ delete m_trks_shallowCopy.first; m_trks_shallowCopy.first=0; }
  if(m_trks_shallowCopy.second){ delete m_trks_shallowCopy.second; m_trks_shallowCopy.second=0; }

  if(m_muons_shallowCopy.first){ delete m_muons_shallowCopy.first; m_muons_shallowCopy.first=0; }
  if(m_muons_shallowCopy.second){ delete m_muons_shallowCopy.second; m_muons_shallowCopy.second=0; }



  if(m_vtx_shallowCopy.first){ delete m_vtx_shallowCopy.first; m_vtx_shallowCopy.first=0; }
  if(m_vtx_shallowCopy.second){ delete m_vtx_shallowCopy.second; m_vtx_shallowCopy.second=0; }

  if(m_trutrks_shallowCopy.first){ delete m_trutrks_shallowCopy.first; m_trutrks_shallowCopy.first=0; }
  if(m_trutrks_shallowCopy.second){ delete m_trutrks_shallowCopy.second; m_trutrks_shallowCopy.second=0; }


  if(m_photons_shallowCopy.first){ delete m_photons_shallowCopy.first; m_photons_shallowCopy.first=0; }
  if(m_photons_shallowCopy.second){ delete m_photons_shallowCopy.second; m_photons_shallowCopy.second=0; }

  if(m_cl_shallowCopy.first){ delete m_cl_shallowCopy.first; m_cl_shallowCopy.first=0; }
  if(m_cl_shallowCopy.second){ delete m_cl_shallowCopy.second; m_cl_shallowCopy.second=0; }
*/
 
 
  //---- clear list of selected particle
  m_goodElectrons.clear();   
  m_goodPhotons.clear();
  m_goodPhotonsNoPID.clear();
  m_goodPhotonsF1Inv.clear();
  m_goodTracks.clear();
  m_goodPixTracks.clear();
  m_goodClusters.clear();

/*  m_goodElectrons_noID.clear();  

  m_goodMuons.clear();   
  m_goodMuons_LooseTrkIso.clear(); 

  m_goodTracksPriVtx.clear();  
  m_goodVertices.clear();
  
  m_TruthTracksPri.clear();
  m_TracksPriMatched.clear();
  
  m_PtMinBothLeptons=0;
  m_EtaMaxBothLeptonZ=0;
  m_MuonQualityAcceptedCut=0;
  m_MuonIsolation=0;
  m_isCompatible=0;
  m_TwoMuons=0;
*/

}




/*****************************************************************/
StatusCode LapxAODEvent :: postExecute () {
/*****************************************************************/

  return StatusCode::SUCCESS;
}



/*****************************************************************/
StatusCode LapxAODEvent :: finalize () {
/*****************************************************************/
//--- clear everything at the end of the job

    
  m_tEndLoop=gSystem->Now();


    
   

 
//---- print summary of execution time 
    std::cout<<">>>>>>>>>>>>>><<<<<<<<<<<<<<<<\n";
    std::cout<<"Number of processed events : "<<m_eventCounter<<"\n";
    std::cout<<"Initialisation time (total) : "<< (m_tEndInitialise-m_tStartInitialise)/1000.<<" s \n";
    std::cout<<"Event processing time : "<< (m_tEndLoop-m_tStartLoop)/1000.<<" s \n";
    if(m_eventCounter) std::cout<<"Event processing time : "<< (m_tEndLoop-m_tStartLoop)/m_eventCounter/1000.<<" s/event \n";

    std::cout<<">>>>>>>>>>>>>><<<<<<<<<<<<<<<<\n";
    std::cout<<">>>>> This is the end... <<<<<\n";
    std::cout<<">>>>>>>>>>>>>><<<<<<<<<<<<<<<<\n";
   
  return StatusCode::SUCCESS;
}



/*****************************************************************/
StatusCode LapxAODEvent :: histFinalize () {
/*****************************************************************/
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.

  return StatusCode::SUCCESS;
}


/*****************************************************************/
double LapxAODEvent::vertexWeight(double z){
/*****************************************************************/	

  if(!m_isMC) return 1.;
  
//---- reweight the z-vertex distribution

	return 1.;
  
}



//---- fill list of electrons
/*****************************************************************/
StatusCode LapxAODEvent::fillElectron()
/*****************************************************************/
{

    //---- retrieve list of electrons from xAOD
    const xAOD::ElectronContainer* electrons = 0;
    ANA_CHECK(evtStore()->retrieve( electrons, "Electrons" ));  

  
    // create a shallow copy of the electron container
    std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > m_electrons_shallowCopy = xAOD::shallowCopyContainer( *electrons );

       
    // loop over the electrons in the container
    xAOD::ElectronContainer::iterator elec_itr = (m_electrons_shallowCopy.first)->begin();
    xAOD::ElectronContainer::iterator elec_end = (m_electrons_shallowCopy.first)->end();
    
    double el_d0sig = -999;
    
    for( ; elec_itr != elec_end; ++elec_itr ) { //loop over electrons in the container
    
    
    //std::cout<< eleSC->pt() << std::endl;
     
      //--- calibrate electrons (maybe should be decoupled from selection later)
      //if(EVENTDEBUG) printf("Elec calib. before pTe=%f \n",(*elec_itr)->pt());
      //m_electronCalibrationTool->applyCorrection(*(*elec_itr)).ignore();
      //if(EVENTDEBUG) printf("Elec calib. after pTe=%f \n",(*elec_itr)->pt());
 
      
      // Formally unnecessary because all electrons in the container have these authors by construction
//      if ( !((*elec_itr)->author(xAOD::EgammaParameters::AuthorElectron)|| (*elec_itr)->author(xAOD::EgammaParameters::AuthorAmbiguous)) ) continue;
      
      //bool LHlooseSel = m_LHToolLoose2015->accept((*elec_itr));
    
      //add electron hits
      
      //const xAOD::TrackParticle* tp = (*elec_itr)->trackParticle();
      //if(tp && m_eventInfo) el_d0sig = xAOD::TrackingHelpers::d0significance( tp, m_eventInfo->beamPosSigmaX(), m_eventInfo->beamPosSigmaY(), m_eventInfo->beamPosSigmaXY() );

      //bool pass_d0sign_elec = false;
      //pass_d0sign_elec = ( tp && fabs(el_d0sig)<5 );
      
//      bool pass_z0_elec = false;
//      pass_z0_elec = ( tp && pv && fabs((tp->z0()+tp->vz() - pv->z())*sin(tp->theta()))<0.5 );

     if ( (*elec_itr)->pt() > 2000. 
      	   && fabs( (*elec_itr)->caloCluster()->etaBE(2) ) < 2.47 
	   && (fabs( (*elec_itr)->caloCluster()->etaBE(2) ) < 1.37 ||  fabs( (*elec_itr)->caloCluster()->etaBE(2) ) > 1.52)   
	   
	   //&& m_EMToolLoose2015->accept((*elec_itr)) 
	   
	   && (*elec_itr)->passSelection("Loose")	   
	   //&& LHlooseSel 	   
	   //&& pass_d0sign_elec 
	   //&& pass_z0_elec 
	   //&& (m_electronIsolationSelectionTool->accept(**elec_itr))
	  // && (*elec_itr)->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON)
	   
	   ) m_goodElectrons.push_back( (*elec_itr) ); 

/*
      if ( (*elec_itr)->pt() > 3000. //&& (*elec_itr)->pt() < 6000.
      	   //&& fabs( (*elec_itr)->eta() ) < 2.5 
      	   && fabs( (*elec_itr)->caloCluster()->etaBE(2) ) < 2.47 
	   && (fabs( (*elec_itr)->caloCluster()->etaBE(2) ) < 1.37 ||  fabs( (*elec_itr)->caloCluster()->etaBE(2) ) > 1.52)   

	   && (*elec_itr)->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON)
	   
	   ) m_goodElectrons_noID.push_back( (*elec_itr) );
*/

    } //end loop over electrons in the container 



    //--- sort leptons by pT order
//    std::sort(m_goodElectrons.begin(),m_goodElectrons.end(), [ ]( const xAOD::Electron* e1, const xAOD::Electron* e2 )
//    {
//      return e1->pt() > e2->pt();
//    });


  return StatusCode::SUCCESS;

}


/*****************************************************************/
StatusCode LapxAODEvent::fillPhoton()
/*****************************************************************/
{
    //---- fill lists of photons
    const xAOD::PhotonContainer* photons = 0;
    ANA_CHECK(evtStore()->retrieve( photons, "Photons" ));  



    // create a shallow copy of the photon container
    std::pair< xAOD::PhotonContainer*, xAOD::ShallowAuxContainer* > m_photons_shallowCopy = xAOD::shallowCopyContainer( *photons );
       
    // loop over the electrons in the container
    xAOD::PhotonContainer::iterator ph_itr = (m_photons_shallowCopy.first)->begin();
    xAOD::PhotonContainer::iterator ph_end = (m_photons_shallowCopy.first)->end();

    
    for( ; ph_itr != ph_end; ++ph_itr ) { //loop over photons in the container    
    
    //std::cout<< phSC->pt() ;
      
 /*     
      if( (*ph_itr)->pt() < 2000.) continue; // to remove low-et photons
  
      //--- calibrate photons (maybe should be decoupled from selection later)
      if(EVENTDEBUG) printf("Photon calib. before pTe=%f \n",(*ph_itr)->pt());
      
      //m_electronCalibrationTool->applyCorrection(**ph_itr).ignore();
      //if(!m_isMC) m_isolationCorrectionTool_data->applyCorrection(**ph_itr).ignore();
      
      if(EVENTDEBUG) printf("Photon calib. after pTe=%f \n",(*ph_itr)->pt());
      
      //apply fudge correction on MC (rel 20.1)
      if(m_isMC) m_fudgeMCTool->applyCorrection(**ph_itr).ignore();
 */
      

      uint16_t author =  (*ph_itr)->author();
      bool photonAuthor = ( (author & xAOD::EgammaParameters::AuthorPhoton) || (author & xAOD::EgammaParameters::AuthorAmbiguous)
      			|| (author & xAOD::EgammaParameters::AuthorCaloTopo35) );
	
      bool photonCleaning = !( ((*ph_itr)->OQ()&1073741824)!=0 ||
        ( ((*ph_itr)->OQ()&134217728)!=0 &&
          ((*ph_itr)->showerShapeValue(xAOD::EgammaParameters::Reta) >0.98
           ||(*ph_itr)->showerShapeValue(xAOD::EgammaParameters::f1) > 0.4
           ||((*ph_itr)->OQ()&67108864)!=0)  ) );

      if ( (*ph_itr)->pt() > 2500.
      	   && fabs( (*ph_itr)->caloCluster()->etaBE(2) ) < 2.37 
	   && (fabs( (*ph_itr)->caloCluster()->etaBE(2) ) < 1.37 ||  fabs( (*ph_itr)->caloCluster()->etaBE(2) ) > 1.52)  
	   
	   //&& m_photonLooseIsEMSelector->accept(**ph_itr)
	   
	   //&& (*ph_itr)->showerShapeValue(xAOD::EgammaParameters::f1)>0.1
	   //&& (*ph_itr)->showerShapeValue(xAOD::EgammaParameters::f1)<0.8
	   //&& (*ph_itr)->showerShapeValue(xAOD::EgammaParameters::Eratio)>0.6
	   //&& (*ph_itr)->showerShapeValue(xAOD::EgammaParameters::weta2)<0.014
	   
	   //&& (*ph_itr)->showerShapeValue(xAOD::EgammaParameters::Eratio)>0.53
	   //&& (*ph_itr)->showerShapeValue(xAOD::EgammaParameters::Rhad1)<0.0554
 	   
	   //&& (*ph_itr)->passSelection("Loose")
	   //&& m_photonIsoSelToolLoose->accept(**ph_itr)	   
	   //&& m_egammaAmbiguityTool->accept( **ph_itr )
          
	   && photonAuthor
	   && photonCleaning
	   && (*ph_itr)->isGoodOQ(xAOD::EgammaParameters::BADCLUSPHOTON) 
	   ){

		m_goodPhotonsNoPID.push_back( (*ph_itr) );
		
		double ph_eta =  (*ph_itr)->caloCluster()->etaBE(2); 
		double ph_weta2 =  (*ph_itr)->showerShapeValue(xAOD::EgammaParameters::weta2);
		double ph_eratio =  (*ph_itr)->showerShapeValue(xAOD::EgammaParameters::Eratio);
		double ph_f1 =  (*ph_itr)->showerShapeValue(xAOD::EgammaParameters::f1);

		//PID selection in eta bins
		if( fabs( ph_eta ) < 0.6 ){
		  if( ((ph_f1 < 0.8) and (ph_f1 > 0.0452)) and (ph_weta2 < 0.0162) and (ph_eratio > 0.503) ) m_goodPhotons.push_back( (*ph_itr) );
		} 
		else if ( fabs(ph_eta)>=0.6 and fabs(ph_eta)< 1.37 ){
		  if( ((ph_f1 < 0.8) and (ph_f1 > 0.0368)) and (ph_weta2 < 0.0157) and (ph_eratio > 0.140) ) m_goodPhotons.push_back( (*ph_itr) );
		}
		else if ( fabs(ph_eta)>=1.52 and fabs(ph_eta)< 1.81 ){
		  if( ((ph_f1 < 0.8) and (ph_f1 > 0.0783)) and (ph_weta2 < 0.0156) and (ph_eratio > 0.195) ) m_goodPhotons.push_back( (*ph_itr) );
		}
		else if ( fabs(ph_eta)>=1.81 and fabs(ph_eta)< 2.37 ){
		  if( ((ph_f1 < 0.8) and (ph_f1 > -0.00663)) and (ph_weta2 < 0.0146) and (ph_eratio > 0.837) ) m_goodPhotons.push_back( (*ph_itr) );
		}


		//Inverted f1 PID selection for fake bkg estimation
		if( fabs( ph_eta ) < 0.6 ){
		  if( ((ph_f1 > 0.8) or (ph_f1 < 0.0452)) and (ph_weta2 < 0.0162) and (ph_eratio > 0.503) ) m_goodPhotonsF1Inv.push_back( (*ph_itr) );
		} 
		else if ( fabs(ph_eta)>=0.6 and fabs(ph_eta)< 1.37 ){
		  if( ((ph_f1 > 0.8) or (ph_f1 < 0.0368)) and (ph_weta2 < 0.0157) and (ph_eratio > 0.140) ) m_goodPhotonsF1Inv.push_back( (*ph_itr) );
		}
		else if ( fabs(ph_eta)>=1.52 and fabs(ph_eta)< 1.81 ){
		  if( ((ph_f1 > 0.8) or (ph_f1 < 0.0783)) and (ph_weta2 < 0.0156) and (ph_eratio > 0.195) ) m_goodPhotonsF1Inv.push_back( (*ph_itr) );
		}
		else if ( fabs(ph_eta)>=1.81 and fabs(ph_eta)< 2.37 ){
		  if( ((ph_f1 > 0.8) or (ph_f1 < -0.00663)) and (ph_weta2 < 0.0146) and (ph_eratio > 0.837) ) m_goodPhotonsF1Inv.push_back( (*ph_itr) );
		}
	   
	      //m_goodPhotons.push_back( (*ph_itr) );
	      //std::cout << " -> this is a good photon" << std::endl;

	   }
	   
	   
    } //end loop over photons in the container 



    //--- sort photons by pT order
    std::sort(m_goodPhotons.begin(),m_goodPhotons.end(), [ ]( const xAOD::Photon* g1, const xAOD::Photon* g2 )
    {
      return g1->pt() > g2->pt();
    });


  return StatusCode::SUCCESS;

}


/*****************************************************************/
StatusCode LapxAODEvent::fillCluster()
/*****************************************************************/
{
    //---- fill lists of photons
    const xAOD::CaloClusterContainer* clusters = 0;
    //ANA_CHECK(evtStore()->retrieve( clusters, "CaloCalTopoClusters" ));  
    ANA_CHECK(evtStore()->retrieve( clusters, "egammaClusters" )); 



    // create a shallow copy of the photon container
    std::pair< xAOD::CaloClusterContainer*, xAOD::ShallowAuxContainer* > m_clusters_shallowCopy = xAOD::shallowCopyContainer( *clusters );
       
    // loop over the electrons in the container
    xAOD::CaloClusterContainer::iterator ph_itr = (m_clusters_shallowCopy.first)->begin();
    xAOD::CaloClusterContainer::iterator ph_end = (m_clusters_shallowCopy.first)->end();

    
    for( ; ph_itr != ph_end; ++ph_itr ) { //loop over clusters in the container    
      


      if ( (*ph_itr)->pt() > 2000.
      	   && fabs( (*ph_itr)->eta() ) < 2.37 
	   && (fabs( (*ph_itr)->eta() ) < 1.37 ||  fabs( (*ph_itr)->eta() ) > 1.52)  
	   ){


	   m_goodClusters.push_back( (*ph_itr) );
		

	   }
	   
	   
    } //end loop over clusters in the container 



  return StatusCode::SUCCESS;

}


/*****************************************************************/
StatusCode LapxAODEvent::fillTrack()
/*****************************************************************/
{

    //---- retrieve list of tracks from xAOD
    const xAOD::TrackParticleContainer* tracks = 0;
    ANA_CHECK(evtStore()->retrieve( tracks, "InDetTrackParticles" ));  

  
    // create a shallow copy of the trk container
    std::pair< xAOD::TrackParticleContainer*, xAOD::ShallowAuxContainer* > m_trks_shallowCopy = xAOD::shallowCopyContainer( *tracks );
    
    // iterate over our shallow copy
    xAOD::TrackParticleContainer::iterator trk_itr = (m_trks_shallowCopy.first)->begin();
    xAOD::TrackParticleContainer::iterator trk_end = (m_trks_shallowCopy.first)->end();
    
    for( ; trk_itr != trk_end; ++trk_itr ) { //loop over tracks in the container
    
       uint8_t npix = 0;
       if(!(*trk_itr)->summaryValue(npix,      xAOD::numberOfPixelHits))        ANA_MSG_ERROR( "Pix hits not filled for standard tracks");

       uint8_t nsct = 0;
       if(!(*trk_itr)->summaryValue(nsct,      xAOD::numberOfSCTHits))        ANA_MSG_ERROR( "SCT hits not filled");

       //std::cout<< trkSC->pt() << std::endl;
       //std::cout<< trkSC->eta() << std::endl;
       //std::cout<< npix << std::endl;
       //std::cout<< npix+nsct << std::endl;
       //std::cout<< std::endl;

        // --- fill selected tracks
        if (npix>=1 && (npix+nsct)>5) m_goodTracks.push_back( (*trk_itr) );

    }
 /*   
    
    const xAOD::TrackParticleContainer* tracks(0);
    m_event->retrieve( tracks, "InDetTrackParticles" ).ignore(); 

    // create a shallow copy of the trk container
    //std::pair< xAOD::TrackParticleContainer*, xAOD::ShallowAuxContainer* > 
    m_trks_shallowCopy = xAOD::shallowCopyContainer( *tracks );
    
    // iterate over our shallow copy
    xAOD::TrackParticleContainer::iterator trk_itr = (m_trks_shallowCopy.first)->begin();
    xAOD::TrackParticleContainer::iterator trk_end = (m_trks_shallowCopy.first)->end();
    
    for( ; trk_itr != trk_end; ++trk_itr ) { //loop over tracks in the container
    
      
      if ( m_trkSelTool->accept(**trk_itr)) { //!!!!!!!! uncommented


	m_goodTracksPriVtx.push_back( (*trk_itr) );  
      
        // --- fill all selected tracks
        m_goodTracks.push_back( (*trk_itr) );	
	
	//--- fill tracks associated to primary vertex
	xAOD::TrackParticle* track = *trk_itr;

//	double trk_d0sig = xAOD::TrackingHelpers::d0significance( track, m_eventInfo->beamPosSigmaX(), m_eventInfo->beamPosSigmaY(), m_eventInfo->beamPosSigmaXY() );
   
	
	//if( 1 fabs((track->z0() - pv->z() + track->vz())*sin(track->theta())) < 1.5 && fabs(track->d0()) < 1.5 ){
	//if (m_trkToVxTool->isCompatible( *track, *pv_mu) ){  /// CHANGED!!!!
 
	//}
      }

    } // end of loop over tracks in the container 
  */  

    //--- sort tracks by pT order
    std::sort(m_goodTracks.begin(),m_goodTracks.end(), [ ]( const xAOD::TrackParticle* p1, const xAOD::TrackParticle* p2 )
    {
      return p1->pt() > p2->pt();
    });


  return StatusCode::SUCCESS;

}



/*****************************************************************/
StatusCode LapxAODEvent::fillPixelTrack()
/*****************************************************************/
{

    //---- retrieve list of tracks from xAOD
    const xAOD::TrackParticleContainer* pixtracks = 0;
    ANA_CHECK(evtStore()->retrieve( pixtracks, "InDetPixelTrackParticles" ));  

  
    // create a shallow copy of the trk container
    std::pair< xAOD::TrackParticleContainer*, xAOD::ShallowAuxContainer* > m_pixtrks_shallowCopy = xAOD::shallowCopyContainer( *pixtracks );
    
    // iterate over our shallow copy
    xAOD::TrackParticleContainer::iterator trk_itr = (m_pixtrks_shallowCopy.first)->begin();
    xAOD::TrackParticleContainer::iterator trk_end = (m_pixtrks_shallowCopy.first)->end();
    
    for( ; trk_itr != trk_end; ++trk_itr ) { //loop over tracks in the container
    
       uint8_t npixhit = 0;
       if(!(*trk_itr)->summaryValue(npixhit,      xAOD::numberOfPixelHits))        ANA_MSG_ERROR( "Pix hits not filled for pixelTracks");

       //std::cout<< pixtrkSC->pt() << std::endl;
       //std::cout<< pixtrkSC->eta() << std::endl;       
       //std::cout<< "hits above 2"<< std::endl;
       //std::cout<< npix+nsct << std::endl;
       //std::cout<< std::endl;

        // --- fill all selected tracks
       
       if (npixhit>2) m_goodPixTracks.push_back( (*trk_itr) );

    }


  return StatusCode::SUCCESS;

}


/**********************************************************************************/                                                                                          
bool LapxAODEvent::configureToolsForSystematics(CP::SystematicSet sysUsed)
/**********************************************************************************/                                                                                          
{
//----- apply a systematic shift to all tool used


//---- muons
/*    if( m_muonCalibrationAndSmearingTool->applySystematicVariation( sysUsed ) != CP::SystematicCode::Ok ) {
      Error("execute()", "Cannot configure muon calibration tool for systematic" );
      return false; 
    } 
    if( m_muonEfficiencyCorrection_L->applySystematicVariation( sysUsed ) != CP::SystematicCode::Ok ) {
      Error("execute()", "Cannot configure muon efficiency tool for systematic" );
      return false; 
    } 
*/

    
    return true; //---- all ok

}





