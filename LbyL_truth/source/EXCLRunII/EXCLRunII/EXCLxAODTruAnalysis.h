#ifndef EXCLRunII_EXCLxAODTruAnalysis_H
#define EXCLRunII_EXCLxAODTruAnalysis_H

#include <EXCLRunII/EXCLUserParticle.h>
#include <EventLoop/Algorithm.h>

#include "xAODTruth/TruthEventContainer.h"

class EXCLxAODTruAnalysis : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;
  int m_eventCounter;  //!
  uint m_EventTrueType; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!



  // this is a standard constructor
  EXCLxAODTruAnalysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  //ClassDef(EXCLxAODTruAnalysis, 1);

  // member functions
  virtual EL::StatusCode EXCLTruEleMu_BuildIn();  
  void EXCLTruEleMu_RS();       //!
  virtual EL::StatusCode EXCLTruIsoTau();

  inline void SetEventTrueType (int truevtype) {m_EventTrueType = truevtype;}
  inline int  GetEventTrueType() {return m_EventTrueType;}

  bool FidRegionPtEtaLept();
  
  // Following: * m_genWMuon is a pointer 
  EXCLUserParticle* m_genZ; //!
  EXCLUserParticle* m_genW; //!  

  EXCLUserParticle* m_genWMuonDRESS; //!
  EXCLUserParticle* m_genZMuon1DRESS; //!
  EXCLUserParticle* m_genZMuon2DRESS; //!
  EXCLUserParticle* m_genWNeutrinoMu; //! 

EXCLUserParticle* m_genWPhotonDRESS; //!
  EXCLUserParticle* m_genZPhoton1DRESS; //!
  EXCLUserParticle* m_genZPhoton2DRESS; //!


  EXCLUserParticle* m_genWElectronDRESS; //! 
  EXCLUserParticle* m_genZElectron1DRESS; //! 
  EXCLUserParticle* m_genZElectron2DRESS; //!
  EXCLUserParticle* m_genWNeutrinoEle; //! 

  inline EXCLUserParticle* genWMuonDRESS() {return m_genWMuonDRESS;};      

  // m_genWMuon is the object. The 2nd line substitute the inline 
  // EXCLUserParticle m_genWMuon; //!
  // EXCLUserParticle& genWMuon() {return m_genWMuon;}; 

  void PrintIDStatusPart (const xAOD::TruthParticle* tx); //!
  void PrintOrigtExtractedPart( int  us_pdgId, uint  us_status,  uint  us_barcode, uint us_type, uint us_orig ); //!
};

#endif
