/**
 *  @file  RecoAnalysis
 *  @brief  to gather-up all information and all corrections related to one xAOD event for the analysis
 *
 *
 *  @author  
 *
 *  @date    2015-11-07
 *
 *  @internal
 *     Created : 
 * Last update : 
 *          by : 
 *
 * =====================================================================================
 */

#include <AsgTools/MessageCheck.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <EXCLRunII/EXCLRecoAnalysis.h>

/*
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
// EDM includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODRootAccess/tools/Message.h"

#include "PATInterfaces/CorrectionCode.h" 
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODCore/ShallowCopy.h"
*/


#include <TSystem.h>

#include <TFile.h>
#include <TRandom3.h>

#include <ctime>
#include <iostream>





// this is needed to distribute the algorithm to the workers
//ClassImp(EXCLRecoAnalysis)



/*****************************************************************/
EXCLRecoAnalysis :: EXCLRecoAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : LapxAODEvent (name, pSvcLocator),
       m_trigDecisionTool("Trig::TrigDecisionTool/TrigDecisionTool"),
       m_trigConfigTool("TrigConf::xAODConfigTool/xAODConfigTool"), 
       m_grl ("GoodRunsListSelectionTool/grl", this)//,
      // m_electronCalibrationTool("m_electronCalibrationTool")
/*****************************************************************/
{
//----- Standard constructor

}


/*****************************************************************/
EL::AnaAlgorithm* EXCLRecoAnalysis::ConnectToAnotherAlgorithm(std::string name){
/*****************************************************************/
//---- get the pointer to another algorithm

  EL::AnaAlgorithm* alg = (EL::AnaAlgorithm*) wk()->getAlg(name);
  if(!alg){
    std::cout<<"Error! Could not find alg " << name  << ". Aborting!" <<"\n";
    std::abort();
  }
  return alg;
}



/*
StatusCode EXCLRecoAnalysis :: setupJob (Job& job) {

  
  job.useXAOD ();

  ANA_CHECK( "setupJob()", xAOD::Init() ); // call before opening first file
  
  return StatusCode::SUCCESS;
}
*/


/*****************************************************************/
StatusCode EXCLRecoAnalysis :: histInitialize (){
/*****************************************************************/

  
  std::cout<<"Initializing histograms..."<<std::endl;
   
  Histogram_map.clear();

  this->bookHistos("1_electron_studies_2015"); 
  this->bookHistos("1_electron_studies_2018_AcoCutTightTrk");
  this->bookHistos("1_electron_studies_2018_L1TAU");
  
  this->bookHistos("2_LbLSel_1MCut");
  this->bookHistos("2_LbLSel_2NTrkVeto");
  this->bookHistos("2_LbLSel_3PtCut2GeV"); 
  this->bookHistos("2_LbLSel_3PtCutInverted");
//  this->bookHistos("2_LbLSel_3PtCutInvertedNoMSTP");
//  this->bookHistos("2_LbLSel_3PtCutInvertedNoMSTPtimeCut");
  this->bookHistos("2_LbLSel_4PixTrkVeto");
  this->bookHistos("2_LbLSel_4PixTrkSumPix5");
  this->bookHistos("2_LbLSel_3GeV_4PixTrkSumPix5");
  
//  this->bookHistos("2_LbLSel_5MuonVeto");
  this->bookHistos("2_LbLSel_6AcoCut");
  
  this->bookHistos("2_LbLSel_3GeV_6AcoCut");
  this->bookHistos("2_LbLSel_3GeV_CtrlNoAcoNoPixTrk");
  this->bookHistos("2_LbLSel_3GeV_CtrlNoAco1PixTrk");
  this->bookHistos("2_LbLSel_3GeV_CtrlNoAco2PixTrk");
  
  this->bookHistos("2_LbLSel_CtrlNoAcoPt");
  this->bookHistos("2_LbLSel_CtrlNoAco");
  this->bookHistos("2_LbLSel_CtrlNoAcoNoPixTrk");
  this->bookHistos("2_LbLSel_CtrlNoAco1PixTrk");
  this->bookHistos("2_LbLSel_CtrlNoAco2PixTrk");
//  this->bookHistos("2_LbLSel_CtrlNoAcoNoPixTrkNoTime");
   this->bookHistos("2_LbLSel_CtrlPixTrk");

  this->bookHistos("2_LbLSel_Ctrl1Trk");
  this->bookHistos("2_LbLSel_Ctrl2Trk");
  
  this->bookHistos("2_LbLSel_1MCut_EMPTY");
  this->bookHistos("2_LbLSel_2PtCut4GeV_EMPTY");

  
/*  
  this->bookHistos("0_Default");
  this->bookHistos("0_NoTracks");
  this->bookHistos("0_2Tracks");
  this->bookHistos("0_FakesL1Mu0");
  
  this->bookHistos("1_HLT_hi_gg_upc_L1TE5_VTE200_2Tracks");
  this->bookHistos("1_HLT_hi_gg_upc_L1TE5_VTE200_2Tracks8PixHits");
  this->bookHistos("1_HLT_hi_gg_upc_L1TE5_VTE200_2Tracks8PixHitsEleSF");
  this->bookHistos("1_HLT_hi_gg_upc_L1TE5_VTE200_2TracksPt5GeV");
  this->bookHistos("1_HLT_hi_gg_upc_L1TE5_VTE200_2TracksEta137");
  
  this->bookHistos("1_HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50_2Tracks");
  this->bookHistos("1_HLT_mb_sptrk_vetombts2in_L1ZDC_A_C_VTE50_2Tracks");
  this->bookHistos("1_HLT_hi_loose_upc_L1ZDC_A_C_VTE50_2Tracks");
  this->bookHistos("1_HLT_mb_sptrk_vetombts2in_L1MU0_VTE50_2Tracks");
  
  this->bookHistos("1_HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50_2Tracks_TE5");
  
  
  this->bookHistos("2_LbLSel_1MCut");
  this->bookHistos("2_LbLSel_2AcoCut");
  this->bookHistos("2_LbLSel_1MCutEt3GeV");
  this->bookHistos("2_LbLSel_2AcoCutEt3GeV"); 
 

  

  this->bookHistos("2_LbLSel_CtrlPtAbove2GeV");  
  this->bookHistos("2_LbLSel_CtrlNoAcoPt");
  this->bookHistos("2_LbLSel_CtrlNoAco");
  
  this->bookHistos("2_LbLSel_1PhotonF1Cut");
  this->bookHistos("2_LbLSel_2PhotonWeta2Cut");
  this->bookHistos("2_LbLSel_3PhotonEratioCut");
  this->bookHistos("2_LbLSel_4PhotonF1UpCut");
  this->bookHistos("2_LbLSel_4PhotonF1InvertedCut");
  this->bookHistos("2_LbLSel_5PhotonF1UpPtCut");
  
  this->bookHistos("2_LbLSel_0dRCut");
  this->bookHistos("2_LbLSel_0dRPtCut");
  this->bookHistos("2_LbLSel_0dRPt23CutTemplate");
  this->bookHistos("2_LbLSel_0dRPt34CutTemplate");
  this->bookHistos("2_LbLSel_0dRPt45CutTemplate");
  
  
    
  this->bookHistos("1Ele_1Photon");
*/
  
  
  
  std::cout<<"... EXCLRecoAnalysis histograms initialized"<<std::endl;
  
  gRandom->SetSeed(0);
  
  return StatusCode::SUCCESS;
}



/*****************************************************************/
StatusCode EXCLRecoAnalysis :: fileExecute ()
/*****************************************************************/
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed

  return StatusCode::SUCCESS;
}



/*****************************************************************/
StatusCode EXCLRecoAnalysis :: changeInput (bool firstFile)
/*****************************************************************/
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  
  return StatusCode::SUCCESS;
}



/*****************************************************************/
StatusCode EXCLRecoAnalysis :: initialize ()
/*****************************************************************/
{
  
  std::cout << "In EXCLRecoAnalysis::initialize"<<"\n";
  //std::cout << "algo name is " << GetName() <<"\n";

  //m_xAODEvent = (LapxAODEvent*) ConnectToAnotherAlgorithm("AnalysisAlg");

  // Initialise neural network for photon id
  use_nn_pid = true;
  use_nn_only_3 = false;
  std::string nn_photon_folder = gSystem->ExpandPathName("$UserAnalysis_DIR/data/EXCLRunII");
  std::string nn_photon_paths[4];

  if (!use_nn_only_3) {
    nn_photon_paths[0] = nn_photon_folder + "/nn_photon_id_003_eta_1.model";
    nn_photon_paths[1] = nn_photon_folder + "/nn_photon_id_003_eta_2.model";
    nn_photon_paths[2] = nn_photon_folder + "/nn_photon_id_003_eta_3.model";
    nn_photon_paths[3] = nn_photon_folder + "/nn_photon_id_003_eta_4.model";
    nn_photon_thresholds[0] = 0.8880;
    nn_photon_thresholds[1] = 0.7800;
    nn_photon_thresholds[2] = 0.4423;
    nn_photon_thresholds[3] = 0.5091;
  } else {
    nn_photon_paths[0] = nn_photon_folder + "/nn_photon_id_first_eta_1.model";
    nn_photon_paths[1] = nn_photon_folder + "/nn_photon_id_first_eta_2.model";
    nn_photon_paths[2] = nn_photon_folder + "/nn_photon_id_first_eta_3.model";
    nn_photon_paths[3] = nn_photon_folder + "/nn_photon_id_first_eta_4.model";
    nn_photon_thresholds[0] = 0.786;
    nn_photon_thresholds[1] = 0.747;
    nn_photon_thresholds[2] = 0.4303;
    nn_photon_thresholds[3] = 0.4848;
  }
  for (int i = 0; i < 4; i++) {
    if (use_nn_pid) {
      ANA_MSG_INFO("Using photon id NN model (eta bin " << i + 1 << ") from " << nn_photon_paths[i]
          << " with threshold = " << nn_photon_thresholds[i]);
      nn_photon_models[i].LoadModel(nn_photon_paths[i]);
    }
  }
  
  histInitialize ();

  
// count number of events
//   m_eventCounter = 0;  
//   m_numCleanEvents = 0;
  num2muon = 0;
  num2ele = 0;  
  num34muon = 0;   
  numforEXCLevents = 0;  
  CutFlowMu_TotalNumber = 0; 
  CutFlowMu_15GeV = 0;
  CutFlowMu_15GeV_d0sig = 0;
  CutFlowMu_15GeV_d0sig_z0 = 0;
  
  CutFlowEl_TotalNumber = 0; 
  CutFlowEl_15GeV = 0;
  CutFlowEl_15GeV_d0sig = 0;
  CutFlowEl_15GeV_d0sig_z0 = 0;  
  zeropileup = 0;
  
  
  h_Clusters_SumEt_2015 = new TH1F("1_Clusters/SumEt_2015", "SumEt_2015", 50, 0.,50.); // 
  wk()->addOutput (h_Clusters_SumEt_2015);
  
  h_Clusters_SumEt_2018_noiseSup = new TH1F("1_Clusters/SumEt_2018_noiseSup", "SumEt_2018_noiseSup", 50, 0.,50.); // 
  wk()->addOutput (h_Clusters_SumEt_2018_noiseSup);
  
  h_Clusters_SumEt_2018_L1TAU = new TH1F("1_Clusters/SumEt_2018_L1TAU", "SumEt_2018_L1TAU", 50, 0.,50.); // 
  wk()->addOutput (h_Clusters_SumEt_2018_L1TAU);
  
  h_Clusters_SumEt_2018_excllooseNoFgap = new TH1F("1_Clusters/SumEt_2018_excllooseNoFgap", "SumEt_2018_excllooseNoFgap", 50, 0.,50.); // 
  wk()->addOutput (h_Clusters_SumEt_2018_excllooseNoFgap);
  h_Clusters_SumEt_2018_excllooseFgap = new TH1F("1_Clusters/SumEt_2018_excllooseFgap", "SumEt_2018_excllooseFgap", 50, 0.,50.); // 
  wk()->addOutput (h_Clusters_SumEt_2018_excllooseFgap);  

  h_Clusters_SumEt_2018_excllooseFgap_ZDC_AND = new TH1F("1_Clusters/SumEt_2018_excllooseFgap_ZDC_AND", "SumEt_2018_excllooseFgap_ZDC_AND", 50, 0.,50.); // 
  wk()->addOutput (h_Clusters_SumEt_2018_excllooseFgap_ZDC_AND);
    h_Clusters_SumEt_2018_excllooseFgap_ZDC_XOR = new TH1F("1_Clusters/SumEt_2018_excllooseFgap_ZDC_XOR", "SumEt_2018_excllooseFgap_ZDC_XOR", 50, 0.,50.); // 
  wk()->addOutput (h_Clusters_SumEt_2018_excllooseFgap_ZDC_XOR);
    h_Clusters_SumEt_2018_excllooseFgap_VZDC = new TH1F("1_Clusters/SumEt_2018_excllooseFgap_VZDC", "SumEt_2018_excllooseFgap_VZDC", 50, 0.,50.); // 
  wk()->addOutput (h_Clusters_SumEt_2018_excllooseFgap_VZDC);
  
  h_Clusters_SumEt_2018_noiseSup_AND_excllooseNoFgap = new TH1F("1_Clusters/SumEt_2018_noiseSup_AND_excllooseNoFgap", "SumEt_2018_noiseSup_AND_excllooseNoFgap", 50, 0.,50.); // 
  wk()->addOutput (h_Clusters_SumEt_2018_noiseSup_AND_excllooseNoFgap);  
  h_Clusters_SumEt_2018_noiseSup_AND_excllooseFgap = new TH1F("1_Clusters/SumEt_2018_noiseSup_AND_excllooseFgap", "SumEt_2018_noiseSup_AND_excllooseFgap", 50, 0.,50.); // 
  wk()->addOutput (h_Clusters_SumEt_2018_noiseSup_AND_excllooseFgap); 
  h_Clusters_SumEt_2018_L1TAU_AND_excllooseNoFgap = new TH1F("1_Clusters/SumEt_2018_L1TAU_AND_excllooseNoFgap", "SumEt_2018_L1TAU_AND_excllooseNoFgap", 50, 0.,50.); // 
  wk()->addOutput (h_Clusters_SumEt_2018_L1TAU_AND_excllooseNoFgap);
  h_Clusters_SumEt_2018_L1TAU_AND_excllooseFgap = new TH1F("1_Clusters/SumEt_2018_L1TAU_AND_excllooseFgap", "SumEt_2018_L1TAU_AND_excllooseFgap", 50, 0.,50.); // 
  wk()->addOutput (h_Clusters_SumEt_2018_L1TAU_AND_excllooseFgap);
  
  h_Clusters_SumEt_2018_L1TAU_OR_TE4_AND_excllooseFgap = new TH1F("1_Clusters/SumEt_2018_L1TAU_OR_TE4_AND_excllooseFgap", "SumEt_2018_L1TAU_OR_TE4_AND_excllooseFgap", 50, 0.,50.); // 
  wk()->addOutput (h_Clusters_SumEt_2018_L1TAU_OR_TE4_AND_excllooseFgap);
  
  h_PixelSum_2018_noiseSup = new TH1F("1_PixelSum/PixelSum_2018_noiseSup", "PixelSum_2018_noiseSup", 40, 0.,40.); // 
  wk()->addOutput (h_PixelSum_2018_noiseSup);
  h_PixelSum_2018_L1TAU = new TH1F("1_PixelSum/PixelSum_2018_L1TAU", "PixelSum_2018_L1TAU", 40, 0.,40.); // 
  wk()->addOutput (h_PixelSum_2018_L1TAU);

  h_PixelSum_2018_excllooseFgap = new TH1F("1_PixelSum/PixelSum_2018_excllooseFgap", "PixelSum_2018_excllooseFgap", 40, 0.,40.); // 
  wk()->addOutput (h_PixelSum_2018_excllooseFgap);
  h_PixelSum_2018_excllooseNoFgap = new TH1F("1_PixelSum/PixelSum_2018_excllooseNoFgap", "PixelSum_2018_excllooseNoFgap", 40, 0.,40.); // 
  wk()->addOutput (h_PixelSum_2018_excllooseNoFgap);


  h_FgapA_2018_noiseSup = new TH1F("1_Fgap/FgapA_2018_noiseSup", "FgapA_2018_noiseSup", 80, -10.,10.); // 
  wk()->addOutput (h_FgapA_2018_noiseSup);
  h_FgapA_2018_L1TAU = new TH1F("1_Fgap/FgapA_2018_L1TAU", "FgapA_2018_L1TAU", 80, -10.,10.); // 
  wk()->addOutput (h_FgapA_2018_L1TAU);
  h_FgapC_2018_noiseSup = new TH1F("1_Fgap/FgapC_2018_noiseSup", "FgapC_2018_noiseSup", 80, -10.,10.); // 
  wk()->addOutput (h_FgapC_2018_noiseSup);
  h_FgapC_2018_L1TAU = new TH1F("1_Fgap/FgapC_2018_L1TAU", "FgapC_2018_L1TAU", 80, -10.,10.); // 
  wk()->addOutput (h_FgapC_2018_L1TAU);  

  h_FgapA_2018_excllooseNoFgap = new TH1F("1_Fgap/FgapA_2018_excllooseNoFgap", "FgapA_2018_excllooseNoFgap", 80, -10.,10.); // 
  wk()->addOutput (h_FgapA_2018_excllooseNoFgap);
  h_FgapC_2018_excllooseNoFgap = new TH1F("1_Fgap/FgapC_2018_excllooseNoFgap", "FgapC_2018_excllooseNoFgap", 80, -10.,10.); // 
  wk()->addOutput (h_FgapC_2018_excllooseNoFgap);
  
  
  /////////////////////
  
  LbLSel_CtrlNoAcoNoPixTrk_nMuons = new TH1F("2_LbLSel_CtrlNoAcoNoPixTrk/nMuonsMSTP", "nMuonsMSTP", 5, -0.5,4.5); // 
  wk()->addOutput (LbLSel_CtrlNoAcoNoPixTrk_nMuons);
  electron_studies_2018_L1TAU_nMuons = new TH1F("1_electron_studies_2018_L1TAU/nMuonsMSTP", "nMuonsMSTP", 5, -0.5,4.5); // 
  wk()->addOutput (electron_studies_2018_L1TAU_nMuons);  
  LbLSel_CtrlNoAcoPt_nMuons = new TH1F("2_LbLSel_CtrlNoAcoPt/nMuonsMSTP", "nMuonsMSTP", 5, -0.5,4.5); // 
  wk()->addOutput (LbLSel_CtrlNoAcoPt_nMuons);  
  LbLSel_1MCut_EMPTY_nMuons = new TH1F("2_LbLSel_1MCut_EMPTY/nMuonsMSTP", "nMuonsMSTP", 5, -0.5,4.5); // 
  wk()->addOutput (LbLSel_1MCut_EMPTY_nMuons); 
    LbLSel_1MCut_EMPTY_time = new TH1F("2_LbLSel_1MCut_EMPTY/time", "time", 50, -50.,50.); // 
  wk()->addOutput (LbLSel_1MCut_EMPTY_time); 

  LbLSel_CtrlNoAcoNoPixTrk_nMuons2 = new TH1F("2_LbLSel_CtrlNoAcoNoPixTrk/nMuonsFull", "nMuonsFull", 5, -0.5,4.5); // 
  wk()->addOutput (LbLSel_CtrlNoAcoNoPixTrk_nMuons2);
  electron_studies_2018_L1TAU_nMuons2 = new TH1F("1_electron_studies_2018_L1TAU/nMuonsFull", "nMuonsFull", 5, -0.5,4.5); // 
  wk()->addOutput (electron_studies_2018_L1TAU_nMuons2);
  
  LbLSel_2_LbLSel_3PtCutInverted_nMuons = new TH1F("2_LbLSel_3PtCutInverted/nMuonsMSTP", "nMuonsMSTP", 5, -0.5,4.5); // 
  wk()->addOutput (LbLSel_2_LbLSel_3PtCutInverted_nMuons);
  LbLSel_2_LbLSel_3PtCutInverted_2ptgg3_nMuons = new TH1F("2_LbLSel_3PtCutInverted/nMuonsMSTP_2ptgg3", "nMuonsMSTP", 5, -0.5,4.5); // 
  wk()->addOutput (LbLSel_2_LbLSel_3PtCutInverted_2ptgg3_nMuons);  
  LbLSel_2_LbLSel_3PtCutInverted_3ptgg4_nMuons = new TH1F("2_LbLSel_3PtCutInverted/nMuonsMSTP_3ptgg4", "nMuonsMSTP", 5, -0.5,4.5); // 
  wk()->addOutput (LbLSel_2_LbLSel_3PtCutInverted_3ptgg4_nMuons);   
  LbLSel_2_LbLSel_3PtCutInverted_4ptgg5_nMuons = new TH1F("2_LbLSel_3PtCutInverted/nMuonsMSTP_4ptgg5", "nMuonsMSTP", 5, -0.5,4.5); // 
  wk()->addOutput (LbLSel_2_LbLSel_3PtCutInverted_4ptgg5_nMuons); 
  LbLSel_2_LbLSel_3PtCutInverted_5ptgg7_nMuons = new TH1F("2_LbLSel_3PtCutInverted/nMuonsMSTP_5ptgg7", "nMuonsMSTP", 5, -0.5,4.5); // 
  wk()->addOutput (LbLSel_2_LbLSel_3PtCutInverted_5ptgg7_nMuons); 

    LbLSel_1MCut_nMuons = new TH1F("2_LbLSel_1MCut/nMuonsMSTP", "nMuonsMSTP", 5, -0.5,4.5);  		wk()->addOutput (LbLSel_1MCut_nMuons); 
    LbLSel_2NTrkVeto_nMuons = new TH1F("2_LbLSel_2NTrkVeto/nMuonsMSTP", "nMuonsMSTP", 5, -0.5,4.5);  	wk()->addOutput (LbLSel_2NTrkVeto_nMuons);
    LbLSel_3PtCut2GeV_nMuons = new TH1F("2_LbLSel_3PtCut2GeV/nMuonsMSTP", "nMuonsMSTP", 5, -0.5,4.5);  	wk()->addOutput (LbLSel_3PtCut2GeV_nMuons);
    LbLSel_4PixTrkVeto_nMuons = new TH1F("2_LbLSel_4PixTrkVeto/nMuonsMSTP", "nMuonsMSTP", 5, -0.5,4.5); wk()->addOutput (LbLSel_4PixTrkVeto_nMuons);
    LbLSel_6AcoCut_nMuons = new TH1F("2_LbLSel_6AcoCut/nMuonsMSTP", "nMuonsMSTP", 5, -0.5,4.5);  	wk()->addOutput (LbLSel_6AcoCut_nMuons);
      
  //////////////////// ZDC sums
  
  LbLSel_5MuonVeto_ZDCsumsA = new TH1F("2_LbLSel_5MuonVeto/ZDCsumsA", "ZDCsumsA", 200, 0.,10000.); // 
  wk()->addOutput (LbLSel_5MuonVeto_ZDCsumsA);
  LbLSel_5MuonVeto_ZDCsumsC = new TH1F("2_LbLSel_5MuonVeto/ZDCsumsC", "ZDCsumsC", 200, 0.,10000.); // 
  wk()->addOutput (LbLSel_5MuonVeto_ZDCsumsC);
  LbLSel_5MuonVeto_ZDCsumsAC = new TH1F("2_LbLSel_5MuonVeto/ZDCsumsAC", "ZDCsumsAC", 200, 0.,10000.); // 
  wk()->addOutput (LbLSel_5MuonVeto_ZDCsumsAC);
  electron_studies_2018_L1TAU_ZDCsumsA = new TH1F("1_electron_studies_2018_L1TAU/ZDCsumsA", "eeZDCsumsA", 200, 0.,10000.); // 
  wk()->addOutput (electron_studies_2018_L1TAU_ZDCsumsA);
  electron_studies_2018_L1TAU_ZDCsumsC = new TH1F("1_electron_studies_2018_L1TAU/ZDCsumsC", "eeZDCsumsC", 200, 0.,10000.); // 
  wk()->addOutput (electron_studies_2018_L1TAU_ZDCsumsC);
  
  //////////////////
  
  //histograms for ABCD method
  h_fakeABCD_B = new TH1F("ABCD/B_aco", "B_aco", 200, 0.,1.); // 
  h_fakeABCD_C = new TH1F("ABCD/C_aco", "C_aco", 200, 0.,1.); // 
  h_fakeABCD_D = new TH1F("ABCD/D_aco", "D_aco", 200, 0.,1.); // 
  wk()->addOutput (h_fakeABCD_B);
  wk()->addOutput (h_fakeABCD_C);
  wk()->addOutput (h_fakeABCD_D);
  
  ////////////////////
  

  h_NTrkRecoTruth = new TH2F("Truth/NTrkRecoPrimTruth2D", "recoPrimTruth", 150, -0.5,149.5, 150, -0.5,149.5); // 
  wk()->addOutput (h_NTrkRecoTruth);

  h_NTrkTruthAll = new TH1F("Truth/NTrkTruthAll", "recoTruthAll", 150, -0.5,149.5); // 
  wk()->addOutput (h_NTrkTruthAll);  

  h_NTrkTruthVtxMatched = new TH1F("Truth/NTrkTruthVtxMatched", "recoTruthVtxMatched", 150, -0.5,149.5); // 
  wk()->addOutput (h_NTrkTruthVtxMatched); 
  

  h_DEt34 = new TH1F("Truth/PhotonDEt34", "PhotonDEt34", 100, -0.5, 0.5); // 
  wk()->addOutput (h_DEt34); 
  h_DEt45 = new TH1F("Truth/PhotonDEt45", "PhotonDEt45", 100, -0.5, 0.5); // 
  wk()->addOutput (h_DEt45);
  h_DEt57 = new TH1F("Truth/PhotonDEt57", "PhotonDEt57", 100, -0.5, 0.5); // 
  wk()->addOutput (h_DEt57);
  h_DEt710 = new TH1F("Truth/PhotonDEt710", "PhotonDEt710", 100, -0.5, 0.5); // 
  wk()->addOutput (h_DEt710);
  h_DEt1015 = new TH1F("Truth/PhotonDEt1015", "PhotonDEt1015", 100, -0.5, 0.5); // 
  wk()->addOutput (h_DEt1015);
    

  h_ElePtTag = new TH1F("EleTandP/ElePtTag", "ElePtTag", 100, 0.,50.); // 
  wk()->addOutput (h_ElePtTag);  
  h_ElePtProbe = new TH1F("EleTandP/ElePtProbe", "ElePtProbe", 100, 0.,50.); // 
  wk()->addOutput (h_ElePtProbe); 

  h_EleEtaTag = new TH1F("EleTandP/EleEtaTag", "EleEtaTag", 60, -3.,3.); // 
  wk()->addOutput (h_EleEtaTag);  
  h_EleEtaProbe = new TH1F("EleTandP/EleEtaProbe", "EleEtaProbe", 60, -3.,3.); // 
  wk()->addOutput (h_EleEtaProbe); 

  h_ElePtTagB = new TH1F("EleTandP/ElePtTagB", "ElePtTagB", 100, 0.,50.); // 
  wk()->addOutput (h_ElePtTagB);  
  h_ElePtProbeB = new TH1F("EleTandP/ElePtProbeB", "ElePtProbeB", 100, 0.,50.); // 
  wk()->addOutput (h_ElePtProbeB); 

  h_ElePtTagEC = new TH1F("EleTandP/ElePtTagEC", "ElePtTagEC", 100, 0.,50.); // 
  wk()->addOutput (h_ElePtTagEC);  
  h_ElePtProbeEC = new TH1F("EleTandP/ElePtProbeEC", "ElePtProbeEC", 100, 0.,50.); // 
  wk()->addOutput (h_ElePtProbeEC); 

  h_ElePtTagBEC = new TH1F("EleTandP/ElePtTagBEC", "ElePtTagBEC", 100, 0.,50.); // 
  wk()->addOutput (h_ElePtTagBEC);  
  h_ElePtProbeBEC = new TH1F("EleTandP/ElePtProbeBEC", "ElePtProbeBEC", 100, 0.,50.); // 
  wk()->addOutput (h_ElePtProbeBEC); 

  
  double newBins[] = {0.,2.5,3.,4.,5.,6.,7.,9.,12.,20.};
  h_EleTrkTag = new TH1F("EleTandP/EleTrkTag", "EleTrkTag", 50, 0.,25.); // 
  wk()->addOutput (h_EleTrkTag);  
  h_EleTrkProbe = new TH1F("EleTandP/EleTrkProbe", "EleTrkProbe", 50, 0.,25.); //  
  wk()->addOutput (h_EleTrkProbe);
  h_EleGamPt = new TH1F("EleTandP/EleGamPt", "EleGamPt", 50, 0.,25.); // 
  wk()->addOutput (h_EleGamPt);
  h_EleGamEta = new TH1F("EleTandP/EleGamEta", "EleGamEta", 60, -3.,3.); // 
  wk()->addOutput (h_EleGamEta);   
  h_EleGamDR = new TH1F("EleTandP/EleGamDR", "EleGamDR", 50, 0.,2.); // 
  wk()->addOutput (h_EleGamDR);  
  h_EleGamPt3 = new TH1F("EleTandP/EleGamPt3", "EleGamPt3", 50, 0.,25.); // 
  wk()->addOutput (h_EleGamPt3);
  h_EleGamPt3ID = new TH1F("EleTandP/EleGamPt3ID", "EleGamPt3ID", 50, 0.,25.); // 
  wk()->addOutput (h_EleGamPt3ID);
    h_EleGamEta3ID = new TH1F("EleTandP/EleGamEta3ID", "EleGamEta3ID", 60, -3.,3.); // 
  wk()->addOutput (h_EleGamEta3ID);
      h_EleGamConv3ID = new TH1F("EleTandP/EleGamConv3ID", "EleGamConv3ID", 7, -0.5, 6.5); // 
  wk()->addOutput (h_EleGamConv3ID);
        h_EleGamNtrk3ID = new TH1F("EleTandP/EleGamNtrk3ID", "EleGamNtrk3ID", 5, -0.5, 4.5); // 
  wk()->addOutput (h_EleGamNtrk3ID);
  

  h_EleTrkPt = new TH1F("EleTandP/EleTrkPt", "EleTrkPt", 50, 0.,5.); // 
  wk()->addOutput (h_EleTrkPt);
  h_EleTrkPtP = new TH1F("EleTandP/EleTrkPtP", "EleTrkPtP", 50, 0.,5.); // 
  wk()->addOutput (h_EleTrkPtP);
  h_ElePtllg = new TH1F("EleTandP/ElePtllg", "ElePtllg", 50, 0.,5.); // 
  wk()->addOutput (h_ElePtllg);


  h_eeg_F1   = new TH1F("EleTandP/h_PhotonF1",		"eegPhotonF1",	  	  50,   0., 1.); 
  wk()->addOutput (h_eeg_F1); 
  h_eeg_Reta   = new TH1F("EleTandP/h_PhotonReta",	"eegPhotonReta",	  60,   0., 1.5); 
  wk()->addOutput (h_eeg_Reta); 
  h_eeg_Weta2   = new TH1F("EleTandP/h_PhotonWeta2",	"eegPhotonWeta2",	  60,   0.0, 0.03); 
  wk()->addOutput (h_eeg_Weta2);
  h_eeg_Eratio   = new TH1F("EleTandP/h_PhotonEratio",	"eegPhotonEratio",	  60,   0.0, 1.2); 
  wk()->addOutput (h_eeg_Eratio);
  h_eeg_Rhad  = new TH1F("EleTandP/h_PhotonRhad",	"eegPhotonRhad",	  40,   -0.2, 0.2); 
  wk()->addOutput (h_eeg_Rhad);
  h_eeg_Rhad1  = new TH1F("EleTandP/h_PhotonRhad1",	"eegPhotonRhad1",	  40,   -0.2, 0.2); 
  wk()->addOutput (h_eeg_Rhad1);
  
  h_eeg_Time  = new TH1F("EleTandP/h_PhotonTime",	"eegPhotonTime",	  100,   -50, 50); 
  wk()->addOutput (h_eeg_Time);


  h_Ntrk1 = new TH1F("EGamMisID/Ntrk1", "Ntrk1", 10, -0.5,9.5); // 
  wk()->addOutput (h_Ntrk1);  
  h_Ntrk2 = new TH1F("EGamMisID/Ntrk2", "Ntrk2", 10, -0.5,9.5); // 
  wk()->addOutput (h_Ntrk2); 


  h_EGMisID1 = new TH1F("EGamMisID/EGMisID1", "EGMisID1", 100, 0.,50.); // 
  wk()->addOutput (h_EGMisID1);  
  h_EGMisID2 = new TH1F("EGamMisID/EGMisID2", "EGMisID2", 100, 0.,50.); // 
  wk()->addOutput (h_EGMisID2); 

  h_EGMisID1Iso = new TH1F("EGamMisID/EGMisID1Iso", "EGMisID1Iso", 100, 0.,50.); // 
  wk()->addOutput (h_EGMisID1Iso); 

  h_EtSinglePhoton = new TH1F("EGamMisID/EtSinglePhoton", "EtSinglePhoton", 100, 0.,50.); // 
  wk()->addOutput (h_EtSinglePhoton);


  h_AcoEG = new TH1F("EGamMisID/AcoEG", "AcoEG", 100, 0.,1.); // 
  wk()->addOutput (h_AcoEG);
  h_AcoEG2 = new TH1F("EGamMisID/AcoEG2", "AcoEG2", 100, 0.,1.); // 
  wk()->addOutput (h_AcoEG2);    

  h_PtEG = new TH1F("EGamMisID/PtEG", "PtEG", 50,  0.,  25.); // 
  wk()->addOutput (h_PtEG);
  h_PtEG2 = new TH1F("EGamMisID/PtEG2", "PtEG2", 50,  0.,  25.); // 
  wk()->addOutput (h_PtEG2); 

  h_trkPtEG = new TH1F("EGamMisID/trkPtEG", "trkPtEG", 1000,  0.,  10.); // 
  wk()->addOutput (h_trkPtEG);

  h_Author = new TH1F("EGamMisID/Author", "Author", 3, -0.5,2.5); // 
  wk()->addOutput (h_Author);


  h_4gMass = new TH1F("4Photons/4gMass", "4gMass", 400, 0.,40); // 
  wk()->addOutput (h_4gMass);
  h_2trkMass = new TH1F("4Photons/2trkMass", "2trkMass", 400, 0.,40); // 
  wk()->addOutput (h_2trkMass);  
  
  //////////////////////////////////////////
  h_TruthMass = new TH1F("Truth/Mass", "Mass", 400, 0.,40); // 
  wk()->addOutput (h_TruthMass);   
  h_TruthMass3GeV = new TH1F("Truth/Masspt3GeV", "Masspt3GeV", 400, 0.,40); // 
  wk()->addOutput (h_TruthMass3GeV);
  h_TruthMass35GeV = new TH1F("Truth/Masspt35GeV", "Masspt35GeV", 400, 0.,40); // 
  wk()->addOutput (h_TruthMass35GeV);

  h_TruthPt = new TH1F("Truth/ElePt", "ElePt", 100, 0.,50); // 
  wk()->addOutput (h_TruthPt);
  h_TruthPt1 = new TH1F("Truth/ElePt1", "ElePt1", 100, 0.,50); // 
  wk()->addOutput (h_TruthPt1);
  h_TruthPt3 = new TH1F("Truth/ElePt3", "ElePt3", 100, 0.,50); // 
  wk()->addOutput (h_TruthPt3);

  h_TruthEta = new TH1F("Truth/EleEta", "EleEta", 60, -3.,3.); // 
  wk()->addOutput (h_TruthEta);
  h_TruthEta1 = new TH1F("Truth/EleEta1", "EleEta1", 60, -3.,3.); // 
  wk()->addOutput (h_TruthEta1);


  h_TE5eff1 = new TH1F("TE5eff/eff1", "eff1", 100, 0.,50); // 
  wk()->addOutput (h_TE5eff1);
  h_TE5eff2 = new TH1F("TE5eff/eff2", "eff2", 100, 0.,50); // 
  wk()->addOutput (h_TE5eff2);

  h_zdca = new TH1F("zdc/ene_a", "ene_a", 1000, 0.,50000); // 
  wk()->addOutput (h_zdca);
  h_zdcc = new TH1F("zdc/ene_c", "ene_c", 1000, 0.,50000); // 
  wk()->addOutput (h_zdcc);

  h_npix_hits_1_HLT_hi_gg_upc_L1TE5_VTE200_2Tracks = new TH1F("Pixel/pix_hits_1_HLT_hi_gg_upc_L1TE5_VTE200_2Tracks", "pixhits1", 50, -0.5,49.5); // 
  wk()->addOutput (h_npix_hits_1_HLT_hi_gg_upc_L1TE5_VTE200_2Tracks);
  h_npix_hits_1_HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50_2Tracks_TE5 = new TH1F("Pixel/pix_hits_1_HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50_2Tracks_TE5", "pixhits2", 50, -0.5,49.5); // 
  wk()->addOutput (h_npix_hits_1_HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50_2Tracks_TE5);
  h_npix_hits_2_LbLSel_4NTrkCutEt3GeV = new TH1F("Pixel/pix_hits_2_LbLSel_4NTrkCutEt3GeV", "pixhits3", 50, -0.5,49.5); // 
  wk()->addOutput (h_npix_hits_2_LbLSel_4NTrkCutEt3GeV);
  h_npix_hits_2_LbLSel_CtrlNoAco = new TH1F("Pixel/pix_hits_2_LbLSel_CtrlNoAco", "pixhits4", 50, -0.5,49.5); // 
  wk()->addOutput (h_npix_hits_2_LbLSel_CtrlNoAco);
  h_npix_hits = new TH1F("Pixel/pix_hits", "pixhits", 50, -0.5,49.5); // 
  wk()->addOutput (h_npix_hits);
          
  h_RunDependence = new TH1F("RunDep", "runDependence", 1300, 286700.5, 288000.5); // 
  wk()->addOutput (h_RunDependence);
  
  ///////
  h_em_aco = new TH1F("emu/aco", "aco", 50, 0.,1.); // 
  wk()->addOutput (h_em_aco);
  h_em_ptmu = new TH1F("emu/ptmu", "ptmu", 150, 0.,150.); // 
  wk()->addOutput (h_em_ptmu);
  h_em_pte = new TH1F("emu/pte", "pte", 150, 0.,150.); // 
  wk()->addOutput (h_em_pte);
  h_em_ngam = new TH1F("emu/n_gam", "n_gam", 5, -0.5, 4.5); // 
  wk()->addOutput (h_em_ngam);
  h_em_deta = new TH1F("emu/deta", "deta", 50, 0., 5.); // 
  wk()->addOutput (h_em_deta);

  h_3g_ngam = new TH1F("3g/n_gam", "n_gam", 5, -0.5, 4.5); // 
  wk()->addOutput (h_3g_ngam);
  h_3g_m = new TH1F("3g/m", "m", 100, 0.,100.); // 
  wk()->addOutput (h_3g_m);
  h_3g_pt = new TH1F("3g/pt", "pt", 50, 0.,50.); // 
  wk()->addOutput (h_3g_pt);
  //////
 
 
 
   //----------------------------- Initialise GRL -----------------------------//
   //const char* GRLFilePath = "/sps/atlas/m/mdyndal/UPC_Run2_rel21/source/EXCLRunII/data/GRL/HI_UPC_GRL.xml";   
   const char* GRLFilePath2015 = "/afs/cern.ch/user/a/atlasdqm/grlgen/HeavyIonP/data15_hi.periodAllYear_DetStatus-v75-pro19-09_DQDefects-00-02-02_PHYS_HeavyIonP_All_Good.UPC.xml"; 
   const char* fullGRLFilePath2015 = gSystem->ExpandPathName (GRLFilePath2015);   
   std::string sfullGRLFilePath2015 = fullGRLFilePath2015;
   std::cout<<"Enable 2015 GRL: "+sfullGRLFilePath2015 <<std::endl;  
   
   const char* GRLFilePath2018 = "$UserAnalysis_DIR/XML/EXCLRunII/data18_hi.periodAllYear_DetStatus-v104-pro22-08_Unknown_PHYS_HeavyIonP_All_Good_ignore_TOROIDSTATUS.xml";
   const char* fullGRLFilePath2018 = gSystem->ExpandPathName (GRLFilePath2018);   
   std::string sfullGRLFilePath2018 = fullGRLFilePath2018;
   std::cout<<"Enable 2018 (unofficial) GRL: "+sfullGRLFilePath2018 <<std::endl;

   std::vector<std::string> vecStringGRL;
   vecStringGRL.push_back(fullGRLFilePath2015);
   vecStringGRL.push_back(fullGRLFilePath2018);
   ANA_CHECK(m_grl.setProperty( "GoodRunsListVec", vecStringGRL));
   ANA_CHECK(m_grl.setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
   ANA_CHECK(m_grl.initialize());  
  //-------------------------------------------------------------------------//  
  
  
  //----------------- Initialize and configure Trigger tools ----------------//

   ANA_CHECK( m_trigConfigTool.initialize() );
   ANA_CHECK( m_trigDecisionTool.setProperty( "ConfigTool", m_trigConfigTool.getHandle() ) ); // connect the TrigDecisionTool to the ConfigTool
   ANA_CHECK( m_trigDecisionTool.setProperty( "TrigDecisionKey", "xTrigDecision" ) );
   ANA_CHECK( m_trigDecisionTool.initialize() ); 
   
   
     //---- init electron/photon calibration
   ANA_CHECK(m_electronCalibrationTool.setProperty("ESModel", "es2015_5TeV")); 
//   ANA_CHECK(m_electronCalibrationTool.setProperty("ESModel", "es2017_R21_v1"));
//   ANA_CHECK(m_electronCalibrationTool->setProperty("ResolutionType", "SigmaEff90"));  
   ANA_CHECK(m_electronCalibrationTool.setProperty("decorrelationModel", "1NP_v1"));
   ANA_CHECK(m_electronCalibrationTool.setProperty("randomRunNumber", "123456"));
   ANA_CHECK(m_electronCalibrationTool.initialize());
  
  // showershape fudge tool
   ANA_CHECK(m_fudgeMCTool.setProperty("Preselection", 22)); 
   ANA_CHECK(m_fudgeMCTool.setProperty("FFCalibFile","ElectronPhotonShowerShapeFudgeTool/v2/PhotonFudgeFactors.root")); 
   ANA_CHECK(m_fudgeMCTool.initialize());
   
   
   // eGamma systematics
// 	EG_RESOLUTION_ALL__1down
//	EG_RESOLUTION_ALL__1up
//	EG_SCALE_ALL__1down
//	EG_SCALE_ALL__1up
   
   


      
  //--------------------- Loop over systematics registry --------------------//
  const CP::SystematicRegistry& registry = CP::SystematicRegistry::getInstance();
  const CP::SystematicSet& recommendedSystematics = registry.recommendedSystematics(); // get list of recommended systematics

  // this is the nominal set, no systematic
  m_sysList.push_back(CP::SystematicSet());  
  
  
  const xAOD::EventInfo* m_eventInfo2 = 0;
  ANA_CHECK(evtStore()->retrieve( m_eventInfo2, "EventInfo"));
  bool is_simu = m_eventInfo2->eventType( xAOD::EventInfo::IS_SIMULATION );
  int MC_id2 = 0;
  if(is_simu) MC_id2 = m_eventInfo2-> mcChannelNumber();

  //-------------------------------------------------------------------------//  
  if( is_simu && (MC_id2 !=420252) && (MC_id2 !=420253) ) { // should be done only for  MC...
  
    // loop over recommended systematics
    for(CP::SystematicSet::const_iterator sysItr = recommendedSystematics.begin(); sysItr != recommendedSystematics.end(); ++sysItr){
      m_sysList.push_back( CP::SystematicSet() );
      m_sysList.back().insert( *sysItr );
        std::cout << *sysItr  <<std::endl;
    }  
    
  m_sysList.push_back(CP::SystematicSet("TRIG_1up"));
  m_sysList.push_back(CP::SystematicSet("TRIG_1down"));

  //----------------- Book histograms for systematic shifts -----------------//
  std::vector<CP::SystematicSet>::const_iterator sysListItr;  
  for (sysListItr = m_sysList.begin(); sysListItr != m_sysList.end(); ++sysListItr){  
    if ((*sysListItr).name()!=""){
    	 this->bookHistos("1_electron_studies_2018_AcoCutTightTrk"+(*sysListItr).name());
         this->bookHistos("1_electron_studies_2018_L1TAU"+(*sysListItr).name());
	 
	 this->bookHistos("2_LbLSel_1MCut"+(*sysListItr).name());
	 this->bookHistos("2_LbLSel_Ctrl2Trk"+(*sysListItr).name());
	 
    	 this->bookHistos("2_LbLSel_6AcoCut"+(*sysListItr).name());
	 this->bookHistos("2_LbLSel_3GeV_6AcoCut"+(*sysListItr).name());
	 
	 this->bookHistos("2_LbLSel_CtrlNoAcoNoPixTrk"+(*sysListItr).name());
	 this->bookHistos("2_LbLSel_3GeV_CtrlNoAcoNoPixTrk"+(*sysListItr).name());
	 }
    
  }
  
    /*  	 this->bookHistos("2_LbLSel_6AcoCutTRIG_1up");
	 this->bookHistos("2_LbLSel_3GeV_6AcoCutTRIG_1up");	 
	 this->bookHistos("2_LbLSel_CtrlNoAcoNoPixTrkTRIG_1up");
	 this->bookHistos("2_LbLSel_3GeV_CtrlNoAcoNoPixTrkTRIG_1up"); 
	       	 this->bookHistos("2_LbLSel_6AcoCutTRIG_1down");
	 this->bookHistos("2_LbLSel_3GeV_6AcoCutTRIG_1down");	 
	 this->bookHistos("2_LbLSel_CtrlNoAcoNoPixTrkTRIG_1down");
	 this->bookHistos("2_LbLSel_3GeV_CtrlNoAcoNoPixTrkTRIG_1down"); 
	 */
  //-------------------------------------------------------------------------// 

  }

  return StatusCode::SUCCESS;
}



/*****************************************************************/
StatusCode EXCLRecoAnalysis :: execute (){
/*****************************************************************/
//---- execute function, done for each event in the loop




  ///*********************** LOOP over recommended SYSTEMATICS ***********************///
  
  std::vector<CP::SystematicSet>::const_iterator sysListItr;  

  for (sysListItr = m_sysList.begin(); sysListItr != m_sysList.end(); ++sysListItr) {  //only central now
  
  
    /////******************* Configure recommended systematics ******************///// 

    //if((*sysListItr).name() != "")  
    configureToolsForSystematics((*sysListItr));

    
    ////////////////////////////////////////////////////////
    /////		    EVENT SELECTION	       ///// 
    ////////////////////////////////////////////////////////
    
    fillEvent();
    
    //--- GRL + data quality requirement  
    if (!isGoodDataEvent()) return StatusCode::SUCCESS;
    
    // 2015 trigger
    if(eventFiredHLT_hi_gg_upc_L1TE5_VTE200()) m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 = true; 
    
    //2018 triggers
    if(eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_noiseSup_L1TEX_VTEY()) m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_noiseSup_L1TEX_VTEY = true; 
    if(eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ()) m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ = true; 
    if(eventFired_2018_exclusivelooseFgapTriggers()) m_eventFired_2018_exclusivelooseFgapTriggers = true; 
    if(eventFired_2018_exclusivelooseNoFgapTriggers()) m_eventFired_2018_exclusivelooseNoFgapTriggers = true; 
    if(eventFired_2018_EMPTY()) m_eventFired_2018_EMPTY = true;
    
    //if(eventFiredHLT_mb_sptrk_ion_L1ZDC_A_C_VTE50()) m_eventFiredHLT_mb_sptrk_ion_L1ZDC_A_C_VTE50 = true; 
    //if(eventFiredHLT_mb_sptrk_vetombts2in_L1ZDC_A_C_VTE50()) m_eventFiredHLT_mb_sptrk_vetombts2in_L1ZDC_A_C_VTE50 = true; 
    //if(eventFiredHLT_hi_loose_upc_L1ZDC_A_C_VTE50()) m_eventFiredHLT_hi_loose_upc_L1ZDC_A_C_VTE50 = true; 
    //if(eventFiredHLT_mb_sptrk_vetombts2in_L1MU0_VTE50()) m_eventFiredHLT_mb_sptrk_vetombts2in_L1MU0_VTE50 = true; 
    //if(eventFiredHLT_TE5()) m_eventFiredHLT_TE5 = true; 
    


    if ( 1 ) {
		

    	   ExclVetoCandidate *V = new ExclVetoCandidate(m_goodTracks, m_goodPixTracks, m_goodVertices, pv );	
			          
	   PairCandidate *Z =  findPairCandidates(m_goodElectrons, m_goodMuons, m_goodPhotons, m_goodTracks);
	   EXCLCandidate *E = new EXCLCandidate(1, Z, V, m_sum_pix);
	   
	   //update trig weight
	   if(  (*sysListItr).name()=="TRIG_1down" ){
	      float old_weight = (0.5*(TMath::Erf((sum_EtTot - 4.56279)/2.83293) + 1.));
	      float new_weight = (0.5*(TMath::Erf((sum_EtTot - 4.60062)/3.07603) + 1.)) ;
	      weight = weight / old_weight * new_weight;
	    //  std::cout<< "TRIG_1up: old trig weight: "<<old_weight<<"  new: "<<new_weight<<std::endl;	      
	   }
	   if(  (*sysListItr).name()=="TRIG_1up" ){
	      float old_weight = (0.5*(TMath::Erf((sum_EtTot - 4.56279)/2.83293) + 1.));
	      float new_weight = (0.5*(TMath::Erf((sum_EtTot - 3.93649)/3.33947) + 1.)) ;
	      weight = weight / old_weight * new_weight;
	    //  std::cout<< "TRIG_1down: old trig weight: "<<old_weight<<"  new: "<<new_weight<<std::endl;	      
	   }
	   


	   
	   // check the (cosmic) muon multiplicity
  	   const xAOD::MuonContainer* muons = 0;
           ANA_CHECK (evtStore()->retrieve (muons, "Muons"));
	   int nMuons2 = muons->size();

  	   const xAOD::TrackParticleContainer* mstp = 0;
           ANA_CHECK (evtStore()->retrieve (mstp, "MuonSpectrometerTrackParticles"));
	   int nMuons = mstp->size();

	   //ZDC    
	   float zdcSumA =0.;
	   float zdcSumC =0.;
/*	   const xAOD::ZdcModuleContainer* zdcMod = 0;
           ANA_CHECK (evtStore()->retrieve (zdcMod, "ZdcModules"));
	   for(uint i = 0; i < zdcMod->size(); i++) {
	     //FitAmp, CalibEnergy, amplitude
	     if(zdcMod->at(i)->side()==-1) zdcSumA += zdcMod->at(i)->amplitudeG1();
	     if(zdcMod->at(i)->side()==1)  zdcSumC += zdcMod->at(i)->amplitude();
	   }
	   
*/	       
	   // cluster selection check   
	   if ( 0 // disabled now
	       && m_goodClusters.size() == 2
	       && m_goodTracks.size() == 2 
	       && (*sysListItr).name()==""
	       ){ 
	       
	       float aco = 1. - acos(cos(m_goodTracks.at(0)->phi()-m_goodTracks.at(1)->phi()))/TMath::Pi();
	         if(aco<0.2 ){
		 
		 // Pixel cluster sum
		 int sum_pix=0;	
		 std::string METTag="HLT_xAOD__TrigSpacePointCountsContainer_spacepoints";	
		 const xAOD::TrigSpacePointCountsContainer* SpacePointCountsCont=0;
     	 	 if(evtStore()->retrieve( SpacePointCountsCont, METTag ).isSuccess()){

		   for(uint i = 0; i < SpacePointCountsCont->size(); i++) {	
		     sum_pix += SpacePointCountsCont->at(i)->pixelClusBarrelSumEntries(20., 0., xAOD::TrigHistoCutType::ABOVE_X_ABOVE_Y);
		     sum_pix += SpacePointCountsCont->at(i)->pixelClusEndcapASumEntries(20., 0., xAOD::TrigHistoCutType::ABOVE_X_ABOVE_Y);
		     sum_pix += SpacePointCountsCont->at(i)->pixelClusEndcapCSumEntries(20., 0., xAOD::TrigHistoCutType::ABOVE_X_ABOVE_Y);
		   }
    		 }
		 
		 
		 //FCal sum Et
		 double FCalA(0);
		 double FCalC(0);
  		 const xAOD::HIEventShapeContainer *hiue(0);
    		 if ( evtStore()->retrieve(hiue, "HIEventShape").isSuccess() ) {
		   
		   for ( size_t slice = 0; slice < hiue->size(); ++slice ) {      
     	 	     const double et = hiue->at(slice)->et()/1e3;
      		     const double etaMin = hiue->at(slice)->etaMin();
		     const double etaMax = hiue->at(slice)->etaMax();
     		     if ( etaMin >= 3.2 && etaMax <= 5. ) FCalA += et;
		     if ( etaMax <= -3.2 && etaMin >= -5. ) FCalC += et;
   		   }

   		 }
		 
		 	       	         	       		 
	         if (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200){
		    h_Clusters_SumEt_2015->Fill(m_goodClusters.at(0)->pt()/1e3 + m_goodClusters.at(1)->pt()/1e3);
		    }
		 
		 if (m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_noiseSup_L1TEX_VTEY){ 
		    h_Clusters_SumEt_2018_noiseSup->Fill(m_goodClusters.at(0)->pt()/1e3 + m_goodClusters.at(1)->pt()/1e3);
		    h_PixelSum_2018_noiseSup->Fill(sum_pix);
		    h_FgapA_2018_noiseSup->Fill(FCalA);
		    h_FgapC_2018_noiseSup->Fill(FCalC);
		    }
		 if (m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ){
		    h_Clusters_SumEt_2018_L1TAU->Fill(m_goodClusters.at(0)->pt()/1e3 + m_goodClusters.at(1)->pt()/1e3);
		    //h_PixelSum_2018_L1TAU->Fill(sum_pix);
		    h_FgapA_2018_L1TAU->Fill(FCalA);
		    h_FgapC_2018_L1TAU->Fill(FCalC);	    		    
		    }
		 
		 if (m_eventFired_2018_exclusivelooseNoFgapTriggers){
		    h_Clusters_SumEt_2018_excllooseNoFgap->Fill(m_goodClusters.at(0)->pt()/1e3 + m_goodClusters.at(1)->pt()/1e3);
		    h_FgapA_2018_excllooseNoFgap->Fill(FCalA);
		    h_FgapC_2018_excllooseNoFgap->Fill(FCalC);
		    h_PixelSum_2018_excllooseNoFgap->Fill(sum_pix);
		    }
		 if (m_eventFired_2018_exclusivelooseFgapTriggers) {
		    h_Clusters_SumEt_2018_excllooseFgap->Fill(m_goodClusters.at(0)->pt()/1e3 + m_goodClusters.at(1)->pt()/1e3);
		    h_PixelSum_2018_excllooseFgap->Fill(sum_pix);
		    
		        if(!m_isMC){
    
    			const Trig::ChainGroup* cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_mb_sptrk_exclusiveloose2_L1ZDC_XOR_VTE50");
    			if( cg->isPassed() ) h_Clusters_SumEt_2018_excllooseFgap_ZDC_XOR->Fill(m_goodClusters.at(0)->pt()/1e3 + m_goodClusters.at(1)->pt()/1e3);
    
        		cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_mb_sptrk_exclusiveloose2_L1ZDC_A_C_VTE50");
    			if( cg->isPassed() ) h_Clusters_SumEt_2018_excllooseFgap_ZDC_AND->Fill(m_goodClusters.at(0)->pt()/1e3 + m_goodClusters.at(1)->pt()/1e3);
    
        		cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_mb_sptrk_exclusiveloose2_L1VZDC_A_C_VTE50");
    			if( cg->isPassed() )  h_Clusters_SumEt_2018_excllooseFgap_VZDC->Fill(m_goodClusters.at(0)->pt()/1e3 + m_goodClusters.at(1)->pt()/1e3);
		    
		    	}		   
		    
		    }
		    
		 if (m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_noiseSup_L1TEX_VTEY && m_eventFired_2018_exclusivelooseNoFgapTriggers) 
		    h_Clusters_SumEt_2018_noiseSup_AND_excllooseNoFgap->Fill(m_goodClusters.at(0)->pt()/1e3 + m_goodClusters.at(1)->pt()/1e3);
		    
		 if (/*m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_noiseSup_L1TEX_VTEY &&*/ !m_isMC && m_eventFired_2018_exclusivelooseFgapTriggers){
		    		 
		    /*const xAOD::EnergySumRoI* esumroi(0);
		    ANA_CHECK (evtStore()->retrieve (esumroi, "LVL1EnergySumRoI"));  
   		    //std::cout<<"ESumROI : "<<esumroi->energyT()/1000.<<"\n";   
   		    if(esumroi->energyT()/1000. >= 4.) 
		    */
		    auto chainGroup = m_trigDecisionTool->getChainGroup("L1_TAU1_TE4_VTE200");
                    auto ibits = chainGroup->isPassedBits();
		    if ((ibits & TrigDefs::L1_isPassedAfterVeto)) h_Clusters_SumEt_2018_noiseSup_AND_excllooseFgap->Fill(m_goodClusters.at(0)->pt()/1e3 + m_goodClusters.at(1)->pt()/1e3);
		    
		    }
		    
		 if (m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ && m_eventFired_2018_exclusivelooseNoFgapTriggers) 
		    h_Clusters_SumEt_2018_L1TAU_AND_excllooseNoFgap->Fill(m_goodClusters.at(0)->pt()/1e3 + m_goodClusters.at(1)->pt()/1e3);
		    
		 if (/*m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ &&*/ !m_isMC && m_eventFired_2018_exclusivelooseFgapTriggers){
		    
		    /*const xAOD::EmTauRoIContainer* l1taus = 0;
		    ATH_CHECK(evtStore()->retrieve(l1taus, "LVL1EmTauRoIs"));
		    int nTau =0;
		    for(unsigned int itau=0; itau < l1taus->size(); itau++){
		      if(l1taus->at(itau)->eT()/1e3 >= 1.) nTau++;
		    }
    	            if (nTau>=2) 
		    */		    
		    auto chainGroup = m_trigDecisionTool->getChainGroup("L1_2TAU1_VTE50");
                    auto ibits = chainGroup->isPassedBits();
		    if ((ibits & TrigDefs::L1_isPassedAfterVeto)) h_Clusters_SumEt_2018_L1TAU_AND_excllooseFgap->Fill(m_goodClusters.at(0)->pt()/1e3 + m_goodClusters.at(1)->pt()/1e3); 
		    
		    auto chainGroup2 = m_trigDecisionTool->getChainGroup("L1_TAU1_TE4_VTE200");
                    auto ibits2 = chainGroup2->isPassedBits();	    
		    if ((ibits & TrigDefs::L1_isPassedAfterVeto) || (ibits2 & TrigDefs::L1_isPassedAfterVeto)) h_Clusters_SumEt_2018_L1TAU_OR_TE4_AND_excllooseFgap->Fill(m_goodClusters.at(0)->pt()/1e3 + m_goodClusters.at(1)->pt()/1e3); 
		    }
		    	   
	         }
	       }

///////////////////////// analysis blinding
           if ( !m_isMC
	       && Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && m_goodTracks.size() == 0
	       && m_goodPixTracks.size() == 0
	       && nMuons == 0
	       && ( (Z->fourMomentum().Pt()/1e3 < 1. && Z->fourMomentum().M()/1e3 <= 12.) || (Z->fourMomentum().Pt()/1e3 < 2. && Z->fourMomentum().M()/1e3 > 12.) )
	       //&& (*sysListItr).name()=="" 
	       ){
	         float aco = 1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi();
	         if (aco<0.01)  return StatusCode::SUCCESS; 	       
	       }
/////////////////////////////////




           if ( Z->type() == 11	       
	       && m_goodElectrons.size() == 2
	       && m_goodTracks.size() == 2
	       && m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 
	       && (*sysListItr).name()==""
	       ){ 	         	       		 
	         this->fillHistos(E, m_eventInfo, weight, "1_electron_studies_2015"); 
	       }

           if ( Z->type() == 11	       
	       && m_goodElectrons.size() == 2
	       && m_goodTracksTight.size() == 2
	       && m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ 
	       //&& (*sysListItr).name()==""
	       ){ 	
	         
		 float aco = 1. - acos(cos(Z->electron1()->phi()-Z->electron2()->phi()))/TMath::Pi();
	         if(aco<0.01 )  this->fillHistos(E, m_eventInfo, weight, "1_electron_studies_2018_AcoCutTightTrk"+(*sysListItr).name()); 
		 
		 // Pixel cluster sum
/*		 int sum_pix=0;	
		 std::string METTag="HLT_xAOD__TrigSpacePointCountsContainer_spacepoints";	
		 const xAOD::TrigSpacePointCountsContainer* SpacePointCountsCont=0;
     	 	 if(evtStore()->retrieve( SpacePointCountsCont, METTag ).isSuccess()){

		   for(uint i = 0; i < SpacePointCountsCont->size(); i++) {	
		     sum_pix += SpacePointCountsCont->at(i)->pixelClusBarrelSumEntries(20., 0., xAOD::TrigHistoCutType::ABOVE_X_ABOVE_Y);
		     sum_pix += SpacePointCountsCont->at(i)->pixelClusEndcapASumEntries(20., 0., xAOD::TrigHistoCutType::ABOVE_X_ABOVE_Y);
		     sum_pix += SpacePointCountsCont->at(i)->pixelClusEndcapCSumEntries(20., 0., xAOD::TrigHistoCutType::ABOVE_X_ABOVE_Y);
		   }
    		 }
		 
		 h_PixelSum_2018_L1TAU->Fill(sum_pix);*/
	       }

           if ( Z->type() == 11	       
	       && m_goodElectrons.size() == 2
	       && m_goodTracks.size() == 2
	       && m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ 
	       //&& (*sysListItr).name()==""
	       ){ 	         	       		 
	         this->fillHistos(E, m_eventInfo, weight, "1_electron_studies_2018_L1TAU"+(*sysListItr).name()); 
		 electron_studies_2018_L1TAU_nMuons->Fill(nMuons);
		 electron_studies_2018_L1TAU_nMuons2->Fill(nMuons2);
		 
/*		  if(!m_isMC){
	           std::cout<<"ee candidate: "<<m_eventInfo->eventNumber()
	           <<" run:"<<m_eventInfo->runNumber()
		   <<" lbn:"<<m_eventInfo->lumiBlock()
	           <<" mass:"<<Z->fourMomentum().M()/1e3
	           <<" pt:"<<Z->fourMomentum().Pt()/1e3
		   <<" pte1:"<<m_goodElectrons.at(0)->pt()/1e3
		   <<" pte2:"<<m_goodElectrons.at(1)->pt()/1e3
		   <<std::endl;	 
		   
		   std::cout<<"ZDC sum A, C: "<<zdcSumA<<"   "<<zdcSumC<<std::endl;	
		   electron_studies_2018_L1TAU_ZDCsumsA->Fill(zdcSumA);    
		   electron_studies_2018_L1TAU_ZDCsumsC->Fill(zdcSumC); 
		   
	           }
*/		   
	       }



           if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       //&& (*sysListItr).name()=="" 
	       ){
	         this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_1MCut"+(*sysListItr).name()); 
		 LbLSel_1MCut_nMuons->Fill(nMuons);
	       }

          if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && m_goodTracks.size() == 0
	       && (*sysListItr).name()=="" 
	       ){  
	          this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_2NTrkVeto"); 
		  LbLSel_2NTrkVeto_nMuons->Fill(nMuons);		 
		}


           if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && m_goodTracks.size() == 0
	       && ( (Z->fourMomentum().Pt()/1e3 < 1. && Z->fourMomentum().M()/1e3 <= 12.) || (Z->fourMomentum().Pt()/1e3 < 2. && Z->fourMomentum().M()/1e3 > 12.) )
	       && (*sysListItr).name()=="" 
	       ){
	         this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_3PtCut2GeV"); 
		 LbLSel_3PtCut2GeV_nMuons->Fill(nMuons);	       
	       }

           if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && m_goodTracks.size() == 0
	       && Z->fourMomentum().Pt()/1e3 > 2.
	       && (*sysListItr).name()=="" 
	       ){
	         this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_3PtCutInverted"); 	 
		 LbLSel_2_LbLSel_3PtCutInverted_nMuons->Fill(nMuons);
		 if( Z->fourMomentum().Pt()/1e3 > 2. && Z->fourMomentum().Pt()/1e3 < 3.) LbLSel_2_LbLSel_3PtCutInverted_2ptgg3_nMuons->Fill(nMuons);
		 else if( Z->fourMomentum().Pt()/1e3 > 3. && Z->fourMomentum().Pt()/1e3 < 4.) LbLSel_2_LbLSel_3PtCutInverted_3ptgg4_nMuons->Fill(nMuons);
		 else if( Z->fourMomentum().Pt()/1e3 > 4. && Z->fourMomentum().Pt()/1e3 < 5.) LbLSel_2_LbLSel_3PtCutInverted_4ptgg5_nMuons->Fill(nMuons);
		 else if( Z->fourMomentum().Pt()/1e3 > 5. && Z->fourMomentum().Pt()/1e3 < 7.) LbLSel_2_LbLSel_3PtCutInverted_5ptgg7_nMuons->Fill(nMuons);
	       }


 /*          if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && m_goodTracks.size() == 0
	       && Z->fourMomentum().Pt()/1e3 > 2.
	       && nMuons == 0
	       && m_goodPixTracks.size() == 0
	       && (*sysListItr).name()=="" 
	       ){
	         this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_3PtCutInvertedNoMSTP"); 	       
	       }
*/
/*           if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && m_goodTracks.size() == 0
	       && Z->fourMomentum().Pt()/1e3 > 2.
	       && nMuons == 0
	       && m_goodPixTracks.size() == 0
	       && (fabs(m_goodPhotons.at(0)->caloCluster()->time())<10. && fabs(m_goodPhotons.at(1)->caloCluster()->time())<10.)
	       && (*sysListItr).name()=="" 
	       ){
	         this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_3PtCutInvertedNoMSTPtimeCut"); 	       
	       }
*/
           if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && m_goodTracks.size() == 0
	       && ( (Z->fourMomentum().Pt()/1e3 < 1. && Z->fourMomentum().M()/1e3 <= 12.) || (Z->fourMomentum().Pt()/1e3 < 2. && Z->fourMomentum().M()/1e3 > 12.) )
	       && m_goodPixTracks.size() == 0
	       && (*sysListItr).name()=="" 
	       ){
	         this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_4PixTrkVeto"); 
		 LbLSel_4PixTrkVeto_nMuons->Fill(nMuons);	       
	       }

           if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && m_goodTracks.size() == 0
	       && ( (Z->fourMomentum().Pt()/1e3 < 1. && Z->fourMomentum().M()/1e3 <= 12.) || (Z->fourMomentum().Pt()/1e3 < 2. && Z->fourMomentum().M()/1e3 > 12.) )
	       && m_goodPixTracks.size() == 0
	       && m_sum_pix >= 5
	       && (*sysListItr).name()=="" 
	       ){
	           
		   this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_4PixTrkSumPix5"); 	
		   if( (Z->photon1()->pt()/1e3) > 3.0 && (Z->photon2()->pt()/1e3) > 3.0 )  this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_3GeV_4PixTrkSumPix5");       
	       }

//           if ( Z->type() == 22
//	       && m_goodPhotons.size() == 2
//	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
//	       && (Z->photon1()->pt()/1e3) > 2.5 
//	       && (Z->photon2()->pt()/1e3) > 2.5
//	       && Z->fourMomentum().M()/1e3 > 5.
//	       && m_goodTracks.size() == 0
//	       && ( (Z->fourMomentum().Pt()/1e3 < 1. && Z->fourMomentum().M()/1e3 <= 12.) || (Z->fourMomentum().Pt()/1e3 < 2. && Z->fourMomentum().M()/1e3 > 12.) )
//	       && m_goodPixTracks.size() == 0
//	       && nMuons == 0
//	       && (*sysListItr).name()=="" 
//	       ){
//	         this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_5MuonVeto"); 	
		 /*if(!m_isMC){
		 auto chainGroup = m_trigDecisionTool->getChainGroup("L1_ZDC_A");
                 auto ibits = chainGroup->isPassedBits();
		 auto chainGroup2 = m_trigDecisionTool->getChainGroup("L1_ZDC_C");
                 auto ibits2 = chainGroup2->isPassedBits();
		 if      ((ibits & TrigDefs::L1_isPassedAfterVeto))  LbLSel_5MuonVeto_ZDCsumsAC->Fill(6000.);
		 else if ((ibits2 & TrigDefs::L1_isPassedAfterVeto)) LbLSel_5MuonVeto_ZDCsumsAC->Fill(3000.);
		 else     LbLSel_5MuonVeto_ZDCsumsAC->Fill(0.);
		 }*/
		 //LbLSel_5MuonVeto_ZDCsumsA ->Fill();
		 //LbLSel_5MuonVeto_ZDCsumsC ->Fill();
		 //LbLSel_5MuonVeto_ZDCsumsAC->Fill();     
//	       }


           if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && m_goodTracks.size() == 0
	       && ( (Z->fourMomentum().Pt()/1e3 < 1. && Z->fourMomentum().M()/1e3 <= 12.) || (Z->fourMomentum().Pt()/1e3 < 2. && Z->fourMomentum().M()/1e3 > 12.) )
	       && m_goodPixTracks.size() == 0
	       //&& nMuons == 0
	       //&& (*sysListItr).name()!="" 
	       ){
	         float aco = 1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi();
	         if(aco<0.01 ){ 
		    this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_6AcoCut"+(*sysListItr).name()); 	
		    LbLSel_6AcoCut_nMuons->Fill(nMuons);
		   // std::cout<<m_sum_pix<<std::endl;
		    
		    if( (Z->photon1()->pt()/1e3) > 3.0 && (Z->photon2()->pt()/1e3) > 3.0 ) this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_3GeV_6AcoCut"+(*sysListItr).name()); 
		 }       
	       }




           if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && m_goodTracks.size() == 0
	       && (*sysListItr).name()=="" 
	       ){ 
	         float aco = 1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi();
		 
		 if(Z->fourMomentum().Pt()/1e3 > 2. && nMuons == 0) h_fakeABCD_C->Fill(aco);
		 
	         if(aco>0.01 ){
		   this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_CtrlNoAcoPt"); 
		   LbLSel_CtrlNoAcoPt_nMuons->Fill(nMuons);
		   }		 
		}		

           if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && ( (Z->fourMomentum().Pt()/1e3 < 1. && Z->fourMomentum().M()/1e3 <= 12.) || (Z->fourMomentum().Pt()/1e3 < 2. && Z->fourMomentum().M()/1e3 > 12.) )
	       && m_goodTracks.size() == 0
	       && (*sysListItr).name()=="" 
	       ){
		 float aco = 1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi();
	         if(aco>0.01 ) this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_CtrlNoAco"); 	
		}

           if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && ( (Z->fourMomentum().Pt()/1e3 < 1. && Z->fourMomentum().M()/1e3 <= 12.) || (Z->fourMomentum().Pt()/1e3 < 2. && Z->fourMomentum().M()/1e3 > 12.) )
	       && m_goodTracks.size() == 0
	       && m_goodPixTracks.size() == 0
	      // && (*sysListItr).name()=="" 
	       ){
	         float aco = 1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi();
	         if(aco>0.01 ){ 
		   this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_CtrlNoAcoNoPixTrk"+(*sysListItr).name()); 		   
		   LbLSel_CtrlNoAcoNoPixTrk_nMuons->Fill(nMuons);
		   LbLSel_CtrlNoAcoNoPixTrk_nMuons2->Fill(nMuons2);	  
		   
		   if( (Z->photon1()->pt()/1e3) > 3.0 && (Z->photon2()->pt()/1e3) > 3.0 ) this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_3GeV_CtrlNoAcoNoPixTrk"+(*sysListItr).name());  	   
		  }  	
		}
		
	if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && ( (Z->fourMomentum().Pt()/1e3 < 1. && Z->fourMomentum().M()/1e3 <= 12.) || (Z->fourMomentum().Pt()/1e3 < 2. && Z->fourMomentum().M()/1e3 > 12.) )
	       && m_goodTracks.size() == 0
	       && m_goodPixTracks.size() == 1
	       && (*sysListItr).name()=="" 
	       ){
		   this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_CtrlNoAco1PixTrk"+(*sysListItr).name()); 		   	  		   
		   if( (Z->photon1()->pt()/1e3) > 3.0 && (Z->photon2()->pt()/1e3) > 3.0 ) this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_3GeV_CtrlNoAco1PixTrk"+(*sysListItr).name());  	   
	
		}
		
	if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && ( (Z->fourMomentum().Pt()/1e3 < 1. && Z->fourMomentum().M()/1e3 <= 12.) || (Z->fourMomentum().Pt()/1e3 < 2. && Z->fourMomentum().M()/1e3 > 12.) )
	       && m_goodTracks.size() == 0
	       && m_goodPixTracks.size() == 2
	       && (*sysListItr).name()=="" 
	       ){
		   this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_CtrlNoAco2PixTrk"+(*sysListItr).name()); 		   	  		   
		   if( (Z->photon1()->pt()/1e3) > 3.0 && (Z->photon2()->pt()/1e3) > 3.0 ) this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_3GeV_CtrlNoAco2PixTrk"+(*sysListItr).name());  	   
	
		}

/*           if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && ( (Z->fourMomentum().Pt()/1e3 < 1. && Z->fourMomentum().M()/1e3 <= 12.) || (Z->fourMomentum().Pt()/1e3 < 2. && Z->fourMomentum().M()/1e3 > 12.) )
	       && m_goodTracks.size() == 0
	       && m_goodPixTracks.size() == 0
	       && m_goodPhotons.at(0)->caloCluster()->time()<-10.
	       && (*sysListItr).name()=="" 
	       ){
	         float aco = 1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi();
	         if(aco>0.01 ){ 
		   this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_CtrlNoAcoNoPixTrkNoTime"); 		   	   	   
		  }  	
		}
*/		
	if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && m_goodTracks.size() == 0
	       && ( (Z->fourMomentum().Pt()/1e3 < 1. && Z->fourMomentum().M()/1e3 <= 12.) || (Z->fourMomentum().Pt()/1e3 < 2. && Z->fourMomentum().M()/1e3 > 12.) )
	       && m_goodPixTracks.size() > 0
	       && (*sysListItr).name()=="" 
	       ){
	         float aco = 1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi();
	         if(aco<0.01 ){ 
		    this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_CtrlPixTrk"); 	
		 }       
	       }
		
	  if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && m_goodTracks.size() == 1
	       && (*sysListItr).name()=="" 
	       ){ 
		 this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_Ctrl1Trk"); 	       
	       }


           if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && m_goodTracks.size() == 2
	      // && (*sysListItr).name()=="" 
	       ){
		 this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_Ctrl2Trk"+(*sysListItr).name()); 
	       }


           if ( Z->type() == 22
	       && m_goodPhotons.size() == 2
	       && (m_eventFired_2018_EMPTY)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && (*sysListItr).name()=="" 
	       ){
	         this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_1MCut_EMPTY"); 
		 LbLSel_1MCut_EMPTY_nMuons->Fill(nMuons);
	       }

           if ( Z->type() == 22
	       && m_goodPhotons.size() == 2 //temp change
	       && (m_eventFired_2018_EMPTY)
	       && (Z->photon1()->pt()/1e3) > 2.5 
	       && (Z->photon2()->pt()/1e3) > 2.5
	       && Z->fourMomentum().M()/1e3 > 5.
	       && Z->fourMomentum().Pt()/1e3 < 1.
	       && m_goodTracks.size() == 0
	       && m_goodPixTracks.size() == 0
	       && nMuons == 0
	       && (*sysListItr).name()=="" 
	       ){
	          //float aco = 1. - acos(cos(m_goodPhotonsNoPID.at(0)->phi()-m_goodPhotonsNoPID.at(1)->phi()))/TMath::Pi();
	          //if(aco<0.02) LbLSel_1MCut_EMPTY_time->Fill( m_goodPhotonsNoPID.at(0)->caloCluster()->time() );//fill time here...
		  this->fillHistos(E, m_eventInfo, weight, "2_LbLSel_2PtCut4GeV_EMPTY"); 

	       }
 
 	   
	   // fill histos for ABCD method
           if ( (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)
	        && m_goodTracks.size() == 0 
		&& nMuons == 0 
		&& (*sysListItr).name()==""){
	        
		if ( m_goodPhotonsF1Inv.size() == 2){
		  TLorentzVector g1, g2, gsum;
      		  g1.SetPtEtaPhiM(m_goodPhotonsF1Inv.at(0)->pt()/1e3,  m_goodPhotonsF1Inv.at(0)->eta(), m_goodPhotonsF1Inv.at(0)->phi(), m_goodPhotonsF1Inv.at(0)->m());
		  g2.SetPtEtaPhiM(m_goodPhotonsF1Inv.at(1)->pt()/1e3,  m_goodPhotonsF1Inv.at(1)->eta(), m_goodPhotonsF1Inv.at(1)->phi(), m_goodPhotonsF1Inv.at(1)->m());
		  gsum = g1+g2;
		  float aco = 1. - acos(cos(m_goodPhotonsF1Inv.at(0)->phi()-m_goodPhotonsF1Inv.at(1)->phi()))/TMath::Pi();
		  if(gsum.M() > 5. && gsum.Pt() < 2.) h_fakeABCD_B->Fill(aco);
		  if(gsum.M() > 5. && gsum.Pt() > 2.) h_fakeABCD_D->Fill(aco);
		  }
		if ( m_goodPhotonsF1Inv.size() == 1 && m_goodPhotons.size() == 1){
		  TLorentzVector g1, g2, gsum;
      		  g1.SetPtEtaPhiM(m_goodPhotonsF1Inv.at(0)->pt()/1e3,  m_goodPhotonsF1Inv.at(0)->eta(), m_goodPhotonsF1Inv.at(0)->phi(), m_goodPhotonsF1Inv.at(0)->m());
		  g2.SetPtEtaPhiM(m_goodPhotons.at(0)->pt()/1e3,  m_goodPhotons.at(0)->eta(), m_goodPhotons.at(0)->phi(), m_goodPhotons.at(0)->m());
		  gsum = g1+g2;
		  float aco = 1. - acos(cos(m_goodPhotonsF1Inv.at(0)->phi()-m_goodPhotons.at(0)->phi()))/TMath::Pi();
		  if(gsum.M() > 5. && gsum.Pt() < 2.) h_fakeABCD_B->Fill(aco);
		  if(gsum.M() > 5. && gsum.Pt() > 2.) h_fakeABCD_D->Fill(aco);
		  }		 
	      
	      
	      }   
	   


           //PHOTON reco TaP
           if (   m_goodElectrons.size()==1 //&& m_goodElectrons.size()<=2
	       && m_goodElectrons.at(0)->pt()/1e3 > 4.
	       && m_goodTracksTight.size() == 2       
	       && (m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200 || m_eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ)	
	       && fabs(m_goodTracksTight.at(0)->eta())<2.37
	       && fabs(m_goodTracksTight.at(1)->eta())<2.37	
	       && (fabs(m_goodTracksTight.at(0)->eta())<1.37 || fabs(m_goodTracksTight.at(0)->eta())>1.52)
	       && (fabs(m_goodTracksTight.at(1)->eta())<1.37 || fabs(m_goodTracksTight.at(1)->eta())>1.52)
	       && (1. - acos(cos(m_goodTracksTight.at(0)->phi() - m_goodTracksTight.at(1)->phi()))/TMath::Pi())<0.3
	       && (*sysListItr).name()=="" 
	       ){
	          
		  
		
		float aco_trk0 = 1. - acos(cos(m_goodElectrons.at(0)->phi() - m_goodTracksTight.at(0)->phi()))/TMath::Pi();		
		float aco_trk1 = 1. - acos(cos(m_goodElectrons.at(0)->phi() - m_goodTracksTight.at(1)->phi()))/TMath::Pi();

      		double g_dist0 = sqrt( (m_goodElectrons.at(0)->phi() - m_goodTracksTight.at(0)->phi())*(m_goodElectrons.at(0)->phi() - m_goodTracksTight.at(0)->phi())
      		       		+  (m_goodElectrons.at(0)->eta() - m_goodTracksTight.at(0)->eta())*(m_goodElectrons.at(0)->eta() - m_goodTracksTight.at(0)->eta()) );

      		double g_dist1 = sqrt( (m_goodElectrons.at(0)->phi() - m_goodTracksTight.at(1)->phi())*(m_goodElectrons.at(0)->phi() - m_goodTracksTight.at(1)->phi())
      		       		+  (m_goodElectrons.at(0)->eta() - m_goodTracksTight.at(1)->eta())*(m_goodElectrons.at(0)->eta() - m_goodTracksTight.at(1)->eta()) );
      
		   
		TLorentzVector e1, trk2, g2;
      		e1.SetPtEtaPhiM(m_goodElectrons.at(0)->pt()/1e3,  m_goodElectrons.at(0)->eta(), m_goodElectrons.at(0)->phi(), m_goodElectrons.at(0)->m()); 

		       		
		float ptdiff = -100.;
		float ptcl = -100.;
		float etacl = -100;
		float phicl = -100;
		bool trkPass = false;
		float etat = -100.;
		float phit = -100.;
		int nClus = 0;
		if( (g_dist0 < 1.0) && (m_goodTracksTight.at(1)->pt()/1e3 < 1.5) /*&& (nsct1>=6) && (npix1<=2)*/ ) {
		   ptdiff = (m_goodElectrons.at(0)->pt()/1e3 -m_goodTracksTight.at(1)->pt()/1e3 );
		   etat = m_goodTracksTight.at(1)->eta();
		   phit = m_goodTracksTight.at(1)->phi();
		   
		   for (int q=0; q< m_goodClusters.size(); q++){ 
		   double g_distq = sqrt( (m_goodClusters.at(q)->phi() - m_goodTracksTight.at(1)->phi())*(m_goodClusters.at(q)->phi() - m_goodTracksTight.at(1)->phi())
      		       		+  (m_goodClusters.at(q)->eta() - m_goodTracksTight.at(1)->eta())*(m_goodClusters.at(q)->eta() - m_goodTracksTight.at(1)->eta()) ); 
		   if (g_distq<0.3) {nClus++;  ptcl = m_goodClusters.at(q)->pt()/1e3; etacl = m_goodClusters.at(q)->eta(); phicl = m_goodClusters.at(q)->phi(); }
		   }
		   		       
      		   trk2.SetPtEtaPhiM(m_goodTracksTight.at(1)->pt()/1e3,  m_goodTracksTight.at(1)->eta(),
		       m_goodTracksTight.at(1)->phi(), 0.); 
		   trkPass = true;
		   }
		else if( (g_dist1 < 1.0) && (m_goodTracksTight.at(0)->pt()/1e3 < 1.5) /*&& (nsct0>=6) && (npix0<=2)*/ ) {
		   ptdiff = (m_goodElectrons.at(0)->pt()/1e3 -m_goodTracksTight.at(0)->pt()/1e3 );
		   etat = m_goodTracksTight.at(0)->eta();
		   phit = m_goodTracksTight.at(0)->phi();

		   for (int q=0; q< m_goodClusters.size(); q++){ 
		   double g_distq = sqrt( (m_goodClusters.at(q)->phi() - m_goodTracksTight.at(0)->phi())*(m_goodClusters.at(q)->phi() - m_goodTracksTight.at(0)->phi())
      		       		+  (m_goodClusters.at(q)->eta() - m_goodTracksTight.at(0)->eta())*(m_goodClusters.at(q)->eta() - m_goodTracksTight.at(0)->eta()) ); 
		   if (g_distq<0.3) { nClus++; ptcl = m_goodClusters.at(q)->pt()/1e3; etacl = m_goodClusters.at(q)->eta(); phicl = m_goodClusters.at(q)->phi(); }
		   }
		   
      		   trk2.SetPtEtaPhiM(m_goodTracksTight.at(0)->pt()/1e3,  m_goodTracksTight.at(0)->eta(),
		       m_goodTracksTight.at(0)->phi(), 0.); 	
		   trkPass = true;	   
		   }
		
		if (trkPass && nClus==1 && fabs(ptdiff-ptcl)/ptcl < 0.7){
		   h_EleTrkTag -> Fill ( ptdiff  );	
		   h_EleTrkPt ->Fill(m_goodElectrons.at(0)->pt()/1e3 - ptdiff);
		   h_EleEtaTag -> Fill(etat);
		}
		  
		  bool findPhoton = false;
		   for (int q=0; q< m_goodPhotonsNoPID.size(); q++){ 
		   double g_distq = sqrt( (m_goodPhotonsNoPID.at(q)->phi() - phicl)*(m_goodPhotonsNoPID.at(q)->phi() - phicl)
      		       		+  (m_goodPhotonsNoPID.at(q)->eta() - etacl)*(m_goodPhotonsNoPID.at(q)->eta() - etacl) ); 
		   if (g_distq<0.4) findPhoton = true;
		   }
		
		if(/*m_goodPhotonsNoPID.size() >= 1*/ findPhoton && trkPass && nClus ==1 && fabs(ptdiff-ptcl)/ptcl < 0.7 ){
		   
		   h_EleTrkPtP ->Fill(m_goodElectrons.at(0)->pt()/1e3 - ptdiff);
		   h_EleEtaProbe -> Fill(etat);
		   h_EleTrkProbe -> Fill ( ptdiff);	   
		   h_EleGamPt ->Fill(m_goodPhotonsNoPID.at(0)->pt()/1e3);
		   h_EleGamEta ->Fill(m_goodPhotonsNoPID.at(0)->eta());
		   
		   h_eeg_F1   ->Fill(m_goodPhotonsNoPID.at(0)->showerShapeValue(xAOD::EgammaParameters::f1));
		   h_eeg_Reta ->Fill(m_goodPhotonsNoPID.at(0)->showerShapeValue(xAOD::EgammaParameters::Reta));
		   h_eeg_Weta2 ->Fill(m_goodPhotonsNoPID.at(0)->showerShapeValue(xAOD::EgammaParameters::weta2));
		   h_eeg_Eratio ->Fill(m_goodPhotonsNoPID.at(0)->showerShapeValue(xAOD::EgammaParameters::Eratio));
		   h_eeg_Rhad ->Fill(m_goodPhotonsNoPID.at(0)->showerShapeValue(xAOD::EgammaParameters::Rhad));
		   h_eeg_Rhad1 ->Fill(m_goodPhotonsNoPID.at(0)->showerShapeValue(xAOD::EgammaParameters::Rhad1));
		   h_eeg_Time ->Fill(m_goodPhotonsNoPID.at(0)->caloCluster()->time());
		     
      		   g2.SetPtEtaPhiM(m_goodPhotonsNoPID.at(0)->pt()/1e3,  m_goodPhotonsNoPID.at(0)->eta(), m_goodPhotonsNoPID.at(0)->phi(), m_goodPhotonsNoPID.at(0)->m()); 
		       		   
		   TLorentzVector tot = e1+trk2+g2;
		   h_ElePtllg->Fill(tot.Pt());
		   
		   double dr0 = sqrt( (g2.Phi() - trk2.Phi())*(g2.Phi() - trk2.Phi()) + (g2.Eta() - trk2.Eta())*(g2.Eta() - trk2.Eta()) );
		   h_EleGamDR ->Fill(dr0);
		   
		   // for PID efficiency
		   if(m_goodPhotons.size() == 1 ){ 
		   	h_EleGamPt3ID->Fill( m_goodPhotons.at(0)->pt()/1e3 );
			h_EleGamEta3ID->Fill( m_goodPhotons.at(0)->eta() );
			h_EleGamConv3ID->Fill( m_goodPhotons.at(0)->conversionType() );
			h_EleGamNtrk3ID->Fill( m_goodTracks.size() );
			}
		}
					  
	       }


       }


    if (m_isMC && (*sysListItr).name()==""){
    
  	const xAOD::TruthParticleContainer* tlept = 0;
  	evtStore()->retrieve( tlept, "TruthParticles" ).ignore();
  	xAOD::TruthParticleContainer::const_iterator tlept_itr = tlept->begin();
  	xAOD::TruthParticleContainer::const_iterator tlept_end = tlept->end();
	
	TLorentzVector g1t,g2t;
 

  	for (; tlept_itr != tlept_end; ++tlept_itr)
  	{    
      		//if ( fabs((*tlept_itr)->pdgId()) != 11 ) continue;     
      		if( (*tlept_itr)->status() != 1 ) continue;		
		//if( (*tlept_itr)->pt()/1000. < 2.5 ) continue;
		//if( fabs((*tlept_itr)->eta()) > 2.35 ) continue;
		//if( fabs((*tlept_itr)->eta()) > 1.35 && fabs((*tlept_itr)->eta()) < 1.55 ) continue;
		if( (*tlept_itr)->barcode() >= 200000 ) continue;		
		// only initial photons!
		if( (*tlept_itr)->barcode() < 10005 || (*tlept_itr)->barcode() > 10006 ) continue;
		
		if( (*tlept_itr)->barcode() == 10005 ) g1t.SetPtEtaPhiM((*tlept_itr)->pt()/1000.,  (*tlept_itr)->eta(), (*tlept_itr)->phi(), 0.);     
      		if( (*tlept_itr)->barcode() == 10006 ) g2t.SetPtEtaPhiM((*tlept_itr)->pt()/1000.,  (*tlept_itr)->eta(), (*tlept_itr)->phi(), 0.); 
		
		
		h_TruthPt->Fill((*tlept_itr)->pt()/1000.);
		h_TruthEta->Fill((*tlept_itr)->eta());

			for(unsigned int ei2=0; ei2 < m_goodClusters.size(); ei2++)
    			{ 
      		  		double g_dist2 = sqrt( (m_goodClusters.at(ei2)->phi() -
				(*tlept_itr)->phi())*(m_goodClusters.at(ei2)->phi() - (*tlept_itr)->phi()  )
      		       	             +  (m_goodClusters.at(ei2)->eta() - (*tlept_itr)->eta())*(m_goodClusters.at(ei2)->eta() - (*tlept_itr)->eta()  ) );
      
      		  	  if (g_dist2 < 0.4){		
			     h_TruthPt1->Fill((*tlept_itr)->pt()/1000.);
			     h_TruthEta1->Fill((*tlept_itr)->eta());
			     break;	   
			  }		  
   			}
	}
		
    TLorentzVector gtot = g1t+g2t;
    float gmass = gtot.M();
    float gpt = gtot.Pt();
    float gaco = (1. - acos(cos(g1t.Phi()-g2t.Phi()))/TMath::Pi());
    if(gmass>5. && gpt <1. && gaco <0.01 && g1t.Pt()>2.5 && g2t.Pt()>2.5 && fabs(g1t.Eta())<2.4 && fabs(g2t.Eta())<2.4 ) h_TruthMass->Fill(gmass);
    if(gmass>6. && gpt <1. && gaco <0.01 && g1t.Pt()>3. && g2t.Pt()>3. && fabs(g1t.Eta())<2.4 && fabs(g2t.Eta())<2.4 ) h_TruthMass3GeV->Fill(gmass);
    
    
	
    }
	
	
	
	
	
	
////////////////////////////////////////////
// old code...
////////////////////////////////////////////


    if (0){
    
  	const xAOD::TruthParticleContainer* tlept = 0;
  	m_xAODEvent->m_event->retrieve( tlept, "TruthParticles" ).ignore();
  	xAOD::TruthParticleContainer::const_iterator tlept_itr = tlept->begin();
  	xAOD::TruthParticleContainer::const_iterator tlept_end = tlept->end();
  
  	TLorentzVector Z;
  	
	int s_ele = 0;
	int s_ele3GeV = 0;
	int s_ele35GeV = 0;
	float phip1 = 0.;
 	float phip2 = 0.;
  	for (; tlept_itr != tlept_end; ++tlept_itr)
  	{    
      		//if ( fabs((*tlept_itr)->pdgId()) != 11 ) continue;     
      		if( (*tlept_itr)->status() != 1 ) continue;
		
		if( (*tlept_itr)->pt()/1000. < 3.0 ) continue;
		if( fabs((*tlept_itr)->eta()) > 2.4 ) continue;
		if( (*tlept_itr)->barcode() >= 200000 ) continue;
		
		// only initial photons!
		if( (*tlept_itr)->barcode() < 10005 || (*tlept_itr)->barcode() > 10006 ) continue;
		
		if( (*tlept_itr)->barcode() == 10005) phip1 = (*tlept_itr)->phi();
		if( (*tlept_itr)->barcode() == 10006) phip2 = (*tlept_itr)->phi();


		//if( (*tlept_itr)->barcode() < 10001 || (*tlept_itr)->barcode() > 10002 ) continue;
		
		
		h_TruthPt->Fill((*tlept_itr)->pt()/1000.);
		h_TruthEta->Fill((*tlept_itr)->eta());
		
		if( (*tlept_itr)->pt()/1000. > 3. ) s_ele3GeV++;
		if( (*tlept_itr)->pt()/1000. > 3.5 ) s_ele35GeV++;

		//for(unsigned int ei=0; ei < m_xAODEvent->m_goodTracks.size(); ei++)
    		//{ 
      		//  double g_dist = sqrt( (m_xAODEvent->m_goodTracks.at(ei)->phi() - (*tlept_itr)->phi())*(m_xAODEvent->m_goodTracks.at(ei)->phi() - (*tlept_itr)->phi()  )
      		//       	             +  (m_xAODEvent->m_goodTracks.at(ei)->eta() - (*tlept_itr)->eta())*(m_xAODEvent->m_goodTracks.at(ei)->eta() - (*tlept_itr)->eta()  ) );
      
      		//  if (g_dist < 0.4) h_TruthPt3->Fill(m_xAODEvent->m_goodTracks.at(ei)->pt()/1e3);
		  
		  
		  /*
			for(unsigned int ei2=0; ei2 < m_xAODEvent->m_goodElectrons.size(); ei2++)
    			{ 
      		  		double g_dist2 = sqrt( (m_xAODEvent->m_goodElectrons.at(ei2)->phi() - (*tlept_itr)->phi())*(m_xAODEvent->m_goodElectrons.at(ei2)->phi() - (*tlept_itr)->phi()  )
      		       	             +  (m_xAODEvent->m_goodElectrons.at(ei2)->eta() - (*tlept_itr)->eta())*(m_xAODEvent->m_goodElectrons.at(ei2)->eta() - (*tlept_itr)->eta()  ) );
      
      		  	   if (g_dist2 < 0.4){ 
			     //h_TruthPt1->Fill((*tlept_itr)->pt()/1000.);
			     h_TruthPt1->Fill(m_xAODEvent->m_goodElectrons.at(ei2)->pt()/1000.);
			     h_TruthEta1->Fill((*tlept_itr)->eta());
			     
			   }
   			}
		*/
			for(unsigned int ei2=0; ei2 < m_xAODEvent->m_goodPhotons.size(); ei2++)
    			{ 
      		  		double g_dist2 = sqrt( (m_xAODEvent->m_goodPhotons.at(ei2)->phi() -
				(*tlept_itr)->phi())*(m_xAODEvent->m_goodPhotons.at(ei2)->phi() - (*tlept_itr)->phi()  )
      		       	             +  (m_xAODEvent->m_goodPhotons.at(ei2)->eta() - (*tlept_itr)->eta())*(m_xAODEvent->m_goodPhotons.at(ei2)->eta() - (*tlept_itr)->eta()  ) );
      
      		  	  if (g_dist2 < 0.4){
			
			     h_TruthPt1->Fill((*tlept_itr)->pt()/1000.);
			     // photon (egamma cluster) energy resolution
			     float photonEt = m_xAODEvent->m_goodPhotons.at(ei2)->pt()/1e3;
			     if( photonEt>3. && photonEt<4. )   h_DEt34->Fill( ((*tlept_itr)->pt()/1000. - photonEt)/photonEt );
			     if( photonEt>4. && photonEt<5. )   h_DEt45->Fill( ((*tlept_itr)->pt()/1000. - photonEt)/photonEt );
			     if( photonEt>5. && photonEt<7. )   h_DEt57->Fill( ((*tlept_itr)->pt()/1000. - photonEt)/photonEt );
			     if( photonEt>7. && photonEt<10. )  h_DEt710->Fill( ((*tlept_itr)->pt()/1000. - photonEt)/photonEt );
			     if( photonEt>10. && photonEt<15. ) h_DEt1015->Fill( ((*tlept_itr)->pt()/1000. - photonEt)/photonEt );
			   
			  }
			  
   			}
			
		  
   		//}
		
		s_ele++;
      
      		TLorentzVector mu1;
      		mu1.SetPtEtaPhiM((*tlept_itr)->pt()/1000.,  (*tlept_itr)->eta(),
		       (*tlept_itr)->phi(), (*tlept_itr)->m()/1000.);     
      		Z += mu1;      
  	}
	
	//std::cout<<"\n";
        float acopp = 1. - acos(cos(phip1-phip2))/TMath::Pi();
	if(s_ele==2 && Z.Pt()<2. && acopp<0.01) h_TruthMass->Fill(Z.M());
	
	if(s_ele3GeV==2) h_TruthMass3GeV->Fill(Z.M());
	if(s_ele35GeV==2) h_TruthMass35GeV->Fill(Z.M());
	

	
    }

   
/////////////////////////////////////////
	
     float sum_pix=40.;
	
     //if(m_xAODEvent-> m_goodTracks.size() == 0){	
/*	std::string METTag="HLT_xAOD__TrigSpacePointCountsContainer_spacepoints";	
	const xAOD::TrigSpacePointCountsContainer* SpacePointCountsCont=0;
     if(m_xAODEvent->m_event->retrieve( SpacePointCountsCont, METTag ).isSuccess()){

	//std::vector<float> getVec;
	sum_pix=0.;
	// Loop over container content
	for(uint i = 0; i < SpacePointCountsCont->size(); i++) {
	
	
	sum_pix += SpacePointCountsCont->at(i)->pixelClusBarrelSumEntries(20., 0., xAOD::TrigHistoCutType::ABOVE_X_ABOVE_Y);
	sum_pix += SpacePointCountsCont->at(i)->pixelClusEndcapASumEntries(20., 0., xAOD::TrigHistoCutType::ABOVE_X_ABOVE_Y);
	sum_pix += SpacePointCountsCont->at(i)->pixelClusEndcapCSumEntries(20., 0., xAOD::TrigHistoCutType::ABOVE_X_ABOVE_Y);
	
	//getVec = SpacePointCountsCont->at(i)->contentsPixelClusBarrel();
	//for (uint j = 0; j < getVec.size(); ++j) { if(getVec[j]>0.1) sum_pix += getVec[j];}//sum_pix += getVec[j];

	//getVec = SpacePointCountsCont->at(i)->contentsPixelClusEndcapA();
	//for (uint j = 0; j < getVec.size(); ++j) { if(getVec[j]>0.1) sum_pix += getVec[j];}//sum_pix += getVec[j];
	
	//getVec = SpacePointCountsCont->at(i)->contentsPixelClusEndcapC();
	//for (uint j = 0; j < getVec.size(); ++j) { if(getVec[j]>0.1) sum_pix += getVec[j];}//sum_pix += getVec[j];
	}
    }*/
   //}
/////////////////////////////////////////

    //--- Check the inclusive spectrum 
    if ( 0 ) {

		

    	   ExclVetoCandidate *V = new ExclVetoCandidate(m_xAODEvent-> m_goodTracks, m_xAODEvent-> m_goodTracksPriVtx, 
	     		       m_xAODEvent-> m_goodVertices, m_xAODEvent->pv );	
			          
	   PairCandidate *Z =  findPairCandidates(m_xAODEvent->m_goodElectrons, m_xAODEvent->m_goodMuons, m_xAODEvent->m_goodPhotons, m_xAODEvent-> m_goodTracks);
	   EXCLCandidate *E = new EXCLCandidate(1, Z, V, m_sum_pix);


           if (   m_xAODEvent->m_goodMuons.size() == 0
	       && m_xAODEvent->m_goodElectrons.size() == 1
	       && //m_xAODEvent->m_eventFiredHLT_mb_sptrk_vetombts2in_L1MU0_VTE50 
	           m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && m_xAODEvent->m_goodTracks.size() == 1
	       && (*sysListItr).name()=="" ){
		//float aco = 1. - acos(cos( m_xAODEvent->m_goodMuons.at(0)->phi() - m_xAODEvent->m_goodElectrons.at(0)->phi() ))/TMath::Pi();	
		//if(aco>0.5){
		//h_em_aco->Fill(aco);
		//h_em_ptmu->Fill(m_xAODEvent->m_goodMuons.at(0)->pt()/1e3);
		h_em_pte->Fill(m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3);
		h_em_ngam->Fill(m_xAODEvent->m_goodPhotons.size());
		//h_em_deta->Fill(fabs(m_xAODEvent->m_goodMuons.at(0)->eta() - m_xAODEvent->m_goodElectrons.at(0)->eta()));
	        
		 /*  std::cout<<"emuevent ptmu:"<<m_xAODEvent->m_goodMuons.at(0)->pt()/1e3<<std::endl;
		   std::cout<<"emuevent pte:"<<m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3<<std::endl;
		   std::cout<<"emuevent pttrk1:"<<m_xAODEvent->m_goodTracks.at(0)->pt()/1e3<<std::endl;
		   std::cout<<"emuevent pttrk2:"<<m_xAODEvent->m_goodTracks.at(1)->pt()/1e3<<std::endl;
		   
		   std::cout<<"emuevent phimu:"<<m_xAODEvent->m_goodMuons.at(0)->phi()<<std::endl;
		   std::cout<<"emuevent phie:"<<m_xAODEvent->m_goodElectrons.at(0)->phi()<<std::endl;
		   std::cout<<"emuevent phitrk1:"<<m_xAODEvent->m_goodTracks.at(0)->phi()<<std::endl;
		   std::cout<<"emuevent phitrk2:"<<m_xAODEvent->m_goodTracks.at(1)->phi()<<std::endl;	
		   std::cout<<"emuevent "<<std::endl;	   
		   */

		//}
	       }


           if ( Z->type() 
	       && (*sysListItr).name()=="" 
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200){
	       this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "0_Default"); 
                /*   if(!m_xAODEvent->m_isMC){
                   std::cout<<"searching event:"<<m_xAODEvent->m_eventInfo->eventNumber()
                   <<" run:"<<m_xAODEvent->m_eventInfo->runNumber()
                   <<" lbn:"<<m_xAODEvent->m_eventInfo->lumiBlock()
                   <<" mass:"<<Z->fourMomentum().M()/1e3
                   <<" pt:"<<Z->fourMomentum().Pt()/1e3<<std::endl;
                   } */
		}

           if ( Z->type() 
	       && m_xAODEvent-> m_goodTracks.size() == 0
	       && (*sysListItr).name()=="" ){
	       this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "0_NoTracks"); }

           if ( Z->type() 
	       && m_xAODEvent-> m_goodTracks.size() == 2
	       && (*sysListItr).name()=="" ){
	       this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "0_2Tracks"); }

           if ( Z->type() == 22
	       && m_xAODEvent->m_goodPhotons.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (Z->photon1()->pt()/1e3) > 3. 
	       && (Z->photon2()->pt()/1e3) > 3.
	       && Z->fourMomentum().M()/1e3 > 6.
	       && m_xAODEvent->m_goodTracks.size() == 0
	       && (*sysListItr).name()=="" ){
	       

	        //const Trig::ChainGroup* cg = m_xAODEvent->m_trigDecisionTool->getChainGroup("L1MU0");
    		//if( cg->isPassed() ) 
		if(m_xAODEvent->m_goodMuons.size() >= 1) this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "0_FakesL1Mu0"); 
		
		}


           if ( Z->type() 
	       && m_xAODEvent->m_goodElectrons.size() == 2
	       && m_xAODEvent-> m_goodTracks.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (*sysListItr).name()=="" ){
	       
	       float aco = 1. - acos(cos(Z->electron1()->phi()-Z->electron2()->phi()))/TMath::Pi();
	       this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "1_HLT_hi_gg_upc_L1TE5_VTE200_2Tracks"); 
	       	   //std::cout<<"signal trigger electrons"
		   //<<" pixel hits:"<<sum_pix
		   //<<std::endl;
		   h_npix_hits_1_HLT_hi_gg_upc_L1TE5_VTE200_2Tracks->Fill(sum_pix);
	       
	       //h_zdca->Fill(m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(0));
	       //h_zdcc->Fill(m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(1));
	       //std::cout<<"ZDC electron activity, m(ee)="<< Z->fourMomentum().M()/1e3<<" aco="<< aco <<"  A="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(0)<<"  C="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(1)<<std::endl;
	       //if(Z->fourMomentum().M()/1e3<10) std::cout<<"ZDC electron activity, m(10GeV)="<< Z->fourMomentum().M()/1e3 <<" aco="<< aco<<"  A="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(0)<<"  C="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(1)<<std::endl;
	       //if(Z->fourMomentum().M()/1e3>80) std::cout<<"ZDC electron activity, m(80GeV)="<< Z->fourMomentum().M()/1e3 <<" aco="<< aco<<"  A="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(0)<<"  C="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(1)<<std::endl;
	       
	       }

           if ( Z->type() 
	       && m_xAODEvent->m_goodElectrons.size() == 2
	       && m_xAODEvent-> m_goodTracks.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (*sysListItr).name()=="" ){
	         
		 uint8_t pix1;
   		 m_xAODEvent-> m_goodTracks.at(0)->summaryValue(pix1,xAOD::numberOfPixelHits);
   		 uint8_t pix2;
   		 m_xAODEvent-> m_goodTracks.at(1)->summaryValue(pix2,xAOD::numberOfPixelHits); 
	       
	         if((pix1+pix2)<=8) this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "1_HLT_hi_gg_upc_L1TE5_VTE200_2Tracks8PixHits"); }

           if ( Z->type() 
	       && m_xAODEvent->m_goodElectrons.size() == 2
	       && m_xAODEvent-> m_goodTracks.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (*sysListItr).name()=="" ){
	       
	         double weight2 = m_xAODEvent->weight*EleEffTrkPtSF(m_xAODEvent->m_goodElectrons.at(0))
		 *EleEffTrkPtSF(m_xAODEvent->m_goodElectrons.at(1));

	         //double weight2 = m_xAODEvent->weight*EleEffTrkPtSF(m_xAODEvent->m_goodTracks.at(0))
		 //*EleEffTrkPtSF(m_xAODEvent->m_goodTracks.at(1));
	         
		 uint8_t pix1;
   		 m_xAODEvent-> m_goodTracks.at(0)->summaryValue(pix1,xAOD::numberOfPixelHits);
   		 uint8_t pix2;
   		 m_xAODEvent-> m_goodTracks.at(1)->summaryValue(pix2,xAOD::numberOfPixelHits); 
	       
	         if((pix1+pix2)<=8) this->fillHistos(E, m_xAODEvent->m_eventInfo, weight2, "1_HLT_hi_gg_upc_L1TE5_VTE200_2Tracks8PixHitsEleSF"); }

           if ( Z->type() == 11
	       && m_xAODEvent->m_goodElectrons.size() == 2
	       && (Z->electron1()->pt()/1e3) > 5. 
	       && (Z->electron2()->pt()/1e3) > 5.
	       && m_xAODEvent-> m_goodTracks.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (*sysListItr).name()=="" ){
	       this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "1_HLT_hi_gg_upc_L1TE5_VTE200_2TracksPt5GeV"); }


           if ( Z->type() == 11
	       && m_xAODEvent->m_goodElectrons.size() == 2
	       && fabs(Z->electron1()->eta()) < 1.37
	       && fabs(Z->electron2()->eta()) < 1.37
	       && m_xAODEvent-> m_goodTracks.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (*sysListItr).name()=="" ){
	       float weight2 = m_xAODEvent->weight;
	       if(m_xAODEvent->m_isMC) weight2 *= EleEffTrkPtSF(m_xAODEvent->m_goodElectrons.at(0))*EleEffTrkPtSF(m_xAODEvent->m_goodElectrons.at(1));
	       this->fillHistos(E, m_xAODEvent->m_eventInfo, weight2, "1_HLT_hi_gg_upc_L1TE5_VTE200_2TracksEta137"); }



           if ( Z->type() ==22
	       && m_xAODEvent-> m_goodTracks.size() <= 20
	       
	       //&& m_xAODEvent->m_eventInfo->runNumber() == 287866
	       
	       && m_xAODEvent->m_eventFiredHLT_mb_sptrk_ion_L1ZDC_A_C_VTE50
	       && (*sysListItr).name()=="" ){
	       
	       float aco = 1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi();
	         if(aco<0.1){
	         this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "1_HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50_2Tracks"); 
	       
	         //if(Z->type()==22) std::cout<<"strange evt, run="<< m_xAODEvent->m_eventInfo->runNumber() <<std::endl;
	         //if(Z->type()==11 || Z->type()==1111) std::cout<<"strange evtee, run="<< m_xAODEvent->m_eventInfo->runNumber() <<std::endl;
	       //std::cout<<"L1ZDC_A_C dilepton activity, pt(ll)="<< Z->fourMomentum().Pt()/1e3 <<"  A="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(0)<<"  C="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(1)<<std::endl;
	         }
	       }

           if ( Z->type() 
	       && m_xAODEvent-> m_goodTracks.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_mb_sptrk_vetombts2in_L1ZDC_A_C_VTE50
	       && (*sysListItr).name()=="" ){
	       this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "1_HLT_mb_sptrk_vetombts2in_L1ZDC_A_C_VTE50_2Tracks"); }

           if ( Z->type() 
	       && m_xAODEvent-> m_goodTracks.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_loose_upc_L1ZDC_A_C_VTE50 
	       && (*sysListItr).name()=="" ){
	       this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "1_HLT_hi_loose_upc_L1ZDC_A_C_VTE50_2Tracks"); }

           if ( Z->type() 
	       && m_xAODEvent-> m_goodTracks.size() == 2
	       && m_xAODEvent->m_goodMuons.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_mb_sptrk_vetombts2in_L1MU0_VTE50
	       && (*sysListItr).name()=="" ){
		float aco = 1. - acos(cos(Z->muon1()->phi()-Z->muon2()->phi()))/TMath::Pi();	       
	       this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "1_HLT_mb_sptrk_vetombts2in_L1MU0_VTE50_2Tracks"); 
	       
	       if(aco<0.01) h_npix_hits->Fill(sum_pix);
	       //h_zdca->Fill(m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(0));
	       //h_zdcc->Fill(m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(1));
	       //std::cout<<"ZDC muon activity, m(mumu)="<< Z->fourMomentum().M()/1e3 <<" aco="<< aco<<"  A="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(0)<<"  C="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(1)<<std::endl;
	       //if(Z->fourMomentum().M()/1e3<10) std::cout<<"ZDC muon activity, m(10GeV)="<< Z->fourMomentum().M()/1e3 <<" aco="<< aco<<"  A="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(0)<<"  C="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(1)<<std::endl;
	       //if(Z->fourMomentum().M()/1e3>80) std::cout<<"ZDC muon activity, m(80GeV)="<< Z->fourMomentum().M()/1e3 <<" aco="<< aco<<"  A="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(0)<<"  C="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(1)<<std::endl;
	      
	       }


           if ( Z->type() == 11
	       && m_xAODEvent-> m_goodTracks.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_mb_sptrk_ion_L1ZDC_A_C_VTE50
	       && m_xAODEvent->m_eventFiredHLT_TE5
	       && (*sysListItr).name()=="" ){
	       this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "1_HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50_2Tracks_TE5"); 
	       	   std::cout<<"control trigger electrons"
		   <<" pixel hits:"<<sum_pix
		   <<std::endl;
		   h_npix_hits_1_HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50_2Tracks_TE5->Fill(sum_pix);
		   }


           if ( Z->type() == 22
	       && m_xAODEvent->m_goodPhotons.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       //&& (Z->photon1()->pt()/Z->fourMomentum().M()) > 0.4 
	       //&& (Z->photon2()->pt()/Z->fourMomentum().M()) > 0.3
	       && Z->fourMomentum().M()/1e3 > 5.
	       && (*sysListItr).name()=="" ){
	       this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_1MCut"); }

           if ( Z->type() == 22
	       && m_xAODEvent->m_goodPhotons.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       //&& (Z->photon1()->pt()/Z->fourMomentum().M()) > 0.4 
	       //&& (Z->photon2()->pt()/Z->fourMomentum().M()) > 0.3
	       && Z->fourMomentum().M()/1e3 > 5.
	       && (*sysListItr).name()=="" ){
	         float aco = 1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi();
	         if(aco<0.2) this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_2AcoCut"); 	       
	       }

           if ( Z->type() == 22
	       && m_xAODEvent->m_goodPhotons.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (Z->photon1()->pt()/1e3) > 3. 
	       && (Z->photon2()->pt()/1e3) > 3.
	       && Z->fourMomentum().M()/1e3 > 6.
	       && (*sysListItr).name()=="" ){
	       this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_1MCutEt3GeV"); 
	       
	       if(!m_xAODEvent->m_isMC) h_RunDependence->Fill(m_xAODEvent->m_eventInfo->runNumber());
	       
	       }


           if ( Z->type() == 22
	       && m_xAODEvent->m_goodPhotons.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (Z->photon1()->pt()/1e3) > 3. 
	       && (Z->photon2()->pt()/1e3) > 3.
	       && Z->fourMomentum().M()/1e3 > 6.
	       && (*sysListItr).name()=="" ){
	         float aco = 1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi();
	         if(aco<0.01) this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_2AcoCutEt3GeV"); 	       
	       }

           if ( Z->type() == 22
	       && m_xAODEvent->m_goodPhotons.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (Z->photon1()->pt()/1e3) > 3. 
	       && (Z->photon2()->pt()/1e3) > 3.
	       && Z->fourMomentum().M()/1e3 > 6.
	       && (*sysListItr).name()=="" ){
	         float aco = 1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi();
	         if(aco<0.01 && Z->fourMomentum().Pt()/1e3 < 2.) this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_3PtCutEt3GeV"); 	       
	       }

           if ( Z->type() == 22
	       && m_xAODEvent->m_goodPhotons.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (Z->photon1()->pt()/1e3) > 3. 
	       && (Z->photon2()->pt()/1e3) > 3.
	       && Z->fourMomentum().M()/1e3 > 6.
	       && m_xAODEvent->m_goodTracks.size() == 0
	       && (*sysListItr).name()=="" ){
	         
/*		  TLorentzVector gm1, gm2, gm3;
      		  gm1.SetPtEtaPhiM(goodPhotons.at(0)->pt(),  goodPhotons.at(0)->eta(),goodPhotons.at(0)->phi(), goodPhotons.at(0)->m()); 
      		  gm2.SetPtEtaPhiM(goodPhotons.at(1)->pt(),  goodPhotons.at(1)->eta(),goodPhotons.at(1)->phi(), goodPhotons.at(1)->m());
		  gm3.SetPtEtaPhiM(goodPhotons.at(2)->pt(),  goodPhotons.at(2)->eta(),goodPhotons.at(2)->phi(), goodPhotons.at(2)->m());
      			TLorentzVector Z3 = gm1+gm2+gm3;
		 h_3g_ngam->Fill(m_xAODEvent->m_goodPhotons.size());
		 h_3g_m->Fill(Z3.M()/1e3);
		 h_3g_pt->Fill(Z3.Pt()/1e3);
*/	         
	         float aco = 1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi();
	         if(aco<0.01 && Z->fourMomentum().Pt()/1e3 < 2.){  
		 
		   this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_4NTrkCutEt3GeV"); 	   
	       
	           if(!m_xAODEvent->m_isMC){
	           std::cout<<"interesting event:"<<m_xAODEvent->m_eventInfo->eventNumber()
	           <<" run:"<<m_xAODEvent->m_eventInfo->runNumber()
		   <<" lbn:"<<m_xAODEvent->m_eventInfo->lumiBlock()
	           <<" mass:"<<Z->fourMomentum().M()/1e3
	           <<" pt:"<<Z->fourMomentum().Pt()/1e3
		   <<" aco:"<<aco
		   <<" pixel hits:"<<sum_pix
		   <<" Eratio1:"<<Z->photon1()->showerShapeValue(xAOD::EgammaParameters::Eratio)
		   <<" Eratio2:"<<Z->photon2()->showerShapeValue(xAOD::EgammaParameters::Eratio)
		   <<std::endl;	     
	           }
		   h_npix_hits_2_LbLSel_4NTrkCutEt3GeV->Fill(sum_pix);
	         }
		 
		 //std::cout<<"ZDC signal activity, pt(gg)="<< Z->fourMomentum().Pt()/1e3 <<"  A="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(0)<<"  C="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(1)<<std::endl;
		 
	       }
	       
	       
           if ( Z->type() == 22
	       && m_xAODEvent->m_goodPhotons.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (Z->photon1()->pt()/1e3) > 3. 
	       && (Z->photon2()->pt()/1e3) > 3.
	       && Z->fourMomentum().M()/1e3 > 6.
	       && m_xAODEvent->m_goodTracks.size() == 0
	       && (*sysListItr).name()=="" ){
	          if(Z->fourMomentum().Pt()/1e3 > 2.) this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_CtrlPtAbove2GeV"); 	       
	       }


           if ( Z->type() == 22
	       && m_xAODEvent->m_goodPhotons.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (Z->photon1()->pt()/1e3) > 3. 
	       && (Z->photon2()->pt()/1e3) > 3.
	       && Z->fourMomentum().M()/1e3 > 6.
	       && m_xAODEvent->m_goodTracks.size() == 0
	       && (*sysListItr).name()=="" ){
	          this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_CtrlNoAcoPt"); 
		  
		  //std::cout<<"ZDC noptnoaco activity, pt(gg)="<< Z->fourMomentum().Pt()/1e3 <<"  A="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(0)<<"  C="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(1)<<std::endl;
		  
		  //float aco = 1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi();
		  //if(Z->fourMomentum().Pt()/1e3 < 2.) std::cout<<"ZDC noaco activity, Aco="<< aco <<"  A="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(0)<<"  C="<<m_xAODEvent->m_zdcAnalysisTool->getCalibModuleSum(1)<<std::endl;	       
	       }

           if ( Z->type() == 22
	       && m_xAODEvent->m_goodPhotons.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (Z->photon1()->pt()/1e3) > 3. 
	       && (Z->photon2()->pt()/1e3) > 3.
	       && Z->fourMomentum().M()/1e3 > 6.
	       && m_xAODEvent->m_goodTracks.size() == 0
	       && (*sysListItr).name()=="" ){
	          if(Z->fourMomentum().Pt()/1e3 < 2.){
		   this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_CtrlNoAco"); 	       
	           h_npix_hits_2_LbLSel_CtrlNoAco->Fill(sum_pix);
		   
			float aco = 1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi();
	           if(!m_xAODEvent->m_isMC){
	           std::cout<<"noaco:"<<m_xAODEvent->m_eventInfo->eventNumber()
	           <<" run:"<<m_xAODEvent->m_eventInfo->runNumber()
		   <<" lbn:"<<m_xAODEvent->m_eventInfo->lumiBlock()
	           <<" mass:"<<Z->fourMomentum().M()/1e3
	           <<" pt:"<<Z->fourMomentum().Pt()/1e3
		   <<" aco:"<<aco
		   <<" pixel hits:"<<sum_pix
		   <<std::endl;	     
	           }
		   }
	       }


           if ( Z->type() == 22
	       && m_xAODEvent->m_goodPhotons.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (Z->photon1()->pt()/1e3) > 3. 
	       && (Z->photon2()->pt()/1e3) > 3.
	       && Z->fourMomentum().M()/1e3 > 6.
	       && m_xAODEvent->m_goodTracks.size() == 1
	       && (*sysListItr).name()=="" ){ 
		 this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_Ctrl1Trk"); 	       
	       	   std::cout<<"ntrk1 control"
		   <<" pixel hits:"<<sum_pix
		   <<std::endl;
	       }


           if ( Z->type() == 22
	       && m_xAODEvent->m_goodPhotons.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (Z->photon1()->pt()/1e3) > 3. 
	       && (Z->photon2()->pt()/1e3) > 3.
	       && Z->fourMomentum().M()/1e3 > 6.
	       && m_xAODEvent->m_goodTracks.size() == 2
	       && (*sysListItr).name()=="" ){
		 this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_Ctrl2Trk"); 	       
	         
		 
		 float aco = 1. - acos(cos(m_xAODEvent->m_goodTracks.at(0)->phi()-m_xAODEvent->m_goodTracks.at(1)->phi()))/TMath::Pi();
		 
	       	   if(!m_xAODEvent->m_isMC){
	           std::cout<<"2trk ee bkg event:"<<m_xAODEvent->m_eventInfo->eventNumber()
	           <<" run:"<<m_xAODEvent->m_eventInfo->runNumber()
		   <<" lbn:"<<m_xAODEvent->m_eventInfo->lumiBlock()
	           <<" mass:"<<Z->fourMomentum().M()/1e3
	           <<" pt:"<<Z->fourMomentum().Pt()/1e3
		   <<" pt_trk1:"<<m_xAODEvent->m_goodTracks.at(0)->pt()/1e3
		   <<" pt_trk2:"<<m_xAODEvent->m_goodTracks.at(1)->pt()/1e3
		   <<" aco_trk:"<<aco
		   <<std::endl;	     
	           }
	       
	       }


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

           if ( Z->type() == 22
	       && m_xAODEvent->m_goodPhotons.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (Z->photon1()->pt()/1e3) > 3. 
	       && (Z->photon2()->pt()/1e3) > 3.
	       && Z->fourMomentum().M()/1e3 > 6.
	       && m_xAODEvent->m_goodTracks.size() == 0
	       && (*sysListItr).name()=="" ){
	         
		 double dR_dist = sqrt( (Z->photon1()->phi() - Z->photon2()->phi())*(Z->photon1()->phi() - Z->photon2()->phi()) 
		 		      + (Z->photon1()->eta() - Z->photon2()->eta())*(Z->photon1()->eta() - Z->photon2()->eta()) );
		 
		 float aco = 1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi();
		 
		 if(aco<0.95){
		 
		 bool passF1 = ( (Z->photon1()->showerShapeValue(xAOD::EgammaParameters::f1)>0.1) && (Z->photon2()->showerShapeValue(xAOD::EgammaParameters::f1)>0.1) );
		 bool passWeta2 = ( (Z->photon1()->showerShapeValue(xAOD::EgammaParameters::weta2)<0.014) && (Z->photon2()->showerShapeValue(xAOD::EgammaParameters::weta2)<0.014) );
		 bool passEratio = ( (Z->photon1()->showerShapeValue(xAOD::EgammaParameters::Eratio)>0.6) && (Z->photon2()->showerShapeValue(xAOD::EgammaParameters::Eratio)>0.6) );
		 bool passF1up = ( (Z->photon1()->showerShapeValue(xAOD::EgammaParameters::f1)<0.8) && (Z->photon2()->showerShapeValue(xAOD::EgammaParameters::f1)<0.8) );
	         bool failF1 = ( (Z->photon1()->showerShapeValue(xAOD::EgammaParameters::f1)<0.1) || (Z->photon2()->showerShapeValue(xAOD::EgammaParameters::f1)<0.1) );
		 
		 if(passF1) this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_1PhotonF1Cut"); 	 
		 if(passF1 && passWeta2) this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_2PhotonWeta2Cut"); 
		 if(passF1 && passWeta2 && passEratio) this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_3PhotonEratioCut");   
		 if(passF1 && passWeta2 && passEratio && passF1up) this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_4PhotonF1UpCut"); 
		 
		 if(failF1 && passWeta2 && passEratio) this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_4PhotonF1InvertedCut");
		 
		 if(passF1 &&  passWeta2 && passEratio && passF1up && (Z->fourMomentum().Pt()/1e3 > 2.)){
		   this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_5PhotonF1UpPtCut"); }
		 
		 this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_0dRCut");
		 if(Z->fourMomentum().Pt()/1e3 > 2.) this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_0dRPtCut");
		 
		 if(Z->fourMomentum().Pt()/1e3 > 2. && Z->fourMomentum().Pt()/1e3 < 3.) 
		   this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_0dRPt23CutTemplate");
		 if(Z->fourMomentum().Pt()/1e3 > 3. && Z->fourMomentum().Pt()/1e3 < 4.) 
		   this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_0dRPt34CutTemplate");
		 if(Z->fourMomentum().Pt()/1e3 > 4. && Z->fourMomentum().Pt()/1e3 < 5.) 
		   this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "2_LbLSel_0dRPt45CutTemplate");  
		     
	         }
	       }


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



           if ( Z->type() == 1122
	       && m_xAODEvent-> m_goodPhotons.size() == 1 && m_xAODEvent-> m_goodElectrons.size() == 1
	       && m_xAODEvent-> m_goodTracks.size() == 2
               && (m_xAODEvent->m_goodTracks.at(0)->pt()/1e3 < 2. || m_xAODEvent->m_goodTracks.at(1)->pt()/1e3 < 2.)
	       && m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 > 5.
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (*sysListItr).name()=="" ){
	       this->fillHistos(E, m_xAODEvent->m_eventInfo, m_xAODEvent->weight, "1Ele_1Photon"); 
     	       
	       }

           if ( m_xAODEvent->m_goodPhotons.size() >= 1
	       //&& m_xAODEvent-> m_goodElectrons.size() == 0
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (*sysListItr).name()=="" ){

		  h_EtSinglePhoton -> Fill ( m_xAODEvent->m_goodPhotons.at(0)->pt()/1e3, m_xAODEvent->weight );

		
	       }


           if ( m_xAODEvent->m_goodPhotons.size() == 1
	       && m_xAODEvent-> m_goodElectrons.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && (*sysListItr).name()=="" ){
 	       
	       TLorentzVector el1, el2, el3;
      		el1.SetPtEtaPhiM(m_xAODEvent->m_goodPhotons.at(0)->pt(),  m_xAODEvent->m_goodPhotons.at(0)->eta(),
		       m_xAODEvent->m_goodPhotons.at(0)->phi(), m_xAODEvent->m_goodPhotons.at(0)->m()); 
		       
      		el2.SetPtEtaPhiM(m_xAODEvent->m_goodElectrons.at(1)->pt(),  m_xAODEvent->m_goodElectrons.at(1)->eta(),
		       m_xAODEvent->m_goodElectrons.at(1)->phi(), m_xAODEvent->m_goodElectrons.at(1)->m()); 
		       
      		el3.SetPtEtaPhiM(m_xAODEvent->m_goodElectrons.at(0)->pt(),  m_xAODEvent->m_goodElectrons.at(0)->eta(),
		       m_xAODEvent->m_goodElectrons.at(0)->phi(), m_xAODEvent->m_goodElectrons.at(0)->m()); 
      		//el4.SetPtEtaPhiM(m_xAODEvent->m_goodPhotons.at(3)->pt(),  m_xAODEvent->m_goodPhotons.at(3)->eta(),
		//       m_xAODEvent->m_goodPhotons.at(3)->phi(), m_xAODEvent->m_goodPhotons.at(3)->m()); 
      		TLorentzVector Zee = el1+el2+el3;
		TLorentzVector Ztrk = el2+el3;
	        
		h_4gMass ->Fill(Zee.M()/1e3);
		
		if (Zee.Pt()/1e3 < 2. ){
		  
		  h_2trkMass ->Fill(Zee.M()/1e3);
		}
		
	       }
       
	       
////////////////////////////////////////////////////////////////////////////////////////////////////////////	       

           if (   m_xAODEvent->m_goodElectrons.size()>=1 && m_xAODEvent->m_goodElectrons.size()<=2
	       && m_xAODEvent-> m_goodTracks.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200	       
	       && m_xAODEvent->m_goodTracks.at(0)->pt()/1e3 > 2.
	       && m_xAODEvent->m_goodTracks.at(1)->pt()/1e3 > 2.	       
	       && (*sysListItr).name()=="" ){
	          
		  float aco = 1. - acos(cos(m_xAODEvent->m_goodTracks.at(0)->phi()-m_xAODEvent->m_goodTracks.at(1)->phi()))/TMath::Pi();
		  
		  uint8_t pix1;
   		  m_xAODEvent-> m_goodTracks.at(0)->summaryValue(pix1,xAOD::numberOfPixelHits);
   		  uint8_t pix2;
   		  m_xAODEvent-> m_goodTracks.at(1)->summaryValue(pix2,xAOD::numberOfPixelHits); 
		  
	          if(aco<0.2 /*&& (pix1+pix2)<=8*/){
		  
		  double phi_trk = m_xAODEvent->m_goodTracks.at(0)->phi();
		  double eta_trk = m_xAODEvent->m_goodTracks.at(0)->eta();

		  double phi_el = m_xAODEvent->m_goodElectrons.at(0)->phi();
		  double eta_el = m_xAODEvent->m_goodElectrons.at(0)->eta();

		  double lep_dist = sqrt( (phi_trk - phi_el)*(phi_trk - phi_el) + (eta_trk - eta_el)*(eta_trk - eta_el) );
                  if ( lep_dist < 1.5 ){
		     h_ElePtTag -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3, EleTagPtWeight(m_xAODEvent->m_goodElectrons.at(0)) );
		     h_EleEtaTag -> Fill ( m_xAODEvent->m_goodTracks.at(1)->eta(), EleTagPtWeight(m_xAODEvent->m_goodElectrons.at(0)) );
		     
		     if( fabs(m_xAODEvent->m_goodTracks.at(1)->eta())<0.7 ) 
		       h_ElePtTagB -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 );
		     if( fabs(m_xAODEvent->m_goodTracks.at(1)->eta())>0.7 && fabs(m_xAODEvent->m_goodTracks.at(1)->eta())<1.37 ) 
		       h_ElePtTagBEC -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3  );
		     if( fabs(m_xAODEvent->m_goodTracks.at(1)->eta())>1.37 ) 
		       h_ElePtTagEC -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3  );
		  }   
		  else {
		     h_ElePtTag -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 );
		     h_EleEtaTag -> Fill ( m_xAODEvent->m_goodTracks.at(0)->eta() );
		     
		     if( fabs(m_xAODEvent->m_goodTracks.at(0)->eta())<0.7  ) 
		       h_ElePtTagB -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3  );
		     if( fabs(m_xAODEvent->m_goodTracks.at(0)->eta())>0.7 && fabs(m_xAODEvent->m_goodTracks.at(0)->eta())<1.37 ) 
		       h_ElePtTagBEC -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3  );
		     if( fabs(m_xAODEvent->m_goodTracks.at(0)->eta())>1.37 ) 
		       h_ElePtTagEC -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3  );		  
		  }
		  //h_ElePtTag -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 );
		   
		 }		  
	       }

           if (   m_xAODEvent->m_goodElectrons.size()==2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && m_xAODEvent-> m_goodTracks.size() == 2
	       && m_xAODEvent->m_goodTracks.at(0)->pt()/1e3 > 2.
	       && m_xAODEvent->m_goodTracks.at(1)->pt()/1e3 > 2.
	       && (*sysListItr).name()=="" ){
	       
		  float aco = 1. - acos(cos(m_xAODEvent->m_goodTracks.at(0)->phi()-m_xAODEvent->m_goodTracks.at(1)->phi()))/TMath::Pi();
		  
		  uint8_t pix1;
   		  m_xAODEvent-> m_goodTracks.at(0)->summaryValue(pix1,xAOD::numberOfPixelHits);
   		  uint8_t pix2;
   		  m_xAODEvent-> m_goodTracks.at(1)->summaryValue(pix2,xAOD::numberOfPixelHits); 
		  
	          if(aco<0.2 /*&& (pix1+pix2)<=8*/){
		  
		  double phi_trk = m_xAODEvent->m_goodTracks.at(0)->phi();
		  double eta_trk = m_xAODEvent->m_goodTracks.at(0)->eta();

		  double phi_el = m_xAODEvent->m_goodElectrons.at(0)->phi();
		  double eta_el = m_xAODEvent->m_goodElectrons.at(0)->eta();

		  double lep_dist = sqrt( (phi_trk - phi_el)*(phi_trk - phi_el) + (eta_trk - eta_el)*(eta_trk - eta_el) );
                  if ( lep_dist < 1.5 ){
		     h_ElePtProbe -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3, EleTagPtWeight(m_xAODEvent->m_goodElectrons.at(0)) );
		     h_EleEtaProbe -> Fill ( m_xAODEvent->m_goodTracks.at(1)->eta(), EleTagPtWeight(m_xAODEvent->m_goodElectrons.at(0)) );
		     
		     if( fabs(m_xAODEvent->m_goodTracks.at(1)->eta())<0.7 ) 
		       h_ElePtProbeB -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 );
		     if( fabs(m_xAODEvent->m_goodTracks.at(1)->eta())>0.7 && fabs(m_xAODEvent->m_goodTracks.at(1)->eta())<1.37 ) 
		       h_ElePtProbeBEC -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 );	
		     if( fabs(m_xAODEvent->m_goodTracks.at(1)->eta())>1.37  ) 
		       h_ElePtProbeEC -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 );		  
		  }
		  else{		  
		     h_ElePtProbe -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 );
		     h_EleEtaProbe -> Fill ( m_xAODEvent->m_goodTracks.at(0)->eta() );
		     
		     if( fabs(m_xAODEvent->m_goodTracks.at(0)->eta())<0.7  ) 
		       h_ElePtProbeB -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 );
		     if( fabs(m_xAODEvent->m_goodTracks.at(0)->eta())>0.7 && fabs(m_xAODEvent->m_goodTracks.at(0)->eta())<1.37 ) 
		       h_ElePtProbeBEC -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 );	
		     if( fabs(m_xAODEvent->m_goodTracks.at(0)->eta())>1.37  ) 
		       h_ElePtProbeEC -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 );		  
		  }
		 //h_ElePtProbe -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 );
		   		   
		  }		  
	       }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    	   const xAOD::ElectronContainer* electronsNoID = 0;
           ANA_CHECK(evtStore()->retrieve( electronsNoID, "Electrons" )); 

           if (   m_xAODEvent->m_goodElectrons.size()==1  && electronsNoID->size() == 1
	       && m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 > 5.
	       && m_xAODEvent-> m_goodTracks.size() == 2
	       
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200	
	       //&& (m_xAODEvent->m_eventFiredHLT_mb_sptrk_ion_L1ZDC_A_C_VTE50 || m_xAODEvent->m_eventFiredHLT_mb_sptrk_vetombts2in_L1ZDC_A_C_VTE50)   
	           
	       //&& m_xAODEvent->m_goodTracks.at(0)->pt()/1e3 > 2.5 
	       //&& m_xAODEvent->m_goodTracks.at(1)->pt()/1e3 > 2.5 
	       && fabs(m_xAODEvent->m_goodTracks.at(0)->eta())<2.45
	       && fabs(m_xAODEvent->m_goodTracks.at(1)->eta())<2.45	
	       && (fabs(m_xAODEvent->m_goodTracks.at(0)->eta())<1.35 || fabs(m_xAODEvent->m_goodTracks.at(0)->eta())>1.55)
	       && (fabs(m_xAODEvent->m_goodTracks.at(1)->eta())<1.35 || fabs(m_xAODEvent->m_goodTracks.at(1)->eta())>1.55)
	       && (1. - acos(cos(m_xAODEvent->m_goodTracks.at(0)->phi() - m_xAODEvent->m_goodTracks.at(1)->phi()))/TMath::Pi())<0.3
	       && (*sysListItr).name()=="" ){
	          
		  
		
		float aco_trk0 = 1. - acos(cos(m_xAODEvent->m_goodElectrons.at(0)->phi() - m_xAODEvent->m_goodTracks.at(0)->phi()))/TMath::Pi();		
		float aco_trk1 = 1. - acos(cos(m_xAODEvent->m_goodElectrons.at(0)->phi() - m_xAODEvent->m_goodTracks.at(1)->phi()))/TMath::Pi();

      		double g_dist0 = sqrt( (m_xAODEvent->m_goodElectrons.at(0)->phi() - m_xAODEvent->m_goodTracks.at(0)->phi())*(m_xAODEvent->m_goodElectrons.at(0)->phi() - m_xAODEvent->m_goodTracks.at(0)->phi())
      		       		+  (m_xAODEvent->m_goodElectrons.at(0)->eta() - m_xAODEvent->m_goodTracks.at(0)->eta())*(m_xAODEvent->m_goodElectrons.at(0)->eta() - m_xAODEvent->m_goodTracks.at(0)->eta()) );

      		double g_dist1 = sqrt( (m_xAODEvent->m_goodElectrons.at(0)->phi() - m_xAODEvent->m_goodTracks.at(1)->phi())*(m_xAODEvent->m_goodElectrons.at(0)->phi() - m_xAODEvent->m_goodTracks.at(1)->phi())
      		       		+  (m_xAODEvent->m_goodElectrons.at(0)->eta() - m_xAODEvent->m_goodTracks.at(1)->eta())*(m_xAODEvent->m_goodElectrons.at(0)->eta() - m_xAODEvent->m_goodTracks.at(1)->eta()) );
      
		   
		TLorentzVector e1, trk2, g2;
      		e1.SetPtEtaPhiM(m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3,  m_xAODEvent->m_goodElectrons.at(0)->eta(),
		       m_xAODEvent->m_goodElectrons.at(0)->phi(), m_xAODEvent->m_goodElectrons.at(0)->m()); 

		       		
		float ptdiff = -100.;
		if( (g_dist0 < 1.0) && (m_xAODEvent->m_goodTracks.at(1)->pt()/1e3 < 2.) ) {
		   ptdiff = (m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 -m_xAODEvent->m_goodTracks.at(1)->pt()/1e3 );
		   		       
      		   trk2.SetPtEtaPhiM(m_xAODEvent->m_goodTracks.at(1)->pt()/1e3,  m_xAODEvent->m_goodTracks.at(1)->eta(),
		       m_xAODEvent->m_goodTracks.at(1)->phi(), 0.); 
		   }
		else if( (g_dist1 < 1.0) && (m_xAODEvent->m_goodTracks.at(0)->pt()/1e3 < 2.) ) {
		   ptdiff = (m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 -m_xAODEvent->m_goodTracks.at(0)->pt()/1e3 );
		   
      		   trk2.SetPtEtaPhiM(m_xAODEvent->m_goodTracks.at(0)->pt()/1e3,  m_xAODEvent->m_goodTracks.at(0)->eta(),
		       m_xAODEvent->m_goodTracks.at(0)->phi(), 0.); 		   
		   }
		
		h_EleTrkTag -> Fill ( ptdiff  );
		
		h_EleTrkPt ->Fill(m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 - ptdiff);
		
		if(m_xAODEvent->m_goodPhotons.size() == 1 /*m_xAODEvent->m_caloClusters.size() == 2*/){
		   
		   h_EleTrkProbe -> Fill ( ptdiff );
		   
		   /*
		   float g_dist3 = sqrt( (m_xAODEvent->m_goodElectrons.at(0)->caloCluster()->phi() - m_xAODEvent->m_caloClusters.at(0)->phi())
		   		  *(m_xAODEvent->m_goodElectrons.at(0)->caloCluster()->phi() - m_xAODEvent->m_caloClusters.at(0)->phi())
      		       		+  (m_xAODEvent->m_goodElectrons.at(0)->caloCluster()->eta() - m_xAODEvent->m_caloClusters.at(0)->eta())
				  *(m_xAODEvent->m_goodElectrons.at(0)->caloCluster()->eta() - m_xAODEvent->m_caloClusters.at(0)->eta()) );
		   
		   if(g_dist3 < 1.0){
		     h_EleGamPt ->Fill(m_xAODEvent->m_caloClusters.at(1)->pt()/1e3);
      		     g2.SetPtEtaPhiM(m_xAODEvent->m_caloClusters.at(1)->pt()/1e3,  m_xAODEvent->m_caloClusters.at(1)->eta(),
		       m_xAODEvent->m_caloClusters.at(1)->phi(), m_xAODEvent->m_caloClusters.at(1)->m()); 
		   }
		   else{
		     h_EleGamPt ->Fill(m_xAODEvent->m_caloClusters.at(0)->pt()/1e3);
      		     g2.SetPtEtaPhiM(m_xAODEvent->m_caloClusters.at(0)->pt()/1e3,  m_xAODEvent->m_caloClusters.at(0)->eta(),
		       m_xAODEvent->m_caloClusters.at(0)->phi(), m_xAODEvent->m_caloClusters.at(0)->m()); 
		   }
		   */
		   
		     h_EleGamPt ->Fill(m_xAODEvent->m_goodPhotons.at(0)->pt()/1e3);
		     h_EleGamEta ->Fill(m_xAODEvent->m_goodPhotons.at(0)->eta());
		     
		     if(m_xAODEvent->m_goodPhotons.at(0)->pt()/1e3>3.) h_EleGamPt3 ->Fill(m_xAODEvent->m_goodPhotons.at(0)->pt()/1e3);
	                 
		     bool passF1 = ( (m_xAODEvent->m_goodPhotons.at(0)->showerShapeValue(xAOD::EgammaParameters::f1)>0.1) );
        	     bool passWeta2 = ( (m_xAODEvent->m_goodPhotons.at(0)->showerShapeValue(xAOD::EgammaParameters::weta2)<0.014) );
                     bool passEratio = ( (m_xAODEvent->m_goodPhotons.at(0)->showerShapeValue(xAOD::EgammaParameters::Eratio)>0.6) );
               	     bool passF1up = ( (m_xAODEvent->m_goodPhotons.at(0)->showerShapeValue(xAOD::EgammaParameters::f1)<0.8) );
		     
		     //if(m_xAODEvent->m_goodPhotons.at(0)->pt()/1e3>3. &&
		     //   passF1 && passWeta2 && passEratio && passF1up) h_EleGamPt3ID ->Fill(m_xAODEvent->m_goodPhotons.at(0)->pt()/1e3);
		     
		     if(m_xAODEvent->m_goodPhotons.at(0)->pt()/1e3>3. ){
		     
		      	double ph_eta =  m_xAODEvent->m_goodPhotons.at(0)->caloCluster()->etaBE(2);
  			double ph_weta2 =  m_xAODEvent->m_goodPhotons.at(0)->showerShapeValue(xAOD::EgammaParameters::weta2);
			double ph_eratio =  m_xAODEvent->m_goodPhotons.at(0)->showerShapeValue(xAOD::EgammaParameters::Eratio);
			double ph_f1 =  m_xAODEvent->m_goodPhotons.at(0)->showerShapeValue(xAOD::EgammaParameters::f1);

			//eta bins
			if( fabs( ph_eta ) < 0.6 ){
			  if( (ph_f1 < 0.8) and (ph_f1 > 0.0452) and (ph_weta2 < 0.0162) and (ph_eratio > 0.503) ) h_EleGamPt3ID ->Fill(m_xAODEvent->m_goodPhotons.at(0)->pt()/1e3);
			} 
			else if ( fabs(ph_eta)>=0.6 and fabs(ph_eta)< 1.37 ){
			  if( (ph_f1 < 0.8) and (ph_f1 > 0.0368) and (ph_weta2 < 0.0157) and (ph_eratio > 0.140) ) h_EleGamPt3ID ->Fill(m_xAODEvent->m_goodPhotons.at(0)->pt()/1e3);
			}
			else if ( fabs(ph_eta)>=1.52 and fabs(ph_eta)< 1.81 ){
			  if( (ph_f1 < 0.8) and (ph_f1 > 0.0783) and (ph_weta2 < 0.0156) and (ph_eratio > 0.195) ) h_EleGamPt3ID ->Fill(m_xAODEvent->m_goodPhotons.at(0)->pt()/1e3);
			}
			else if ( fabs(ph_eta)>=1.81 and fabs(ph_eta)< 2.37 ){
			  if( (ph_f1 < 0.8) and (ph_f1 > -0.00663) and (ph_weta2 < 0.0146) and (ph_eratio > 0.837) ) h_EleGamPt3ID ->Fill(m_xAODEvent->m_goodPhotons.at(0)->pt()/1e3);
			}
		     
		     }
		     
      		     g2.SetPtEtaPhiM(m_xAODEvent->m_goodPhotons.at(0)->pt()/1e3,  m_xAODEvent->m_goodPhotons.at(0)->eta(),
		       m_xAODEvent->m_goodPhotons.at(0)->phi(), m_xAODEvent->m_goodPhotons.at(0)->m()); 
		       		   
		   TLorentzVector tot = e1+trk2+g2;
		   h_ElePtllg->Fill(tot.Pt());
		   
		   double dr0 = sqrt( (g2.Phi() - trk2.Phi())*(g2.Phi() - trk2.Phi()) + (g2.Eta() - trk2.Eta())*(g2.Eta() - trk2.Eta()) );
		   h_EleGamDR ->Fill(dr0);
		}
		  
		/*const xAOD::CaloClusterContainer* clus = 0;
  		m_xAODEvent->m_event->retrieve( clus, "egammaClusters" ).ignore();
  		xAOD::CaloClusterContainer::const_iterator cl_itr = clus->begin();
  		xAOD::CaloClusterContainer::const_iterator cl_end = clus->end();

  		for (; cl_itr != cl_end; ++cl_itr)
  		{    
          	        //float aco_cl = 1. - acos(cos(m_xAODEvent->m_goodElectrons.at(0)->phi() - (*cl_itr)->phi()))/TMath::Pi();

      			double g_dist0 = sqrt( ((*cl_itr)->phi() - m_xAODEvent->m_goodTracks.at(0)->phi())*((*cl_itr)->phi() - m_xAODEvent->m_goodTracks.at(0)->phi())
      		       		+  ((*cl_itr)->eta() - m_xAODEvent->m_goodTracks.at(0)->eta())*((*cl_itr)->eta() - m_xAODEvent->m_goodTracks.at(0)->eta()) );

      			double g_dist1 = sqrt( ((*cl_itr)->phi() - m_xAODEvent->m_goodTracks.at(1)->phi())*((*cl_itr)->phi() - m_xAODEvent->m_goodTracks.at(1)->phi())
      		       		+  ((*cl_itr)->eta() - m_xAODEvent->m_goodTracks.at(1)->eta())*((*cl_itr)->eta() - m_xAODEvent->m_goodTracks.at(1)->eta()) );
      
      			if (g_dist0 < 0.6 && aco_trk0 > 0.5){
			   h_EleTrkProbe -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 );
			   break;
			}

      			if (g_dist1 < 0.6 && aco_trk0 < 0.5){
			   h_EleTrkProbe -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 );
			   break;
			}

		  
  		}*/
			  
	       }
	       
	       

           /*if (   m_xAODEvent->m_goodElectrons.size()==2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && m_xAODEvent-> m_goodTracks.size() == 2
	       && m_xAODEvent->m_goodTracks.at(0)->pt()/1e3 > 2.5
	       //&& m_xAODEvent->m_goodTracks.at(1)->pt()/1e3 > 2.5
	       && (*sysListItr).name()=="" ){
	       
		  float aco = 1. - acos(cos(m_xAODEvent->m_goodTracks.at(0)->phi()-m_xAODEvent->m_goodTracks.at(1)->phi()))/TMath::Pi();
		  
	          if(aco<0.2)  h_EleTrkProbe -> Fill ( m_xAODEvent->m_goodElectrons.at(0)->pt()/1e3 );		   
		  
	       }*/	
	              
/////////////////////////////////////////////////////////////////////////////////////	   


	   if (   m_xAODEvent->m_goodElectrons.size() == 2
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && m_xAODEvent-> m_goodTracks.size() <= 9
	       && (*sysListItr).name()=="" ){	           
		   float aco = 1. - acos(cos(m_xAODEvent->m_goodElectrons.at(0)->caloCluster()->phi()-m_xAODEvent->m_goodElectrons.at(1)->caloCluster()->phi()))/TMath::Pi();		   
		   
		   h_AcoEG2->Fill(aco);
		   
		   if(aco<0.2){
		    h_EGMisID2 -> Fill ( m_xAODEvent->m_goodElectrons.at(1)->caloCluster()->et()/1e3, m_xAODEvent->weight );	
		    h_Ntrk2 -> Fill ( m_xAODEvent-> m_goodTracks.size(), m_xAODEvent->weight );

		    TLorentzVector el1, el2;
      		    el1.SetPtEtaPhiM(m_xAODEvent->m_goodElectrons.at(0)->caloCluster()->pt(),  m_xAODEvent->m_goodElectrons.at(0)->caloCluster()->eta(),
		       m_xAODEvent->m_goodElectrons.at(0)->caloCluster()->phi(), m_xAODEvent->m_goodElectrons.at(0)->m()); 
		       
      		    el2.SetPtEtaPhiM(m_xAODEvent->m_goodElectrons.at(1)->caloCluster()->pt(),  m_xAODEvent->m_goodElectrons.at(1)->caloCluster()->eta(),
		       m_xAODEvent->m_goodElectrons.at(1)->caloCluster()->phi(), m_xAODEvent->m_goodElectrons.at(1)->m()); 
		    
		    TLorentzVector Zee = el1+el2;
		    
		    h_PtEG2->Fill(Zee.Pt()/1e3);

		    }	   
	        }    

	   if (   m_xAODEvent->m_goodElectrons.size() == 1 && m_xAODEvent->m_goodPhotons.size() == 1
	       //&& m_xAODEvent->m_goodPhotons.at(0)->conversionType() == 0
	       && m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       && m_xAODEvent-> m_goodTracks.size() <=9
	       && (*sysListItr).name()=="" ){	           
		   float aco = 1. - acos(cos(m_xAODEvent->m_goodElectrons.at(0)->caloCluster()->phi()-m_xAODEvent->m_goodPhotons.at(0)->caloCluster()->phi()))/TMath::Pi();
		  
		  h_AcoEG->Fill(aco);

		   if(aco<0.2 ){

		    if(m_xAODEvent->m_goodPhotons.at(0)->author() & xAOD::EgammaParameters::AuthorPhoton) h_Author->Fill(0);
		    if(m_xAODEvent->m_goodPhotons.at(0)->author() & xAOD::EgammaParameters::AuthorAmbiguous) h_Author->Fill(1);
		    if(m_xAODEvent->m_goodPhotons.at(0)->author() & xAOD::EgammaParameters::AuthorCaloTopo35) h_Author->Fill(2);

		    h_EGMisID1 -> Fill ( m_xAODEvent->m_goodPhotons.at(0)->caloCluster()->et()/1e3, m_xAODEvent->weight );
		    h_Ntrk1 -> Fill ( m_xAODEvent-> m_goodTracks.size(), m_xAODEvent->weight );
		    
		    //if( trackIso(m_xAODEvent->m_goodPhotons.at(0), m_xAODEvent->m_goodTracks) ) 
		    if ( m_xAODEvent->m_goodPhotons.at(0)->passSelection("Tight") )  h_EGMisID1Iso -> Fill ( m_xAODEvent->m_goodPhotons.at(0)->caloCluster()->et()/1e3 );
		    
		    TLorentzVector el1, el2;
      		    el1.SetPtEtaPhiM(m_xAODEvent->m_goodPhotons.at(0)->caloCluster()->pt(),  m_xAODEvent->m_goodPhotons.at(0)->caloCluster()->eta(),
		       m_xAODEvent->m_goodPhotons.at(0)->caloCluster()->phi(), m_xAODEvent->m_goodPhotons.at(0)->m()); 
		       
      		    el2.SetPtEtaPhiM(m_xAODEvent->m_goodElectrons.at(0)->caloCluster()->pt(),  m_xAODEvent->m_goodElectrons.at(0)->caloCluster()->eta(),
		       m_xAODEvent->m_goodElectrons.at(0)->caloCluster()->phi(), 0.); 
		    
		    TLorentzVector Zee = el1+el2;
		    
		    h_PtEG->Fill(Zee.Pt()/1e3);
		    
		        for(unsigned int trki=0; trki < m_xAODEvent->m_goodTracks.size(); trki++)
    			{ 
   
      			float trk_pt = (m_xAODEvent->m_goodTracks.at(trki))->pt();
      			double g_dist = sqrt( (m_xAODEvent->m_goodPhotons.at(0)->phi() - m_xAODEvent->m_goodTracks.at(trki)->phi())*(m_xAODEvent->m_goodPhotons.at(0)->phi() - m_xAODEvent->m_goodTracks.at(trki)->phi())
      		       		+  (m_xAODEvent->m_goodPhotons.at(0)->eta() - m_xAODEvent->m_goodTracks.at(trki)->eta())*(m_xAODEvent->m_goodPhotons.at(0)->eta() - m_xAODEvent->m_goodTracks.at(trki)->eta()) );
      
      			if (g_dist < 1.0) h_trkPtEG->Fill(trk_pt/1e3);
   			}

		    
		    }		   
	        }  

           if ( //m_xAODEvent->m_eventFiredHLT_hi_gg_upc_L1TE5_VTE200
	       (m_xAODEvent->m_eventFiredHLT_mb_sptrk_ion_L1ZDC_A_C_VTE50 || m_xAODEvent->m_eventFiredHLT_mb_sptrk_vetombts2in_L1ZDC_A_C_VTE50)
	       && m_xAODEvent-> m_goodTracks.size() <=3
	       && (*sysListItr).name()=="" ){
	       
	       	const xAOD::CaloClusterContainer* clus = 0;
  		m_xAODEvent->m_event->retrieve( clus, "egammaClusters" ).ignore(); //
  		xAOD::CaloClusterContainer::const_iterator cl_itr = clus->begin();
  		xAOD::CaloClusterContainer::const_iterator cl_end = clus->end();

	       	const xAOD::CaloClusterContainer* clusTS = 0;
  		m_xAODEvent->m_event->retrieve( clusTS, "egammaTopoSeededClusters" ).ignore(); //
  		xAOD::CaloClusterContainer::const_iterator clTS_itr = clusTS->begin();
  		xAOD::CaloClusterContainer::const_iterator clTS_end = clusTS->end();
		
		int cl_n =0;
		float sum_Et = 0;
		float phi1 =0;
		float phi2 = 0;
  		for (; cl_itr != cl_end; ++cl_itr)
  		{    
		   if( (*cl_itr)->pt()/1e3 > 1.5 && fabs((*cl_itr)->etaBE(2)) < 2.47  
		        &&  (fabs( (*cl_itr)->etaBE(2) ) < 1.37 ||  fabs( (*cl_itr)->etaBE(2) ) > 1.52) ) {
		     cl_n ++;
		     sum_Et += (*cl_itr)->pt()/1e3;
		     
		     if(phi1==0) phi1 = (*cl_itr)->phi();
		     if(phi1!=0) phi2 = (*cl_itr)->phi();
		   }
	        }

  		for (; clTS_itr != clTS_end; ++clTS_itr)
  		{    
		   if( (*clTS_itr)->pt()/1e3 > 1.5 && fabs((*clTS_itr)->etaBE(2)) < 2.47  
		        &&  (fabs( (*clTS_itr)->etaBE(2) ) < 1.37 ||  fabs( (*clTS_itr)->etaBE(2) ) > 1.52) ) {
		     cl_n ++;
		     sum_Et += (*clTS_itr)->pt()/1e3;
		     
		     if(phi1==0) phi1 = (*clTS_itr)->phi();
		     if(phi1!=0) phi2 = (*clTS_itr)->phi();
		   }
	        }
	       
	        const xAOD::EnergySumRoI* esumroi(0);
   		m_xAODEvent->m_event->retrieve( esumroi, "LVL1EnergySumRoI" ).ignore();   
   		//std::cout<<"ESumROI : "<<esumroi->energyT()/1000.<<"\n";   
   		//if(esumroi->energyT()/1000. >= 5.) 
	       
	       float aco = 1. - acos(cos(phi1-phi2))/TMath::Pi();
	       //if( cl_n == 2) std::cout<<"DiCluster acoplanarity : "<<aco<<"\n"; 
	       
	       if( cl_n == 2 && aco < 0.2) h_TE5eff2->Fill(sum_Et);
	       if( cl_n == 2 && aco < 0.2 && esumroi->energyT()/1000. >= 5.) h_TE5eff1->Fill(sum_Et);
	   }

	      
	       
           delete Z;
	   delete E;
	   delete V; 
	 
    } 




 
    
    

   /////*************************************************************************///// 
      
  } // end for loop over systematics
    
  /////**********************************************///// 
   
   
  return StatusCode::SUCCESS;
}


/*****************************************************************/
double EXCLRecoAnalysis :: LeptScaleFactors (PairCandidate *m_Z) {
/*****************************************************************/

  if(!(m_xAODEvent->m_isMC)) return 1.;
  
  
  double Lep_SF = 1.;
  
  if (m_Z->type()==13 || m_Z->type()==1313) {
    /*Lep_SF *= m_xAODEvent->getMuonEfficiencySF(m_Z->muon1(),"medium");
    Lep_SF *= m_xAODEvent->getMuonEfficiencySF(m_Z->muon2(),"medium");

    Lep_SF *= m_xAODEvent->getMuonIsolationSF(m_Z->muon1(),"medium");
    Lep_SF *= m_xAODEvent->getMuonIsolationSF(m_Z->muon2(),"medium");  
	*/
  }

  if (m_Z->type()==11 || m_Z->type()==1111) {
    /*Lep_SF *= m_xAODEvent->getEleEfficiencySF(m_Z->electron1(),"medium");
    Lep_SF *= m_xAODEvent->getEleEfficiencySF(m_Z->electron2(),"medium");

    Lep_SF *= m_xAODEvent->getEleIsolationSF(m_Z->electron1(),"medium");
    Lep_SF *= m_xAODEvent->getEleIsolationSF(m_Z->electron2(),"medium");  
    */
  }  

  if (m_Z->type()==-1113 || m_Z->type()==1311) {
   /* Lep_SF *= m_xAODEvent->getEleEfficiencySF(m_Z->electron1(),"medium");
    Lep_SF *= m_xAODEvent->getMuonEfficiencySF(m_Z->muon1(),"medium");

    Lep_SF *= m_xAODEvent->getEleIsolationSF(m_Z->electron1(),"medium");
    Lep_SF *= m_xAODEvent->getMuonIsolationSF(m_Z->muon1(),"medium");  
    */
  } 
  
  return Lep_SF;
  
}


/*****************************************************************/
StatusCode EXCLRecoAnalysis :: postExecute () {
/*****************************************************************/

  return StatusCode::SUCCESS;
}



/*****************************************************************/
StatusCode EXCLRecoAnalysis :: finalize () {
/*****************************************************************/

  Info("finalize()", "Number of events with at least two leptons = %i", numforEXCLevents);
  
   
   
  return StatusCode::SUCCESS;
}



/*****************************************************************/
StatusCode EXCLRecoAnalysis :: histFinalize () {
/*****************************************************************/
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.

  return StatusCode::SUCCESS;
}



/*****************************************************************/
bool EXCLRecoAnalysis::trackIso(xAOD::Photon* goodPhoton, const std::vector<xAOD::TrackParticle*> goodTracks) { 
/*****************************************************************/

    for(unsigned int trki=0; trki < goodTracks.size(); trki++)
    { 
   
      float trk_pt = (goodTracks.at(trki))->pt();
      
      //if (trk_pt < 500.) continue;
      
      double g_dist = sqrt( (goodPhoton->phi() - goodTracks.at(trki)->phi())*(goodPhoton->phi() - goodTracks.at(trki)->phi())
      		       +  (goodPhoton->eta() - goodTracks.at(trki)->eta())*(goodPhoton->eta() - goodTracks.at(trki)->eta()) );
      
      if (g_dist < 0.6) return false;
    }
    
    return true;

}



/*****************************************************************/
PairCandidate *EXCLRecoAnalysis::findPairCandidates(const std::vector<xAOD::Electron*> goodElectrons,
  				const std::vector<xAOD::Muon*> goodMuons,
				const std::vector<xAOD::Photon*> goodPhotons,
				const std::vector<xAOD::TrackParticle*> goodTracks ) {
/*****************************************************************/


  
  
  // Leading and subleading electron pair
  unsigned int contSize_el = goodElectrons.size();
  
  if (contSize_el>=2){
  
      TLorentzVector el1, el2;
      el1.SetPtEtaPhiM(goodElectrons.at(0)->pt(),  goodElectrons.at(0)->eta(),
		       goodElectrons.at(0)->phi(), goodElectrons.at(0)->m()); 
      el2.SetPtEtaPhiM(goodElectrons.at(1)->pt(),  goodElectrons.at(1)->eta(),
		       goodElectrons.at(1)->phi(), goodElectrons.at(1)->m()); 
      TLorentzVector Z = el1+el2;
      //m_PairCandidates->push_back(new PairCandidate(Z, 11, goodElectrons.at(i), goodElectrons.at(j)));
      // Electrons must have opposite sign
      if ( goodElectrons.at(0)->charge()*goodElectrons.at(1)->charge() < 0 ) return new PairCandidate(Z, 11, goodElectrons.at(0), goodElectrons.at(1));
      else return new PairCandidate(Z, 1111, goodElectrons.at(0), goodElectrons.at(1));
    
  }
  
  // Leading and subleading Photon pair
  unsigned int contSize_g = goodPhotons.size();
  
  if (contSize_g>=2){

  xAOD::Photon* photon_1 = new xAOD::Photon();
  xAOD::Photon* photon_2 = new xAOD::Photon();
  *photon_1 = *goodPhotons.at(0);
  *photon_2 = *goodPhotons.at(1);   

/*
///////for sys check! ////////////////
   float pt1 = photon_1->pt()*(1.+gRandom->Gaus(0., 0.1));
   float pt2 = photon_2->pt()*(1.+gRandom->Gaus(0., 0.1));
     
      TLorentzVector g1, g2;
      g1.SetPtEtaPhiM(pt1,  goodPhotons.at(0)->eta(),
		       goodPhotons.at(0)->phi(), goodPhotons.at(0)->m()); 
      g2.SetPtEtaPhiM(pt2,  goodPhotons.at(1)->eta(),
		       goodPhotons.at(1)->phi(), goodPhotons.at(1)->m()); 
      TLorentzVector Z = g1+g2;
//////////////////////////////////////
*/
      TLorentzVector g1z, g2z;
      g1z.SetPtEtaPhiM(photon_1->pt(),  photon_1->eta(),
		       photon_1->phi(), photon_1->m()); 
      g2z.SetPtEtaPhiM(photon_2->pt(),  photon_2->eta(),
		       photon_2->phi(), photon_2->m()); 
      TLorentzVector Zz = g1z+g2z;
      
      
      //if(pt1/1e3>3. && pt2/1e3>3.) 
      
      return new PairCandidate(Zz, 22, photon_1, photon_2);
    
  }

  // Leading and subleading muon pair
  unsigned int contSize_mu = goodMuons.size();

  if (contSize_mu>=2){
  
      TLorentzVector mu1, mu2;
      mu1.SetPtEtaPhiM(goodMuons.at(0)->pt(),  goodMuons.at(0)->eta(),
		       goodMuons.at(0)->phi(), goodMuons.at(0)->m()); 
      mu2.SetPtEtaPhiM(goodMuons.at(1)->pt(),  goodMuons.at(1)->eta(),
		       goodMuons.at(1)->phi(), goodMuons.at(1)->m()); 
      TLorentzVector Z = mu1+mu2;

      //m_ZCandidates->push_back(new ZCandidate(Z, 13, goodMuons.at(i_mu), goodMuons.at(j_mu)));
      //Muons must have opposite sign
      if ( goodMuons.at(0)->charge()*goodMuons.at(1)->charge() < 0 ) return new PairCandidate(Z, 13, goodMuons.at(0), goodMuons.at(1));
      else return new PairCandidate(Z, 1313, goodMuons.at(0), goodMuons.at(1));

  }  


  if (contSize_g == 1 && contSize_el == 1){

  xAOD::Photon* photon_1 = new xAOD::Photon();
  *photon_1 = *goodPhotons.at(0);  

      TLorentzVector g1z, g2z;
      g1z.SetPtEtaPhiM(photon_1->pt(),  photon_1->eta(),
		       photon_1->phi(), photon_1->m()); 
      g2z.SetPtEtaPhiM(goodElectrons.at(0)->pt(),  goodElectrons.at(0)->eta(),
		       goodElectrons.at(0)->phi(), goodElectrons.at(0)->m()); 
      TLorentzVector Zz = g1z+g2z;

      return new PairCandidate(Zz, 1122, goodElectrons.at(0), photon_1);
    
  }
 

  return new PairCandidate();

}


/*****************************************************************/
void EXCLRecoAnalysis::bookHistos(TString SystematicName){
/*****************************************************************/

  std::map<TString, TH1*> hm_gg = m_Analysis_gg.bookHistos(SystematicName);
  for (std::map<TString, TH1*>::iterator it = hm_gg.begin(); it != hm_gg.end(); ++it){
    it->second->Sumw2();
    wk()->addOutput(it->second); 
  }

  std::map<TString, TH1*> hm_eeOS = m_Analysis_eeOS.bookHistos(SystematicName);
  for (std::map<TString, TH1*>::iterator it = hm_eeOS.begin(); it != hm_eeOS.end(); ++it){
    it->second->Sumw2();
    wk()->addOutput(it->second); 
  }

/*  std::map<TString, TH1*> hm_eeSS = m_Analysis_eeSS.bookHistos(SystematicName);
  for (std::map<TString, TH1*>::iterator it = hm_eeSS.begin(); it != hm_eeSS.end(); ++it){
    it->second->Sumw2();
    wk()->addOutput(it->second); 
  }
*/
/*  std::map<TString, TH1*> hm_mmOS = m_Analysis_mmOS.bookHistos(SystematicName);
  for (std::map<TString, TH1*>::iterator it = hm_mmOS.begin(); it != hm_mmOS.end(); ++it){
    it->second->Sumw2();
    wk()->addOutput(it->second); 
  }


  std::map<TString, TH1*> hm_mmSS = m_Analysis_mmSS.bookHistos(SystematicName);
  for (std::map<TString, TH1*>::iterator it = hm_mmSS.begin(); it != hm_mmSS.end(); ++it){
    it->second->Sumw2();
    wk()->addOutput(it->second); 
  }
*/  

  Histogram_map.insert(hm_gg.begin(), hm_gg.end());
 // Histogram_map.insert(hm_gg.begin(), hm_gg.end());
  
  //Histogram_map.insert(hm_eeSS.begin(), hm_eeSS.end());
  Histogram_map.insert(hm_eeOS.begin(), hm_eeOS.end());

 // Histogram_map.insert(hm_mmSS.begin(), hm_mmSS.end());
 // Histogram_map.insert(hm_mmOS.begin(), hm_mmOS.end());

//  Histogram_map.insert(hm_emuSS.begin(), hm_emuSS.end());
 // Histogram_map.insert(hm_emuOS.begin(), hm_emuOS.end());
  

}  

/*****************************************************************/
void EXCLRecoAnalysis :: fillHistos(const EXCLCandidate* E, const xAOD::EventInfo* eventInfo, float weight, TString SystematicName){
/*****************************************************************/

  if (E->Paircandidate()->type()==22) m_Analysis_gg.fillHistos(E, eventInfo, weight, Histogram_map, SystematicName);
  
  //if (E->Paircandidate()->type()==1313) m_Analysis_mmSS.fillHistos(E, eventInfo, weight, Histogram_map, SystematicName);
  
  // changed to cover egamma pairs
//  if (E->Paircandidate()->type()==1122) m_Analysis_eeSS.fillHistos(E, eventInfo, weight, Histogram_map, SystematicName); 
  if (E->Paircandidate()->type()==11) m_Analysis_eeOS.fillHistos(E, eventInfo, weight, Histogram_map, SystematicName);
  
  //if (E->Paircandidate()->type()==13) m_Analysis_mmOS.fillHistos(E, eventInfo, weight, Histogram_map, SystematicName);
//  if (E->Paircandidate()->type()==-1113) m_Analysis_emuOS.fillHistos(E, eventInfo, weight, Histogram_map, SystematicName);
//  if (E->Paircandidate()->type()==1311) m_Analysis_emuSS.fillHistos(E, eventInfo, weight, Histogram_map, SystematicName);

}


/*****************************************************************/
void EXCLRecoAnalysis :: fillNoCutsHistos(const xAOD::EventInfo* eventInfo, TString SystematicName){
/*****************************************************************/
//...  

}

/*****************************************************************/
double EXCLRecoAnalysis :: EleEffTrkPtSF(xAOD::Electron* goodEle){
/*****************************************************************/
  
  if(!(m_xAODEvent->m_isMC)) return 1.;
  
  if(fabs(goodEle->eta()) < 1.37 && goodEle->pt()/1e3>2. && goodEle->pt()/1e3<4.) return 1.0; 
  else if(fabs(goodEle->eta()) < 1.37 && goodEle->pt()/1e3>4. && goodEle->pt()/1e3<6.) return 1.68; 
  else if(fabs(goodEle->eta()) < 1.37 && goodEle->pt()/1e3>6. && goodEle->pt()/1e3<8.) return 1.43;
  else if(fabs(goodEle->eta()) < 1.37 && goodEle->pt()/1e3>8. && goodEle->pt()/1e3<10.) return 1.35; 
  else if(fabs(goodEle->eta()) < 1.37 && goodEle->pt()/1e3>10. && goodEle->pt()/1e3<12.) return 1.05;
  

  else return 1.;
  
}

/*****************************************************************/
double EXCLRecoAnalysis :: EleEffTrkPtSF(xAOD::TrackParticle *goodTrk){
/*****************************************************************/
  
  if(fabs(goodTrk->eta()) > 1.37) return 1.;
  
  if(goodTrk->pt()/1e3 <2.5) return 1.79266; 
  else if(goodTrk->pt()/1e3 <3.0) return 2.04811; 
  else if(goodTrk->pt()/1e3 <3.5) return 1.41299; 
  else if(goodTrk->pt()/1e3 <4.0) return 1.30555; 
  else if(goodTrk->pt()/1e3 <4.5) return 1.13552; 
  else if(goodTrk->pt()/1e3 <5.0) return 1.12085;
  else return 1.;
  
}

/*****************************************************************/
double EXCLRecoAnalysis :: EleTagPtWeight(xAOD::Electron* goodEle){
/*****************************************************************/
  
  if(!(m_xAODEvent->m_isMC)) return 1.;
  
  if(goodEle->pt()/1e3>2. && goodEle->pt()/1e3<4.) return 0.45; 
  else if(goodEle->pt()/1e3>4. && goodEle->pt()/1e3<5.) return 1.22; 
  else if(goodEle->pt()/1e3>5. && goodEle->pt()/1e3<6.) return 1.54; 
  else if(goodEle->pt()/1e3>6. && goodEle->pt()/1e3<7.) return 1.48; 
  else if(goodEle->pt()/1e3>7. && goodEle->pt()/1e3<8.) return 1.33; 
  else if(goodEle->pt()/1e3>8. && goodEle->pt()/1e3<9.) return 1.28;
  else if(goodEle->pt()/1e3>9. && goodEle->pt()/1e3<10.) return 1.1;

  

  else return 1.;
  
}

/*****************************************************************/
bool EXCLRecoAnalysis::eventFiredHLT_hi_gg_upc_L1TE5_VTE200(void){
/*****************************************************************/
    
    if(m_isMC) return true;
    
    const Trig::ChainGroup* cg = m_trigDecisionTool->getChainGroup("HLT_hi_gg_upc_L1TE5_VTE200");
    if( cg->isPassed() ) return true; 
    else return false;
    
}

/*****************************************************************/
bool EXCLRecoAnalysis::eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_noiseSup_L1TEX_VTEY(void){
/*****************************************************************/
    
    if(m_isMC) return true;
    
    const Trig::ChainGroup* cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_hi_gg_upc_noiseSup_L1TE4_VTE200");
    if( cg->isPassed() ) return true; 
    
       cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_hi_gg_upc_noiseSup_L1TE4_VTE50");
    if( cg->isPassed() ) return true;
    
            cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_hi_gg_upc_noiseSup_L1TE5_VTE200");
    if( cg->isPassed() ) return true;
    
            cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_hi_gg_upc_noiseSup_L1TE5_VTE50");
    if( cg->isPassed() ) return true;
    
    return false;
    
}

/*****************************************************************/
bool EXCLRecoAnalysis::eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ(void){
/*****************************************************************/
    
    if(m_isMC) return true;
    
    // primary triggers?
    const Trig::ChainGroup* cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_hi_gg_upc_noiseSup_L12TAU1_VTE50");
    if( cg->isPassed() ) return true;
    
        cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_hi_gg_upc_noiseSup_L1TAU1_TE4_VTE200");
    if( cg->isPassed() ) return true;
    
    
    // other triggers
/*	cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_hi_gg_upc_L12TAU1_VTE50");
    if( cg->isPassed() ) return true; 

        cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_hi_gg_upc_L1TE50_VTE200");
    if( cg->isPassed() ) return true;
       
        cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAU1_TE4_VTE200");
    if( cg->isPassed() ) return true;
*/

    return false;
    
}

/*****************************************************************/
bool EXCLRecoAnalysis::eventFired_2018_exclusivelooseFgapTriggers(void){
/*****************************************************************/
    
    if(m_isMC) return true;
    
    const Trig::ChainGroup* cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_mb_sptrk_exclusiveloose2_L12TAU1_VTE50");
    //if( cg->isPassed() ) return true; 
    
    //cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_mb_sptrk_exclusiveloose2_L12TAU2_VTE200");
    //if( cg->isPassed() ) return true;

    //cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_mb_sptrk_exclusiveloose1_L1ZDC_XOR_VTE50");
    //if( cg->isPassed() ) return true;
    
        cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_mb_sptrk_exclusiveloose2_L1ZDC_XOR_VTE50");
    if( cg->isPassed() ) return true;
    
        cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_mb_sptrk_exclusiveloose2_L1ZDC_A_C_VTE50");
    if( cg->isPassed() ) return true;
    
        cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_mb_sptrk_exclusiveloose2_L1VZDC_A_C_VTE50");
    if( cg->isPassed() ) return true;

  
    return false;
    
}

/*****************************************************************/
bool EXCLRecoAnalysis::eventFired_2018_exclusivelooseNoFgapTriggers(void){
/*****************************************************************/
    
    if(m_isMC) return true;

    //const Trig::ChainGroup* cg = m_trigDecisionTool->getChainGroup("HLT_mb_sptrk_exclusiveloose_vetosp1500_L1VTE20");
    //if( cg->isPassed() ) return true; 
    
    
    const Trig::ChainGroup* cg = m_trigDecisionTool->getChainGroup("HLT_mb_sptrk_exclusiveloose2_L12TAU2_VTE50");
    //if( cg->isPassed() ) return true; 
    
    cg = m_trigDecisionTool->getChainGroup("HLT_mb_sptrk_exclusiveloose2_L12TAU2_VTE100");
    //if( cg->isPassed() ) return true;

    cg = m_trigDecisionTool->getChainGroup("HLT_mb_sptrk_exclusiveloose2_L12TAU2_VTE200");
    //if( cg->isPassed() ) return true;
    
        cg = m_trigDecisionTool->getChainGroup("HLT_mb_sptrk_vetombts2in_exclusiveloose2_L12TAU1_VTE50");
    if( cg->isPassed() ) return true;
    
        cg = m_trigDecisionTool->getChainGroup("HLT_mb_sptrk_vetombts2in_exclusiveloose2_L12TAU2_VTE50");
    if( cg->isPassed() ) return true;
    
        cg = m_trigDecisionTool->getChainGroup("HLT_mb_sptrk_vetombts2in_exclusiveloose2_L12TAU2_VTE100");
    if( cg->isPassed() ) return true;

        cg = m_trigDecisionTool->getChainGroup("HLT_mb_sptrk_vetombts2in_exclusiveloose2_L12TAU2_VTE200");
    if( cg->isPassed() ) return true;

        cg = m_trigDecisionTool->getChainGroup("HLT_mb_sp_L1VTE50");
    if( cg->isPassed() ) return true;
    
    return false;
    
}

/*****************************************************************/
bool EXCLRecoAnalysis::eventFired_2018_EMPTY(void){
/*****************************************************************/
    
    if(m_isMC) return true;

    
    
    const Trig::ChainGroup* cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_hi_gg_upc_noiseSup_L1TE4_VTE200_EMPTY");
    if( cg->isPassed() ) return true; 
    
    cg = m_trigDecisionTool->getChainGroup("HLT_hi_upc_FgapAC3_hi_gg_upc_noiseSup_L1TE5_VTE200_EMPTY");
    if( cg->isPassed() ) return true;

    return false;
    
}

/*****************************************************************/
bool EXCLRecoAnalysis::eventFiredHLT_mb_sptrk_ion_L1ZDC_A_C_VTE50(void){
/*****************************************************************/

   // const Trig::ChainGroup* cg = m_trigDecisionTool->getChainGroup("HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50");
   // bool passesTrigger = cg->isPassed();
   // if(passesTrigger) return true;

  return true;
}

/*****************************************************************/
bool EXCLRecoAnalysis::eventFiredHLT_mb_sptrk_vetombts2in_L1ZDC_A_C_VTE50(void){
/*****************************************************************/

    //const Trig::ChainGroup* cg = m_trigDecisionTool->getChainGroup("HLT_mb_sptrk_vetombts2in_L1ZDC_A_C_VTE50");
    //bool passesTrigger = cg->isPassed();
    //if(passesTrigger) return true;

  return true;
}

/*****************************************************************/
bool EXCLRecoAnalysis::eventFiredHLT_hi_loose_upc_L1ZDC_A_C_VTE50(void){
/*****************************************************************/

    //const Trig::ChainGroup* cg = m_trigDecisionTool->getChainGroup("HLT_hi_loose_upc_L1ZDC_A_C_VTE50");
    //bool passesTrigger = cg->isPassed();
    //if(passesTrigger) return true;

  return true;
}

/*****************************************************************/
bool EXCLRecoAnalysis::eventFiredHLT_mb_sptrk_vetombts2in_L1MU0_VTE50(void){
/*****************************************************************/

    //const Trig::ChainGroup* cg = m_trigDecisionTool->getChainGroup("HLT_mb_sptrk_vetombts2in_L1MU0_VTE50");
    //bool passesTrigger = cg->isPassed();
    //if(passesTrigger) return true;

  return true;
}



/*****************************************************************/
bool EXCLRecoAnalysis::eventFiredHLT_TE5(void){
/*****************************************************************/

   //const xAOD::EnergySumRoI* esumroi(0);
   //m_event->retrieve( esumroi, "LVL1EnergySumRoI" ).ignore();
   
   //std::cout<<"ESumROI : "<<esumroi->energyT()/1000.<<"\n";
   
   //if(esumroi->energyT()/1000. >= 5.) return true;
 
  return true;
}


/*****************************************************************/
bool  EXCLRecoAnalysis::isGoodDataEvent()
/*****************************************************************/
{
//---- check if it's a good data event and apply GRL


 if(m_isMC) return true;


  //----------------------------- GRL ----------------------------// 
  //commented for 2018 data-taking
  if(!m_grl->passRunLB(*m_eventInfo)){
     return false;
  }

 //----------------------------- EVENT CLEANING ----------------------------// 
 
  if( (m_eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error ) || 
      (m_eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error ) || 
      (m_eventInfo->errorState(xAOD::EventInfo::SCT)==xAOD::EventInfo::Error ) || 
      (m_eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) ) )  {
     
     return false;
  } 


  return true;

}

