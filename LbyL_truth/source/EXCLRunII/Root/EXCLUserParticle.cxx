/**
 *  @file  EXCLUserParticle.cxx
 *  @brief  
 *
 *
 *  @author  
 *
 *
 * =====================================================================================
 */

//--- Root

//--- Pacakge EXCLRunII
#include <EXCLRunII/EXCLUserParticle.h>

using namespace std;
//using namespace EXCLRunII;

/*****************************************************************/
EXCLUserParticle::EXCLUserParticle(void)
			:TLorentzVector(0.,0.,0.,0.),
			m_ID(0)
/*****************************************************************/
{

}

/*****************************************************************/
EXCLUserParticle::EXCLUserParticle(const short charge)
			:TLorentzVector(0.,0.,0.,0.),
			m_ID(0)
/*****************************************************************/
{
  m_charge = charge;
}


/*****************************************************************/
EXCLUserParticle::EXCLUserParticle(const EXCLUserParticle& body)
			:TLorentzVector(body)
/*****************************************************************/
{
	// Copy information from body
	m_ID = body.Type();
	m_charge = body.Charge();
}


/*****************************************************************/
EXCLUserParticle::EXCLUserParticle(const int ID,const short charge,
						const float E,const float Pt,
						const float Eta,const float Phi)
/*****************************************************************/
{     
	SetPtEtaPhiE(Pt,Eta,Phi,E);
	m_ID = ID;
	m_charge = charge;
}


/*****************************************************************/
EXCLUserParticle::~EXCLUserParticle()
/*****************************************************************/
{

}


