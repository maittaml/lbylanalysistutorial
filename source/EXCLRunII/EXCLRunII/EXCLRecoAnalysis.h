#ifndef EXCLRunII_EXCLRecoAnalysis_H
#define EXCLRunII_EXCLRecoAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>

class EXCLRecoAnalysis : public EL::AnaAlgorithm
{
public:
  /// This is a standard algorithm constructor
  EXCLRecoAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  /// These are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

private:
  /// Configuration, and any other types of variables go here.

};

#endif
