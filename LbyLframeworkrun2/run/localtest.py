#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
import time
timestr = time.strftime("%Y.%m.%d_%H.%M.%S")
print timestr
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default ="lbyl"+timestr,
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import argparse
import ROOT
import re
import os
import shutil
import sys

ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )


########################################
##########-----DATA 2018-----------#####
#######################################
#inputFilePath = ''

######################################################################
#########-----Signal MC: Superchic3 (100k)-----#######################
######################################################################
inputFilePath = '/eos/user/m/maittaml/lbyl_codes/lbylanalysis/mc/mc16_valid.420254.HepMCAscii_Superchic3gammagamma2gammagamma_M3p6.recon.AOD.e7188_s3428_r11334_tid17615400_00'

######################################################################
#########------Background MC: yy-> e+e-: STARlight (1.5M events)---##### 
######################################################################
#inputFilePath = ''

#####################################################################################
######################################################################
#########------Background MC: yy-> e+e-: STARlight (0.5M events)---##### 
######################################################################
#inputFilePath = ''

#####################################################################################
#########------Background MC: CEP gg -> yy: SuperChic2 (50k events)---############## 
####################################################################################
#inputFilePath = ''

#####################################################################################
#########------Background MC: CEP gg -> yy: SuperChic3 (100k events)---############## 
####################################################################################
#inputFilePath = ''

ROOT.SH.ScanDir().filePattern( '*' ).scan( sh, inputFilePath) 
sh.Print()

# Create an EventLoop job.
job = ROOT.EL.Job()
#job.useXAOD()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, -1 )
#job.options().setDouble (ROOT.EL.Job.optFilesPerWorker,200);

# Create the algorithm's configuration. Note that we'll be able to add
# algorithm property settings here later on.
from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
config = AnaAlgorithmConfig( 'LapxAODEvent/AnalysisAlg' )
config2 = AnaAlgorithmConfig( 'EXCLRecoAnalysis/AnalysisAlg2' )

#job.algsAdd( config )
job.algsAdd( config2 )
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))
# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
print "Output ==============> "+options.submission_dir
