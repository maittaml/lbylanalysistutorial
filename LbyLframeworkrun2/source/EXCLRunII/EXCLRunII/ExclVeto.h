#ifndef EXCLVETO_H
#define EXCLVETO_H
 
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackingPrimitives.h"
#include<TRandom3.h> 

class ExclVetoCandidate { 

  public:

     ExclVetoCandidate( const std::vector< xAOD::TrackParticle *> &fullTrkList, 
     			const std::vector< xAOD::TrackParticle *> &priTrkList,
			const std::vector< const xAOD::Vertex *> &fullVtxList,
			const xAOD::Vertex *priVtx) :
		m_fullTrkList(fullTrkList), m_priTrkList(priTrkList),
		m_fullVtxList(fullVtxList), m_priVtx(priVtx){ }

     ~ExclVetoCandidate() { }  


     int   nTrkAll() const {return m_fullTrkList.size(); }
     int   nTrkPri() const {return m_priTrkList.size(); }
     int   nVtx() const {return m_fullVtxList.size(); }
     
     std::vector< xAOD::TrackParticle *> fullTrkList() const {     return m_fullTrkList; }
     std::vector< xAOD::TrackParticle *> priTrkList() const {      return m_priTrkList; }
     std::vector< const xAOD::Vertex *> const fullVtxList() {     return m_fullVtxList; }
     const xAOD::Vertex * priVtx() const{     return m_priVtx; }
     
     bool passedRandomSampling(double z_ref, double vetoSize) const;

  private:

     const std::vector< xAOD::TrackParticle *> m_fullTrkList;
     const std::vector< xAOD::TrackParticle *> m_priTrkList;
     const std::vector< const xAOD::Vertex *> m_fullVtxList;
     const xAOD::Vertex * m_priVtx;
     
};


#endif
